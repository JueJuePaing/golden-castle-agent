import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import AppNavigator from './src/navigation/AppNavigator';
import {Provider} from './src/context/Provider';

import messaging from '@react-native-firebase/messaging';
import BGActions from './BGActions';
import { setItem } from './src/utils/appStorage';

const App = () => {

  useEffect(()=> {
    BGActions.startService()
  }, [])

  useEffect(() => {

    const initializeFirebase = async () => {
      await messaging().registerDeviceForRemoteMessages();

      const token = await messaging().getToken();

      setItem('@fcmToken', token);


      const granted = await messaging().requestPermission();
    }
    initializeFirebase();
  }, []);

  return (
    <Provider>
      <AppNavigator />
    </Provider>
  );
};
export default App;
