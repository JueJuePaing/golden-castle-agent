import React from 'react';
import { Easing } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

// Components
import HomeScreen from '../../pages/home/Home';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
  config: {
    duration: 200,
    easing: Easing.linear,
  },
};

const HomeStackNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureDirection: 'horizontal',
        gestureEnabled: false,
        transitionSpec: {
          open: config,
          close: config,
        }
      }}>
      <Stack.Screen
        name="Home"
        component={HomeScreen} />
    </Stack.Navigator>
  );
};

export default HomeStackNavigator;
