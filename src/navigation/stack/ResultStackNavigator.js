import React from 'react';
import { Easing } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

// Components
import ResultScreen from '../../pages/result/Result';
import CheckResultScreen from '../../pages/result/CheckResult';
import Winner from '../../pages/result/Winner';
import Sorry from '../../pages/result/Sorry';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
  config: {
    duration: 200,
    easing: Easing.linear,
  },
};

const ResultStackNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureDirection: 'horizontal',
        gestureEnabled: false,
        transitionSpec: {
          open: config,
          close: config,
        }
      }}>
      <Stack.Screen
        name="Result"
        component={ResultScreen} />
      <Stack.Screen
        name="CheckResult"
        component={CheckResultScreen}/>
      <Stack.Screen
        name="Winner"
        component={Winner}/>
      <Stack.Screen
        name="Sorry"
        component={Sorry}/>
    </Stack.Navigator>
  );
};

export default ResultStackNavigator;
