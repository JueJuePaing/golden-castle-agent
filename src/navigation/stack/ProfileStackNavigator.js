import React from 'react';
import { Easing } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

// Components
import ProfileScreen from '../../pages/profile/Profile';
import PurchasedTicketsScreen from '../../pages/purchasedTickets/PurchasedTickets';
import WinnerList from '../../pages/winnerList/WinnerList';
import ChangePassword from '../../pages/changePassword/ChangePassword';
import ContactUs from '../../pages/contactUs/ContactUs';
import AboutUs from '../../pages/aboutUs/AboutUs';
import FAQ from '../../pages/faq/FAQ';
import Feedback from '../../pages/feedback/Feedback';
import WithdrawScreen from '../../pages/withdraw/Withdraw';
import WithdrawHistory from '../../pages/withdraw/WithdrawHistory';
import RecordDetailScreen from '../../pages/withdraw/RecordDetail';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
  config: {
    duration: 200,
    easing: Easing.linear,
  }
};

const ProfileStackNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureDirection: 'horizontal',
        gestureEnabled: false,
        transitionSpec: {
          open: config,
          close: config
        }
      }}>
      <Stack.Screen
        name="Profile"
        component={ProfileScreen} />
      <Stack.Screen
        name="PurchasedTickets"
        component={PurchasedTicketsScreen} />
      <Stack.Screen
        name="WinnerList"
        component={WinnerList} />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword} />
      <Stack.Screen
        name="ContactUs"
        component={ContactUs} />
      <Stack.Screen
        name="FAQ"
        component={FAQ} />
      <Stack.Screen
        name="Feedback"
        component={Feedback} />
      <Stack.Screen
        name="AboutUs"
        component={AboutUs} />
      <Stack.Screen
        name="Withdraw"
        component={WithdrawScreen} />
      <Stack.Screen
        name="WithdrawHistory"
        component={WithdrawHistory} />
              <Stack.Screen
        name="RecordDetail"
        component={RecordDetailScreen} />
    </Stack.Navigator>
  );
};

export default ProfileStackNavigator;
