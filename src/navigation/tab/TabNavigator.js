import React, {
  useState,
  useContext,
  useEffect
} from 'react';
  import {
    Modal,
    DeviceEventEmitter,
    Animated,
    TouchableOpacity,
    Text,
    View,
    ScrollView,
    Image
  } from 'react-native';
import { CurvedBottomBar } from 'react-native-curved-bottom-bar';
import { NavigationContainer,  } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import PushNotification from "react-native-push-notification";
import messaging from "@react-native-firebase/messaging";

import { Context } from '../../context/Provider';
import { formatNumber } from '../../utils/common';
import { fetchPostByToken, fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { decryptData } from '../../utils/decryptData';
import { encryptData } from '../../utils/encryptData';

import styles from './Style';
import TicketNumber from '../../components/ticketNumber/TicketNumber';
import HomeStackNavigator from '../stack/HomeStackNavigator';
import ResultStackNavigator from '../stack/ResultStackNavigator';
import NotificationStackNavigator from '../stack/NotificationStackNavigator';
import ProfileStackNavigator from '../stack/ProfileStackNavigator';
import UsersModal from '../../pages/usersModal/UsersModal';

const TabBar = () => {

  const local = useLocal();
  const {
    userInfo,
    lang
  } = useContext(Context);

  const [
    profileData,
    setProfileData
  ] = useState();
  const [
    showUsers,
    setShowUsers
  ] = useState(false);

  useEffect(()=> {
    const eventEmitter = DeviceEventEmitter.addListener(
        'noti_count_change',
        val => {
          if (val === "true") {
            getProfile();
          }
        },
      );
      getProfile();
      return () => eventEmitter;
}, [userInfo])

const getProfile = async () => {
    if (userInfo?.access_token) {
      const response = await fetchGetByToken(apiUrl.profile, userInfo.access_token);

      if (response?.status === 200 && response?.data) {
          const decAgentinfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
          setProfileData(decAgentinfo?.agent);
      }
    }
}

  useEffect(() => {

    PushNotification.configure({

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {

        getProfile();
        if (notification && notification.data && notification.data.title && notification.data.message) {
          displayNotification(notification.data.title, notification.data.message);
        }
      }
    });

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('setBackgroundMessageHandler : ' + JSON.stringify(remoteMessage));
    });
  }, [userInfo]);

const displayNotification = (title, desc) => {

  const options = {
    soundName: 'default',
    playSound: true
  }

  PushNotification.localNotification({

    channelId : "fcm_fallback_notification_channel",
    autoCancel: true,
    largeIcon: options.largeIcon || 'icon',
    smallIcon: options.smallIcon || 'icon',
    bigText: desc || '',
    subText: title || '',
    vibrate: options.vibrate || true,
    vibration: options.vibration || 300,
    priority: options.priority || 'high',
    importance: options.importance || 'high',
    
    title: title || '',
    message: desc || '',
    playSound: true,
    //soundName: 'default',
    userInteraction: true,
    invokeApp: true
  })
}

  const _renderIcon = (routeName, selectedTab) => {
    let icon = '';

    switch (routeName) {
      case 'Home1':
        icon = 'ios-home-outline';
        break;
      case 'ResultStack':
        icon = 'ios-trophy-outline';
      break;
      case 'title3':
          icon = 'notifications-outline';
          break;
      case 'Profile':
          icon = 'user';
          break;
    }

    let showCount = 0;
    if (routeName === "title3") {
      if (profileData?.unread_noti_count && profileData?.unread_noti_count !== 0 && profileData?.unread_noti_count !== "0") {
        showCount = profileData.unread_noti_count;
      }
    }

    return (
        <View style={{
          backgroundColor: routeName === selectedTab ? '#ff9700' : '#fff',
          width: 35,
          height: 35,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 20}}>
          {
            icon === "user" ?
              <AntDesign
                name={icon}
                size={routeName === selectedTab ? hp(2.2) : hp(3)}
                color={routeName === selectedTab ? '#fff' : 'gray'} /> :
              <Ionicons
                name={icon}
                size={routeName === selectedTab ? hp(2.2) : hp(3)}
                color={routeName === selectedTab ? '#fff' : 'gray'}
                />
          }
          {
            routeName === "title3" && showCount !== 0 && (
              <TouchableOpacity style={styles.badge}>
                <Text style={styles.notiCount}>
                  {showCount}
                </Text>
              </TouchableOpacity>
            )
        }
        {/* {(routeName === "title3") && <TouchableOpacity style={styles.badge}>
          <Text style={styles.notiCount}>
            9
          </Text>
          </TouchableOpacity>} */}
        </View>
      );

  };

  const renderTabBar = ({ routeName, selectedTab, navigate }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigate(routeName);
        }}
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {_renderIcon(routeName, selectedTab)}
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <CurvedBottomBar.Navigator
             screenOptions={{
                headerShown: false
            }}
          type="UP"
          style={styles.bottomBar}
          strokeWidth={0.5}
          strokeColor="#DDDDDD"
          height={55}
          circleWidth={55}
          bgColor="white"
          initialRouteName="Home1"
          borderTopLeftRight
          renderCircle={({ selectedTab, navigate }) => (
            <Animated.View style={styles.btnCircleUp}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  justifyContent: 'center',
                }}
                onPress={() => {
                  setShowUsers(true)
                }}>
                  <Entypo
                    name='users'
                    size={hp(2)}
                    color='#fff' />
              </TouchableOpacity>
            </Animated.View>
          )}
          tabBar={renderTabBar}>
             <CurvedBottomBar.Screen
              name="Home1"
              position="LEFT"
              component={() => (
                <HomeStackNavigator />
              )}
            />
             <CurvedBottomBar.Screen
              name="ResultStack"
              position="LEFT">
              {props => <ResultStackNavigator {...props}/>}
            </CurvedBottomBar.Screen>
            
             <CurvedBottomBar.Screen
              name="title3"
              position="RIGHT"
              component={() => (
                <NotificationStackNavigator />
              )}
            />
            <CurvedBottomBar.Screen
              name="Profile"
              position="RIGHT">
              {props => <ProfileStackNavigator {...props}/>}
            </CurvedBottomBar.Screen> 
            {/* 
             <CurvedBottomBar.Screen
              name="title3"
              position="RIGHT"
            >
               {props => <NotificationStackNavigator {...props}/>}
            </CurvedBottomBar.Screen>
          */}

        </CurvedBottomBar.Navigator>
        {showUsers && <UsersModal
          onClose={()=> setShowUsers(false)}
        />}
      </NavigationContainer>
    </View>
  );
};


export default TabBar;