import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
      },
      button: {
        marginVertical: 5,
      },
      bottomBar: {},
      btnCircleUp: {
        width: 60,
        height: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff9700',
        bottom: 18,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 1,
    
    
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
    
        borderWidth: 1.5,
        borderColor: '#11212f'
      },
      imgCircle: {
        width: 30,
        height: 30,
        tintColor: 'gray',
      },
      img: {
        width: 30,
        height: 30,
      },
      badge : {
        width: 20,
        height: 20,
        backgroundColor: '#11212f',
        borderRadius: 20,
        position: 'absolute',
        top: -3,
        right: -3,
        justifyContent: 'center',
        alignItems: 'center'
      },
      notiCount: {
        color: '#fff',
        fontSize : 11
      },
      conImg: {
        width: wp(100),
        height: hp(100),
        position: 'absolute'
    },
    modalView: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        height: hp(100),
        justifyContent: 'center',
        width: wp(100)
    },
    modalBg: {
        backgroundColor: '#ff9700',
        borderRadius: hp(1),
        width: wp(80),
        alignSelf: 'center',
        minHeight: hp(25),
        maxHeight: hp(75)
        // marginVertical: hp(10)
    },
    crown: {
        width: wp(50),
        height: hp(20),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: hp(-8)
    },
    sorryImg: {
        width: wp(60),
        height: hp(25),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: hp(-12),
        marginLeft: wp(-4)
    },
    prizeRow: {
        width: wp(65),
        //backgroundColor: 'red',
        alignSelf: 'center',
        marginVertical: hp(2),
        alignItems: 'center'
    },
    prizeTitle: {
        color: '#11212f',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6)
    },
    prizeAmt: {
        color: '#fff',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2.2),
        letterSpacing: .5,
        paddingBottom: hp(.7)
    },
    sorryTitle: {
        color: '#11212f',
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.7),
        textAlign: 'center'
    },
    sorryDesc: {
        color: '#f2cc94',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        textAlign: 'center',
        paddingTop: hp(1),
    },
    submitBtn: {
        width: wp(28),
        height: hp(4),
        backgroundColor: '#11212f',
        borderRadius: hp(3),
        flexDirection:'row',
        alignItems: 'center',
        marginTop: hp(1.5),
        alignSelf: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3
  },
  submit: {
        fontSize :hp(1.6),
        color: '#fff',
        //paddingLeft: wp(4),
        fontFamily: 'Poppins-Medium',
        paddingTop: hp(.3)
  },
});

export default styles;