import React, {useEffect, useContext, useState} from 'react';
import { LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import NetInfo from '@react-native-community/netinfo';
import DeviceInfo from 'react-native-device-info';

import { getItem, setItem } from '../utils/appStorage';
import { Context } from '../context/Provider';
import { fetchPostByToken, fetchGetByToken } from '../utils/fetchData';
import apiUrl from '../utils/apiUrl';
import { deleteMultiItems } from '../utils/appStorage';
import { encryptData } from '../utils/encryptData';
import { decryptData } from '../utils/decryptData';

import TabNavigator from './tab/TabNavigator';
import SplashScreen from '../pages/splash/Splash';
import LoginModal from '../pages/auth/Login';

const Stack = createStackNavigator();

const AppNavigator = () => {

    const { 
        userInfo,
        lang,
        profileInfo,
        changeLang,
        changeUserInfo,
        changeBanners,
        changeNet,
        changeProfileInfo,
        changeCountdownData,
        changeLastDrawData,
        changeUsersTicketsData
    } = useContext(Context);

    const lanData = {
        language : lang
    }
    let encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        temporary,
        setTemporary
    ] = useState(true);

    useEffect(() => {
        const netListener = NetInfo.addEventListener(state => {
            changeNet(state.isInternetReachable);
        });
        return () => {
            netListener();
        };
    }, []);

    useEffect(() => {
        const ignoreWarns = [
            "EventEmitter.removeListener",
            "[fuego-swr-keys-from-collection-path]",
            "Setting a timer for a long period of time",
            "ViewPropTypes will be removed from React Native",
            "AsyncStorage has been extracted from react-native",
            "exported from 'deprecated-react-native-prop-types'.",
            "Non-serializable values were found in the navigation state.",
            "VirtualizedLists should never be nested inside plain ScrollViews",
            'Warning: ...',
            'Looks like',
            'ViewPropTypes '
          ];
        
          const warn = console.warn;
          console.warn = (...arg) => {
            for (const warning of ignoreWarns) {
              if (arg[0].startsWith(warning)) {
                return;
              }
            }
            warn(...arg);
          };
        LogBox.ignoreLogs(ignoreWarns); // Ignore log notification by message //['Warning: ...']
        LogBox.ignoreAllLogs();//Ignore all log notifications
    }, [])

    useEffect(() => {
        const getProfileData = async () => {
            if (userInfo?.access_token) {
                const response = await fetchGetByToken(apiUrl.profile, userInfo.access_token);
                if (response?.status === 200 && response?.data) {
                    const decAgentinfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
                    changeProfileInfo(decAgentinfo?.agent);
                    setItem('@offline_profile', JSON.stringify(decAgentinfo?.agent));
                }
            } 
        }
        if (userInfo && !profileInfo) {
            getProfileData();
        }
    }, [userInfo, profileInfo]);

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
        setTemporary(false);
    }

    const getDeviceInfo = () => {

        let _device_model = DeviceInfo.getModel();
        let _os_version = DeviceInfo.getSystemVersion();
        let _app_version = DeviceInfo.getVersion();
        let _os_type = DeviceInfo.getSystemName(); // Android? iOS?

        DeviceInfo.getDeviceName().then((deviceName) => {
            const info = JSON.stringify({
                _device_name : deviceName,
                _device_model,
                _os_version,
                _os_type,
                _app_version
            });

            setItem('@deviceInfo', info);
        });
    };

    useEffect(() => {
        const getStorageData = async () => {
            const language = await getItem('@lang');
            const userInfo = await getItem('@userInfo');

            const offline_banners = await getItem('@offline_banners')
            const offline_profile = await getItem('@offline_profile');
            const offline_countdown = await getItem('@offline_countdown')
            const offline_lastDraw = await getItem('@offline_lastDraw')
            const offline_userTickets = await getItem('@offline_userTickets');

            const parsedBanners = JSON.parse(offline_banners);
            const parsedProfile = JSON.parse(offline_profile);
            const parsedCountdown = JSON.parse(offline_countdown);
            const parsedLastDraw = JSON.parse(offline_lastDraw);
            const parsedUsersTickets = JSON.parse(offline_userTickets);

            changeBanners(parsedBanners);
            changeProfileInfo(parsedProfile);
            changeCountdownData(parsedCountdown);
            changeLastDrawData(parsedLastDraw);
            changeUsersTicketsData(parsedUsersTickets);

            changeLang(language ? language : 'en');
            getDeviceInfo()

            const user = JSON.parse(userInfo)
            if (user?.access_token) {
                changeUserInfo(JSON.parse(userInfo));

                const response = await fetchPostByToken(apiUrl.banners, lanData, user.access_token);
                if (response?.success && response?.data) {
                    const decBanners = JSON.parse(decryptData(response.data, user.secret_key));
                    changeBanners(decBanners?.banner_urls);
                    setItem('@offline_banners', JSON.stringify(decBanners?.banner_urls));
                    
                } else if (response?.status === 401) {
                    // Unauthenticated
                    logoutHandler();
                } 

                let encryptLanData = encryptData(JSON.stringify(lanData), user.secret_key);
                const countdownResponse = await fetchPostByToken(apiUrl.countdown, {data: encryptLanData}, user.access_token);

                if (countdownResponse?.success && countdownResponse?.data) {
                    const decCountDown = JSON.parse(decryptData(countdownResponse.data, user.secret_key));

                    changeCountdownData(decCountDown);
                    setItem('@offline_countdown', JSON.stringify(decCountDown));
                } else if (countdownResponse?.status === 401 || countdownResponse?.message === "Unauthenticated.") {
                    logoutHandler();
                }


                const lastDrawResponse = await fetchPostByToken(apiUrl.lastDrawTickets, {data: encryptLanData}, user.access_token);

                if (lastDrawResponse?.success && lastDrawResponse?.data) {
                    const decLastDraw = JSON.parse(decryptData(lastDrawResponse.data, user.secret_key));
                    changeLastDrawData(decLastDraw);
                    setItem('@offline_lastDraw', JSON.stringify(decLastDraw));
                }

                const usersTicketsResponse = await fetchPostByToken(apiUrl.usersTicketsNumbers, {data: encryptLanData}, user.access_token);
                if (usersTicketsResponse?.success && usersTicketsResponse?.data) {
                    let decTickets = JSON.parse(decryptData(usersTicketsResponse.data, user.secret_key));
                    changeUsersTicketsData(decTickets);
                    setItem('@offline_userTickets', JSON.stringify(decTickets));

                }

                setTemporary(false);
            } else {
                setTemporary(false);
            }
        }

        getStorageData();
    }, []);

    if (temporary) 
        return <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}>
                <Stack.Screen 
                    name="Splash" 
                    component={SplashScreen} />
            </Stack.Navigator>
     
        </NavigationContainer>

    else if (!userInfo) 
        return <LoginModal />
    
    return <TabNavigator />
}

export default AppNavigator;