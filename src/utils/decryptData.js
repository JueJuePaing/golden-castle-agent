
import CryptoJS from 'crypto-js';
import { decode } from 'base-64';

export const decryptData = (passdata, key=null) => {
  var dynamicKey = key ? key : "Reab/PseflYtVWws08yj6u5RCHXZL/gkSq5AAyn8re4=";

  let encrypted = decode(passdata);
  encrypted = JSON.parse(encrypted);

  const iv = CryptoJS.enc.Base64.parse(encrypted.iv);

  // Value (chipher text) is also base64 encoded in Laravel, same in cryptojs
  const value = encrypted.value;

  var cryptoKey = CryptoJS.enc.Base64.parse(dynamicKey);

  // Decrypt the value, providing the IV. 
  var decrypted = CryptoJS.AES.decrypt(value, cryptoKey, {
      iv: iv
  });

  // CryptoJS returns a word array which can be 
  // converted to string like this
  return decrypted.toString(CryptoJS.enc.Utf8);
}
