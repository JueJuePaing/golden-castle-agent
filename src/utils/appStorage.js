import AsyncStorage from '@react-native-async-storage/async-storage';

export const setItem = async (key, value) => {
  try {
    if (typeof value == 'undefined' || typeof key == 'undefined') {
      //
    } else {
      await AsyncStorage.setItem(key, value)
    }
  } catch (e) {
    console.log("Can't save data : ", e);
  }
}

export const getItem = async (key) => {
  const jsonValue = await AsyncStorage.getItem(key);
  return jsonValue ? jsonValue : null;
}

export const clearStorage = async () => {
  await AsyncStorage.clear();
}

export const deleteMultiItems = async (keys) => {
  await AsyncStorage.multiRemove(keys);
}


