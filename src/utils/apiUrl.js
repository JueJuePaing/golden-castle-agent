export default {
    // Auth
    agentLogin: 'agent/login',
    userRegister: 'agent/register',
    getDrawDates: 'agent/draw-ids',
    getLotteryResults: 'agent/results',
    checkLotteryResults: 'agent/results/check',
    winnerList: 'agent/results/winners',
    updateLanguage: 'agent/language/update',
    banners: 'agent/banners',
    countdown: 'agent/count-down',
    liveTickets: 'agent/tickets/live',
    usersTicketsNumbers: 'agent/tickets/users',
    lastDrawTickets: 'agent/results/numbers',
    profile: 'agent/profile',
    purchasedTickets: 'agent/tickets/purchased',
    purchase: 'agent/tickets/purchase',
    updateProfile: 'agent/profile/update',
    updatePassword: 'agent/password/update',
    faqs: 'agent/faqs',
    feedback: 'agent/feedback',
    notifications: 'agent/notifications',
    markAllRead: 'agent/notifications/read-all',
    checkLotteryWon: 'agent/notifications/won',
    readLotteryWon: 'agent/notifications/read/won',
    contactInfo: 'agent/helps',
    users: 'agent/get/users/list',
    usersTickets: 'agent/get/user-tickets',

    // Withdraw
    withdrawAccounts : 'agent/withdraw/options',
    withdraw : 'agent/withdraw',
    withdrawRecords: 'agent/withdraw/records'
}