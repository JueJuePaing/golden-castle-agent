import Config from 'react-native-config';

const api = Config.API_URL;

const timeoutforfetch = 5000;

// FetchPost
export const fetchPost = async (route, data) => {
  let response = null;
  try {
    response = await fetch(api + route, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
        // 'Content-Type': 'multipart/form-data',
      },
      body: JSON.stringify(data)
      // body: data
    });
    if (response != null) {
      let jsonRes = await response.json();
      return { data : jsonRes, success: response.status === 200, status: response.status };
    }
  } catch (e) {
    console.error(e);
  }
  return null;
};

// FetchPost With Token

export const fetchPostByToken = async (route, data, token, signal = null) => {
  let response = null;
  try {
    response = await fetch(api + route, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify(data),
      signal: signal,
      timeout: timeoutforfetch,
    });
    if (response != null) {
      let json = await response.json();
      return {...json, success: response.status === 200, status: response.status};
    }
  } catch (e) {
    console.log(e);
  }
  return null;
};

// FetchPostImageUPload With Token

export const fetchPostImageUpload = async (
  route,
  data,
  token,
  signal = null,
) => {
  let response = null;
  try {
    response = await fetch(api + route, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + token,
      },
      body: data,
      signal: signal,
      timeout: timeoutforfetch,
    });
    if (response != null) {
      let json = await response.json();
      return {...json, success: response.status === 200, status: response.status};
    }
  } catch (e) {
    console.log(
      e.name === 'AbortError' ? 'Promise Aborted' : 'Promise Rejected',
    );
  }

  return null;
};

// fetchPostRegister

export const fetchPostRegister = async (route, data) => {
  let response = null;
  try {
    response = await fetch(api + route, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    });
    if (response != null) {
      let json = await response.json();
      return json;
    }
  } catch (e) {
    console.log(
      e.name === 'AbortError' ? 'Promise Aborted' : 'Promise Rejected',
    );
  }

  return null;
};

// FetchPost Pagination With Token

export const fetchPostPaginate = async (route, data, token, signal = null) => {
  let response = null;
  try {
    response = await fetch(route, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify(data),
      signal: signal,
      timeout: timeoutforfetch,
    });
    if (response != null) {
      let json = await response.json();
      return json;
    }
  } catch (e) {
    console.log(
      e.name === 'AbortError' ? 'Promise Aborted' : 'Promise Rejected',
    );
  }
  return null;
};

// FetchGet With Token

export const fetchGetByToken = async (route, token, signal = null) => {
  let response = null;
  try {
    response = await fetch(api + route, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      signal: signal,
      timeout: timeoutforfetch,
    });
    if (response != null) {
      let json = await response.json();
      return json;
    }
  } catch (e) {
    console.log(
      e.name === 'AbortError' ? 'Promise Aborted' : 'Promise Rejected',
    );
  }
  return null;
};
