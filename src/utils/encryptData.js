import CryptoJS from 'crypto-js';

export const encryptData = (passdata, key = null) => {

  let dynamicKey = key ? key : 'Reab/PseflYtVWws08yj6u5RCHXZL/gkSq5AAyn8re4=';

  const cryptoKey = CryptoJS.enc.Base64.parse(dynamicKey);
  let iv = CryptoJS.lib.WordArray.random(16);
  let options = {
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
  };
  let encrypted = CryptoJS.AES.encrypt(passdata, cryptoKey, options);
  encrypted = encrypted.toString();
  iv = CryptoJS.enc.Base64.stringify(iv);
  let result = {
      iv: iv,
      value: encrypted,
      mac: CryptoJS.HmacSHA256(iv + encrypted, cryptoKey).toString()
  }
  result = JSON.stringify(result);
  result = CryptoJS.enc.Utf8.parse(result);
  return CryptoJS.enc.Base64.stringify(result);
}
