export default {
  buyTab: 'BUY',
  // Login
  login: 'Login',
  register: 'Register',
  welcomeTo: 'Welcome To',
  appName: 'Shwe Taik',
  name: 'Name',
  username: 'Username',
  password: 'Password',
  confirmPassword: 'Confirm Password',
  agentCode: 'Agent Code',
  code: 'Code',

  // Home
  nextdraw: 'Next draw countdown',
  liveSaleTickets: 'Live Sale Tickets',
  lastDrawTickets: 'Last Draw Tickets',
  live: 'LIVE',
  noUserTickets: 'There is no ticket from users.',
  contactUsers: 'Contact Users',
  earningPoints: 'Earning Points',
  
  // Results
  results: 'Results',
  checkResult: 'Check Result',
  firstPrize: '1st Prize',
  closetFirstPrize: 'Closet 1st Prize',
  secondPrize: '2nd Prize',
  thirdPrize: '3rd Prize',
  fourthPrize: '4th Prize',
  fifthPrize: '5th Prize',
  first3Digits: 'First 3 Digits',
  last3Digits: 'Last 3 Digits',
  last2Digits: 'Last 2 Digits',
  ticketNumbers: 'Ticket Numbers',
  series: 'Series',
  from: 'From',
  to: 'To',
  youDidNotWin: "YOU DIDN'T WIN",
  thankyou: 'Thank you for participation',

  // Purchase
  purchase: 'Purchase',
  purchaseSuccessful: 'Your purchase was successful',
  coming: 'Coming',

  // Users
  users: 'Users',

  // Notification
  notifications: 'Notifications',
  notification: 'Notification',
  yesterday: 'Yesterday',
  today: 'Today',
  markAllRead: 'Mark All Read',

  // Profile
  agent: 'Agent',
  notickets: 'tickets',
  purchasedTickets: 'Purchased Tickets',
  userTickets: "Users Tickets",
  winnerList: 'Winner List',
  liveOpening: 'Live Opening',
  general: 'General',
  changePassword: 'Change Password',
  language: 'Language',
  aboutUs: 'About Us',
  contactUs: 'Contact Us',
  rateUs:'Rate Us',
  terms: 'Terms & Conditions',
  faq: 'FAQ',
  feedback: 'Feedback',
  logout: 'Logout',
  buy: 'BUY',
  or: 'OR',
  recharge: 'Recharge',
  general: 'General',
  aboutAndTerms: 'About & Terms',
  helpAndSupport: 'Help & Support',
  updateName: 'Update Name',

  // Purchased Tickets
  draw: 'Draw',
  tickets: 'tickets',

  // Winner List
  firstPrizeWinners: '1st Prize Winners',
  secondPrizeWinners: '2nd Prize Winners',
  thirdPrizeWinners: '3rd Prize Winners',
  fourthPrizeWinners: '4th Prize Winners',
  fifthPrizeWinners: '5th Prize Winners',
  first3DigitsWinners: 'First 3 Digits  Winners',
  last3DigitsWinners: 'Last 3 Digits Winners',
  last2DigitsWinners: 'Last 2 Digits Winners',

  // FAQ
  frequentlyAskedQues: 'Frequently Asked Questions',
  faqDescrition: 'Here are some most commonly asked questions about Shwe Taik. Feel free to contact us for more questions.',

  // Contact Us
  callUs: 'Call Us',
  emailUs: 'Email Us',

  // Change Password
  oldPassword: 'Old Password',
  newPassword: 'New Password',

  // Feedback
  giveFeedback: 'Give Feedback',
  title: 'Title',
  detail: 'Detail',
  thanksForFeedback: 'Thanks for your feedback!',
  reviewFeedback: 'We will review your feedback and try to improve our system. Thanks again for your time.',
  anotherFeedback: 'Give another Feedback',

  // Recharge
  recharge: 'Recharge',
  history: 'History',
  rechargeHistory: 'Recharge History',
  scanToPay: 'Scan here to pay',
  noWithdrawAccount: 'NO RECHARGE ACCOUNTS',
  uploadReceipt: 'UPLOAD SCREENSHOT',
  rechargeSS: 'Recharge Screenshot',

    // Withdraw
    withdraw: 'Withdraw',
    point: 'Point',
    accountNo: 'Account Number',
    accountName: 'Account Name',
    qrImage: 'QR Image (Optional)',
    enterPasscode: 'Please enter password',
    withdrawHistory: 'Withdraw History',

  // Error Messages
  invalidName: 'Please enter name',
  invalidUsername: 'Please enter username',
  invalidPassword: 'Please enter password',
  invalidConfirmPassword: 'Please enter confirm password',
  mismatchPassword: "Confirm password doesn't match",
  invalidAgentCode: 'Please enter agent code',
  invalidTicketNo: 'Please enter valid ticket number',
  invalidTicketNos: 'Please enter valid ticket numbers',
  noConnection: '"Please check your internet connection',
  somethingWrong: 'Something went wrong. Please try again!',
  invalidOldPassword: 'Please enter old password',
  invalidNewPassword: 'Please enter new password',
  invalidFeedbackTitle: 'Please enter title',
  invalidFeedbackDetail: 'Please enter detail',
  invalidAccNo: 'Please enter account number',
  invalidAccName: 'Please enter account name',
  invalidPoint: 'Please enter point to withdraw',
  invalidZeroPoint:  "Can't withdraw 0 point",
  
  //Empty Messages
  noNotification: 'NO NOTIFICATIONS',
  noPurchasedTickets: 'NO PURCHASE HISTORY',
  noWinnerList: 'NO WINNER',
  noData: 'NO DATA',
  noInternetConnection: 'No Internet Connection',
  noRecord: 'NO RECORDS',
  noUser: 'NO USERS',

  // Buttons
  accept: 'Accept',
  decline: 'Decline',
  buy: 'Buy',
  submit: 'Submit',
  finish: 'Finish',
  ok: 'OK',
  update: 'Update',
  reload: 'Reload',
  cancel: 'Cancel',
  confirm: 'Confirm',
  
  // Common
  loading: 'Loading',

  // Alert Messages
  successful: 'Successful',
  unauthenticated: 'Unauthenticated',
  duplicateTickets: 'Duplicate Tickets',
  forceUpdateApp: 'Please update app latest version to continue using!',
  sessionExpired: 'Sorry, your session has expired!',
  loginAgain: 'Please login again',
  copied: 'Copied phone number',
  requested: 'Requested',
  completed: 'Completed',
  rejected: 'Rejected',
  refunded: 'Refunded',
  cannotBuy: "Can't buy on lottery opening day",
  transactionId: 'Transaction ID',
  transferWith: 'Transfer with',
  rechargePoint: 'Recharge Point',
  rechargeDate: 'Recharge Date',
  rechargeStatus: 'Recharge Status',
  ss: 'Screenshot',
  yourQr: 'Your QR Image',
  withdrawSS: 'Withdraw Screenshot',
  tooManyAttempts: 'Too Many Attempts',
  
};
