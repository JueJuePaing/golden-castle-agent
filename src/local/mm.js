
export default {
  buyTab: '၀ယ်',

  // Login
  login: 'အကောင့်ဝင်မည်',
  register: 'အသစ်ပြုလုပ်မည်',
  welcomeTo: 'Shwe Taik မှ',
  appName: 'ကြိုဆိုပါတယ်',
  name: 'အသုံးပြုသူနာမည်',
  username: 'နာမည်',
  password: 'လျှို့ဝှက်ကုဒ်',
  confirmPassword: 'အတည်ပြု လျှို့ဝှက်ကုဒ်',
  agentCode: 'ကိုယ်စားလှယ်ကုဒ်',
  code: 'ကိုယ်စားလှယ်ကုဒ်',

  // Home
  nextdraw: 'နောင်လာမည့်ထီ',
  liveSaleTickets: 'အရောင်းလက်မှတ်များ',
  lastDrawTickets: 'နောက်ဆုံးထွက် ထီလက်မှတ်များ',
  live: 'တိုက်ရိုက်',
  noUserTickets: 'အသုံးပြုသူများမှ လက်မှတ် မ၀ယ်ယူထားပါ',
  contactUsers: 'အသုံးပြုသူများကို ဆက်သွယ်မည်',
  earningPoints: 'လက်ရှိပွိုင့်',
  
  // Results
  results: 'ထီပေါက်စဉ်များ',
  checkResult: 'လက်မှတ်စစ်ဆေးရန်',
  firstPrize: 'ပထမဆု',
  closetFirstPrize: 'အနီးစပ်ဆုံး ပထမဆု',
  secondPrize: 'ဒုတိယဆု',
  thirdPrize: 'တတိယဆု',
  fourthPrize: 'စတုတ္ထဆု',
  fifthPrize: 'ပဉ္စမဆု',
  first3Digits: 'ပထမသုံးလုံးတူ',
  last3Digits: 'နောက်ဆုံးသုံးလုံးတူ',
  last2Digits: 'နောက်ဆုံးနှစ်လုံးတူ',
  ticketNumbers: 'ထီလက်မှတ်များ',
  series: 'စီးရီး',
  from: 'မှ',
  to: 'သို့',
  youDidNotWin: "သင့်ဂဏန်းသည် ထီပေါက်စာရင်းတွင် မပါ၀င်ပါ",
  thankyou: 'ကျေးဇူးတင်ပါသည်',

  // Purchase
  purchase: '၀ယ်ယူမည်',
  purchaseSuccessful: '၀ယ်ယူမှု အောင်မြင်ပါသည်',
  coming: 'နောက်လာမည်',

  // Users
  users: 'အသုံးပြုသူများ',

  // Notification
  notifications: 'အသိပေးချက်များ',
  notification: 'အသိပေးချက်',
  yesterday: 'မနေ့က',
  today: 'ဒီနေ့',
  markAllRead: 'အားလုံး ဖတ်ပြီးပြီ',

  // Profile
  agent: 'ကိုယ်စားလှယ်',
  notickets: 'စောင်',
  purchasedTickets: '၀ယ်ယူထား‌သော လက်မှတ်များ',
  userTickets: "၀ယ်ယူသူ လက်မှတ်များ",
  winnerList: 'ထီပေါက်သူစာရင်း',
  liveOpening: 'ထီဖွင့်ပွဲတိုက်ရိုက်',
  general: 'General',
  changePassword: 'လျှို့ဝှက်ကုဒ် ပြောင်းမည်',
  language: 'ဘာသာစကား',
  aboutUs: 'App အကြောင်း',
  contactUs: 'ဆက်သွယ်ရန်',
  rateUs: 'Rating ပေးမည်',
  terms: 'သတ်မှတ်ချက်များ',
  faq: 'အမေးများသော မေးခွန်းများ',
  feedback: 'ထင်မြင်ချက်ပေးမည်',
  logout: 'အကောင့်မှထွက်မည်',
  or: 'သို့',
  general: 'အထွေထွေ',
  aboutAndTerms: 'အချက်အလက်ဆိုင်ရာ',
  helpAndSupport: 'အကူအညီ',
  updateName: 'အမည်ပြောင်းမည်',

  // Purchased Tickets
  draw: 'ထွက်စဉ်',
  tickets: 'လက်မှတ်များ',

  // Winner List
  firstPrizeWinners: 'ပထမဆု',
  secondPrizeWinners: 'ဒုတိယဆု',
  thirdPrizeWinners: 'တတိယဆု',
  fourthPrizeWinners: 'စတုတ္ထဆု',
  fifthPrizeWinners: 'ပဉ္စမဆု',
  first3DigitsWinners: 'ပထမသုံးလုံးတူ',
  last3DigitsWinners: 'နောက်ဆုံးသုံးလုံးတူ',
  last2DigitsWinners: 'နောက်ဆုံးနှစ်လုံးတူ',

  // FAQ
  frequentlyAskedQues: 'အမေးများသော မေးခွန်းများ',
  faqDescrition: 'အောက်ပါမေးခွန်းများသည် Shwe Taik တွင် အမေးများသော မေးခွန်းများဖြစ်သည်',

  // Contact Us
  callUs: 'ဖုန်းခေါ်ရန််',
  emailUs: 'အီးမေးပို့ရန်',

  // Change Password
  oldPassword: 'လျှို့ဝှက်ကုဒ် အဟောင်း',
  newPassword: 'လျှို့ဝှက်ကုဒ် အသစ်',

  // Feedback
  giveFeedback: 'ထင်မြင်ချက် ရေးသားရန်',
  title: 'ခေါင်းစဉ်',
  detail: 'အသေးစိတ်',
  thanksForFeedback: 'ကျေးဇူးတင်ပါသည်',
  reviewFeedback: 'သင့်ထင်မြင်ချက်ကို ရီဗျူးလုပ်ပြီး တိုးတက်အောင် ကြိုးစားသွားပါမည်',
  anotherFeedback: 'နောက်ထင်မြင်ချက် ပေးမည်',

  // Recharge
  recharge: 'ပွိုင့်ဖြည့်မည်',
  history: 'မှတ်တမ်း',
  rechargeHistory: 'မှတ်တမ်း',
  scanToPay: 'ငွေလွှဲရန် စကန် ဖတ်ပါ',
  noWithdrawAccount: 'ငွေထုတ်အကောင့် မရှိပါ',
  uploadReceipt: 'စခရင်ရှော့ တင်မည်',
  rechargeSS: 'စခရင်ရှော့ တင်မည်',

     // Withdraw
     withdraw: 'ငွေထုတ်',
     point: 'ပွိုင့်',
     accountNo: 'အကောင့်နံပါတ်',
     accountName: 'အကောင့်အမည်',
     qrImage: 'ကျူအာကုဒ်',
     enterPasscode: 'ကျေးဇူးပြု၍ လျှို့ဝှက်နံပါတ်ကို ရိုက်ထည့်ပါ',
     withdrawHistory: 'မှတ်တမ်း',

  // Error Messages
  invalidName: 'အသုံးပြုသူအမည် ထည့်ပါ',
  invalidUsername: 'အမည် ထည့်ပါ',
  invalidPassword: 'လျှို့ဝှက်ကုဒ် ထည့်ပါ',
  invalidConfirmPassword: 'အတည်ပြုလျှို့ဝှက်ကုဒ် ထည့်ပါ',
  mismatchPassword: "အတည်ပြု လျှို့ဝှက်ကုဒ် မမှန်ကန်ပါ",
  invalidAgentCode: 'ကိုယ်စားလှယ်ကုဒ် ထည့်ပါ',
  invalidTicketNo: 'မှန်ကန်သော လက်မှတ်ထည့်ပါ',
  invalidTicketNos: 'မှန်ကန်သော လက်မှတ်များထည့်ပါ',
  noConnection: 'အင်တာနက်လိုင်း မရှိပါ',
  somethingWrong: 'တစ်ခုခု မှားယွင်းနေပါသည် နောက်တစ်ကြိမ် ပြန်လည်ကြိုးစားကြည့်ပါ',
  invalidOldPassword: 'လျှို့ဝှက်ကုဒ်အဟောင်း ထည့်ပါ',
  invalidNewPassword: 'လျှို့ဝှက်ကုဒ်အသစ် ထည့်ပါ',
  invalidFeedbackTitle: 'ခေါင်းစဉ် ထည့်ပါ',
  invalidFeedbackDetail: 'အသေးစိတ် ထည့်ပါ',
  invalidAccNo: 'အကောင့်နံပါတ် ထည့်ပါ',
  invalidAccName: 'အကောင့်အမည် ထည့်ပါ',
  invalidPoint: 'ငွေထုတ်ယူရန် ပွိုင့်ထည့်ပါ',
  invalidZeroPoint:  "သုညပွိုင့် မထုတ်ယူနိုင်ပါ",

  //Empty Messages
  noNotification: 'အသိပေးချက် မရှိပါ',
  noPurchasedTickets: 'လက်မှတ်၀ယ်ယူထားခြင်း မရှိပါ',
  noWinnerList: 'ထီပေါက်သူ မရှိပါ',
  noData: 'ဒေတာ မရှိပါ',
  noInternetConnection: 'အင်တာနက်လိုင်း မရှိပါ',
  noRecord: 'မှတ်တမ်းမရှိပါ',
  noUser: 'အသုံးပြုသူ မရှိပါ',

  // Buttons
  accept: 'လက်ခံမည်',
  decline: 'ငြင်းဆိုမည်',
  buy: '၀ယ်ယူမည်',
  submit: 'ပို့မည်',
  finish: 'ပြီးဆုံးမည်',
  ok: 'အိုခေ',
  update: 'ပြင်ဆင်မည်',
  reload: 'ပြန်လည်ခေါ်ယူမည်',
  cancel: 'မလုပ်ပါ',
  confirm: 'ပို့မည်',

  // Common
  loading: '',

  // Alert Messages
  successful: 'အောင်မြင်သည်',
  unauthenticated: 'အကောင့်ပြန်၀င်ပါ',
  duplicateTickets: '၀ယ်ယူ၍မရတော့သော လက်မှတ်များ',
  forceUpdateApp: 'Play Store တွင် နောက်ဆုံးထွက် ဗားရှင်းကို ဒေါင်းလုပ်ဆွဲပါ',
  sessionExpired: 'Session ကုန်ဆုံးသွားသည်',
  loginAgain: 'နောက်တစ်ကြိမ် အကောင့်၀င်ပါ',
  copied: 'ကူးယူသည်',
  tooManyAttempts: 'ခေါ်ဆိုမှု အရမ်းများနေပါသည်',
  requested: 'တောင်းဆိုထားသည်',
  completed: 'အောင်မြင်ပါသည်',
  rejected: 'ငြင်းပယ်ခံရသည်',
  refunded: 'ပြန်အမ်းထားသည်',
  cannotBuy: "ထီဖွင့်သောနေ့တွင် ထီထိုး၍မရပါ",
  transactionId: 'ငွေလွှဲနံပါတ်စဥ်',
  transferWith: 'ငွေလွှဲသည့်နည်းလမ်း', 
  rechargePoint: 'ဖြည့်သွင်းသောပွိုင့်',
  rechargeDate: 'အချိန်',
  rechargeStatus: 'ပွိုင့်ဖြည့်သွင်းမှုအခြေအနေ',
  ss: 'စခရင်ရှော့',
  yourQr: 'သင့်ကျူအာ',
  withdrawSS: 'ငွေထုတ်စခရင်ရှော့',
};
