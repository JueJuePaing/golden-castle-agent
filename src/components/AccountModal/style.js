import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  modalView: {
    backgroundColor: 'rgba(0,0,0, 0.8)',
    height: hp(100),
    justifyContent: 'center',
  },
  modalBg: {
      borderRadius: hp(1),
      width: wp(90),
      alignSelf: 'center'
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
   // height: hp(7),
    width: wp(90),
    paddingHorizontal: wp(3),
    paddingVertical: hp(3)
  },
  last : {
    borderBottomLeftRadius: hp(1),
    borderBottomRightRadius: hp(1)
  },
  first: {
    borderTopLeftRadius: hp(1),
    borderTopRightRadius: hp(1)
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  flag: {
    width: wp(7),
    height: wp(7),
    // borderRadius: wp(8)
  },
  name: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.6),
    color: '#ff9700',
    paddingLeft: wp(3)
  }
});
