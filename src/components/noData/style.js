import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  reloadContent : {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom : 20
},
nodata: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.8),
    color: '#ff9700',
    paddingBottom : hp(0.5)
}
});