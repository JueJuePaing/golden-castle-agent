import {StyleSheet, StatusBar} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  container : {
    height: hp(6),
    width: wp(100),
    paddingHorizontal: wp(3),
    backgroundColor: '#ff9700',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: StatusBar.currentHeight
  },
  buttons : {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  logo : {
    // width: wp(10), //12
    // height: wp(10),
    // resizeMode: 'contain',
    // borderRadius: wp(6)
  },
  nameContent : {
    paddingLeft: wp(2),
  },
  infoRow: {
    flexDirection: 'row',
    marginTop : hp(-0.5)
  },
  pointRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: wp(3)
},
point: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(2),
    color: '#fff',
    paddingLeft: wp(1),
    paddingTop: hp(.5)
},
  title: {
    fontSize : hp(1.9),
    fontFamily: 'Poppins-Medium',
    color: '#fff',
    // paddingTop : hp(.4)
  },
  detailHeader: {
    backgroundColor: '#ff9700',
    height: hp(6.5),
    width: wp(100),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: wp(3),
    borderBottomLeftRadius: hp(2),
    borderBottomRightRadius: hp(2),
    alignItems: 'center'
  },
  leftRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  detailTitle: {
    fontSize : hp(1.8),
    fontFamily: 'Poppins-Medium',
    color: '#fff',
    paddingTop: hp(.3),
    paddingLeft: wp(2)
  },
  dateBtn: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: hp(.5)
},
date: {
    fontFamily: 'Poppins-Regular',
    fontSize: hp(1.6),
    color: '#fff',
    paddingRight: wp(2),
    paddingTop: hp(.35)
},
currencyRow: {
  
},
currency: {
  fontFamily: 'Poppins-Regular',
  fontSize: hp(1.6),
  color: '#fff',
  paddingRight: wp(2),
  paddingTop: hp(.35)
},
header: {
  backgroundColor: '#ff9700',
  height: hp(5.5),
  width: wp(100),
  alignItems: 'center',
  justifyContent: 'center',
  marginTop : StatusBar.currentHeight,
  borderBottomLeftRadius: hp(2),
  borderBottomRightRadius: hp(2),
},
});
