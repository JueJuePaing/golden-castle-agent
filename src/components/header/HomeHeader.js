import React from 'react';
import {
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import AntDesign from "react-native-vector-icons/AntDesign"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Entypo from 'react-native-vector-icons/Entypo';

// Components
import styles from './style';
import Logo from '../../assets/icons/Logo';
import { useLocal } from '../../hook/useLocal';

export default function Header({
    loggedIn, 
    name, 
    ticketCount,
    point,
    lanHandler, 
    callCenterHandler
}) {
    const local = useLocal();
    return (
        <View style={styles.container}>
            <View style={styles.buttons}>
                {
                    loggedIn ? 
                    <FontAwesome
                        name="user-circle-o"
                        size={wp(8)}
                        color='#283e52'
                        style={styles.logo}  /> :
                    <Logo />
                }
                <View style={styles.nameContent}>
                    <Text style={styles.title}>
                        {loggedIn ? name : local.appName}
                    </Text> 
                </View>
            </View>

          
            
            <View style={styles.buttons}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={callCenterHandler}>
                    <AntDesign
                        name="customerservice"
                        size={22}
                        color='#fff'
                        />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={lanHandler}>
                    <FontAwesome
                        name="language"
                        size={22}
                        color='#fff'
                        style={{
                            marginLeft: 10
                        }}
                        />
                </TouchableOpacity>
            </View>

        </View>
    )
}
