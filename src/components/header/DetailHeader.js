import React, {
    useState
} from 'react';
import {
    View, 
    Image,
    Text,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons"
import AntDesign from "react-native-vector-icons/AntDesign"
import {
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
import { useLocal } from '../../hook/useLocal';

// Components
import styles from './style';

export default function DetailHeader({
    title,
    exchange_rate,
    goBack,
    showDate,
    date,
    rightBtn,
    rightBtnHandler
}) {
    const local = useLocal();
    return (
        <View style={[styles.detailHeader, {
            marginTop: title === local.users ? 0 : StatusBar.currentHeight
        }]}>
            <View style={styles.leftRow}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.backBtn}
                    onPress={goBack}>
                    <Ionicons
                        name="chevron-back-sharp"
                        size={hp(3)}
                        color="#fff" />
                </TouchableOpacity>
                <Text style={styles.detailTitle}>
                    { title }
                </Text>
            </View>
            {
                // title === "Purchase" || 
                title === local.userTickets ?
                <View style={styles.currencyRow}>
                    {
                        exchange_rate && <Text style={styles.currency}>
                           {`1 baht = ${exchange_rate} mmk`}
                        </Text>
                    }
                </View> :
                title === local.checkResult || title === local.winnerList ?
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.dateBtn}
                        onPress={showDate}>
                        <Text style={styles.date}>
                            { date }
                        </Text>
                        <AntDesign
                            name="caretdown"
                            size={hp(1.1)}
                            color="#11212f" />
                    </TouchableOpacity> : 
                rightBtn ?
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        style={styles.dateBtn}
                        onPress={rightBtnHandler}>
                        <Text style={styles.date}>
                            { rightBtn }
                        </Text>
                        <Ionicons
                            name="chevron-forward-sharp"
                            size={hp(1.8)}
                            color="#fff" />
                    </TouchableOpacity>
                     : null
            }
        </View>
    )
}