import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  separatorRow: {
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: wp(84),
    alignSelf: 'center',
    height: hp(3),
    paddingHorizontal: wp(5),
    marginVertical: hp(1.5)
},
separatorBlock : {
    width: wp(1.3),
    height: wp(1.2),
    borderRadius: wp(2),
    borderWidth: 1,
    borderColor: 'rgba(163, 166, 168, 0.5)'
},
curve: {
    height: hp(3),
    width: hp(4),
    backgroundColor: '#f1f1f1',
    borderRadius: hp(4),
    position: 'absolute',
    left: hp(-2.5)
},
rightCurve: {
    height: hp(3),
    width: hp(4),
    backgroundColor: '#f1f1f1',
    borderRadius: hp(4),
    position: 'absolute',
    right: hp(-2.5)
}
});
