import React, {
  useContext,
  useState,
  useRef
} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    Modal,
    TextInput
} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp
} from 'react-native-responsive-screen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { useLocal } from '../../hook/useLocal';
import { Context } from '../../context/Provider';
import { onlySpaces } from '../../utils/validation';

// Components
import styles from './style';
import ErrorMessage from '../errorMessage/ErrorMessage';

export default function PasswordModal({
    point,
    modalVisible,
    closeModalHandler,
    submitHandler
}) {
    const { 
        lang
    } = useContext(Context);
    const local = useLocal();

    const [error, setError] = useState(false);
    const [password, setPassword] = useState('');

    const confirmHandler = () => {
      if (onlySpaces(password)) {
        setError(true)
      } else {
        submitHandler(password);
      }
    }

    return (
        <Modal
          transparent={true}
          isVisible={modalVisible}
          animationIn="fadeIn"
          animationOut="fadeOut"
          animationInTiming={10}
          animationOutTiming={10}
          onRequestClose={closeModalHandler}>
          <View style={styles.modalContainerMain}>
            <View style={styles.modalContainer}>
              <View style={styles.modalTitle}>
                {/* <TouchableOpacity
                  style={styles.close}
                  onPress={closeModalHandler}>
                  <Close />
                </TouchableOpacity> */}
                <Text
                  style={
                    lang == 'mm'
                      ? {...styles.title, fontSize: hp(1.65)}
                      : styles.title
                  }>
                  {local.enterPasscode}
                </Text>
              </View>

              <View style={styles.descContent}>
                <View style={styles.pointContent}>
                  <FontAwesome5
                    name="coins"
                    size={hp(1.8)}
                    color='#ff9700' />
                  <Text style={styles.point}>
                    {`${point ? point : 0} ${local.point}`}
                  </Text>
                </View>
                {/* <Text style={styles.currency}>
                  {`1 baht = ${exchange_rate} mmk`}
                </Text> */}
              </View>

              <TextInput
                  placeholder={local.password}
                  value={password}
                  onChangeText={ (val) => {
                    setPassword(val);
                    setError(false);
                  } }
                  maxLength={30}
                  style={[styles.inputText, {
                    fontSize: password === '' ? hp(1.6) : hp(1.8)
                  }]}
                  autoFocus={true}
                  secureTextEntry={true} />
                {error && <ErrorMessage message={'Enter password'} width={wp(82)} />}

              <View style={styles.footer}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={ styles.cancelBtn }
                    onPress={ () => {
                      setPassword('');
                      closeModalHandler();
                    } }>
                    <Text style={styles.cancelTxt}>{local.cancel}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={ styles.confirmBtn }
                    onPress={ confirmHandler }>
                    <Text style={styles.confirmTxt}>{local.confirm}</Text>
                </TouchableOpacity>
            </View>
            </View>
          </View>
        </Modal>
    )
}