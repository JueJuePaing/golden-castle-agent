import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    width: wp(100),
    alignSelf: 'center',
    marginTop: hp(2.5)
},
dateContent: {
    width: wp(20),
    alignItems: 'center',
    borderRightColor: '#11212f'
},
date: {
    fontSize :hp(3),
    color:'#ff9700',
    fontFamily: 'Poppins-Medium',
    marginBottom: hp(-.5)
},
month: {
    fontSize :hp(1.5),
    color:'#000',
    fontFamily: 'Poppins-Regular'
},
coming: {
    fontSize :hp(1.3),
    color:'#11212f',
    fontFamily: 'Poppins-Medium'
},
ticketContent: {
    width: wp(75)
},
ticketBlock: {
    width: wp(75),
    backgroundColor: '#fff',
    marginBottom: hp(1),
    paddingHorizontal: wp(4),
    paddingVertical: hp(1),
    borderRadius: hp(1),
    overflow: 'hidden'
},
ticket: {
    flexDirection: 'row',
    alignItems:'center',
    // alignSelf: 'center',
    marginBottom: hp(-1.5),
    paddingTop: hp(.5),
    justifyContent: 'space-between'
},
ticketnoContent: {
    flexDirection: 'row',
    alignItems:'center'
},
ticketNo: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.8),
    letterSpacing: 2,
    paddingLeft: wp(3),
    color: '#ff9700'
},
username: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.3),
    color: '#ff9700'
},
ticketFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: hp(-1.5)
},  
row: {
    flexDirection: 'row',
    alignItems: 'center',
},
price: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.7),
    paddingLeft: wp(2),
    color: '#5c4e3a'
},
ticketDate: {
    fontFamily: 'Poppins-Regular',
    fontSize: hp(1.4),
    paddingLeft: wp(1),
    color: 'darkgray'
},
won: {
    position: 'absolute',
    top: hp(.6),
    right: wp(2)
   // padding: hp(1)
}
});