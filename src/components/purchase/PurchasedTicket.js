import React from 'react';
import styles from './style';
import {View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons'

import Ticket from '../../assets/icons/Ticket2';
import TicketSeparator from "../separator/TicketSeparator";
import {useLocal} from "../../hook/useLocal";

export default PurchasedTicket = ({
  day,
  month,
  coming,
  tickets
}) => {
    const local = useLocal();
  return (
  <View style={[styles.content, {marginTop: 0}]}>
      <View style={styles.dateContent}>
          <Text style={styles.date}>
              {day}
          </Text>
          <Text style={styles.month}>
            {month}
          </Text>
          {/* {coming && <Text style={styles.coming}>
            {`(${local.coming})`}
          </Text>} */}
      </View>
      <View style={styles.ticketContent}>
          {
              tickets.map((ticket,index) => {
                  return (
                      <View key={index} style={styles.ticketBlock}>
                          <View style={styles.ticket}>
                            <View style={styles.ticketnoContent}>
                                <Ticket />
                                <Text style={styles.ticketNo}>
                                      { ticket.number }
                                </Text>
                            </View>
                                <Text style={styles.username}>
                                    {`@${ticket.user_name}`}
                                </Text>
                          </View>

                          <TicketSeparator width={wp(75)} />
                              
                          <View style={styles.ticketFooter}>
                              <View style={styles.row}>
                                  <Ionicons
                                      name='pricetags'
                                      size={hp(1.6)}
                                      color='#756856' />
                                  <Text style={styles.price}>
                                      {`${ticket.amount} ${"\u0E3F"}`}
                                  </Text>
                              </View>
                              <View style={styles.row}>
                                  <Ionicons
                                      name='time'
                                      size={hp(1.5)}
                                      color='#968e81' />
                                  <Text style={styles.ticketDate}>
                                      { ticket.purchased_date }
                                  </Text>
                              </View>
                          </View>
                      </View>
                  )
              })
          }
      </View>
  </View>
  );
};
