import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  countdownContent : {
    width: wp(92),
    borderRadius: hp(1),
    shadowOffset: {width: 5, height: 10},
    shadowColor: '#000',
    shadowOpacity: 0.5,
    shadowRadius: hp(1),
    elevation: 3,
    alignSelf: 'center',
    backgroundColor:'#fff',
    paddingHorizontal: wp(2), // padding: 10
    marginVertical: hp(2),
    zIndex: 2,
    height: hp(11),
    justifyContent: 'center'
  },
  countdown: {
      flexDirection: 'row',
      alignItems: 'center',
     // justifyContent: 'space-between',
      height: hp(11) // 9.2
  },
  date: {
      width: wp(25),
      height: hp(8),
      alignSelf: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      borderRightWidth: 0.6,
      borderRightColor: '#11212f'
  },
  day: {
      fontSize :hp(3),
      color:'#ff9700',
      fontFamily: 'Poppins-Medium',
      marginBottom: hp(-.5)
  },
  month: {
      fontSize :hp(1.5),
      color:'#000',
      fontFamily: 'Poppins-Regular'
  },
  time:{
      width: wp(62),
      alignItems: 'center'
  },
  label: {
      fontSize :hp(1.2),
      color:'gray',
      fontFamily: 'Poppins-Regular',
      paddingBottom: 1
  },
  row: {
      flexDirection: 'row',
  },
  col: {
      backgroundColor: '#11212f',
      width: wp(8),
      height: wp(10),
      borderRadius: 4,
      justifyContent: 'center',
      alignItems:'center'
  },
  col2: {
    height: wp(10),
    justifyContent:'center'
  },
  data: {
    fontSize :hp(1.5),
    color:'#fff',
    fontFamily: 'Poppins-SemiBold'
  },
  bottomCol: {
      width: wp(8),
      //height: wp(5),
      justifyContent: 'center',
      alignItems:'center',
      //backgroundColor: 'red'
  },
  timeStatus: {
      fontSize :hp(1.1),
      color:'#11212f',
      fontFamily: 'Poppins-Medium',
  },

  moreBtn: {
    width: wp(4.3),
    height: wp(4.3),
    backgroundColor: '#ff9700',
    borderRadius: wp(4),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 3
  },
  buyBtn: {
      width: wp(16),
      height: hp(2.9),
      backgroundColor: '#11212f',
      borderRadius: hp(3),
      alignSelf: 'flex-end',
      flexDirection:'row',
      alignItems: 'center',
      marginBottom: wp(5)
  },
  buy: {
      fontSize :hp(1.5),
      color:'#fff',
      paddingLeft: 10,
      textTransform: 'uppercase'
  },
});
