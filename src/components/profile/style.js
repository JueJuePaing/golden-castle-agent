import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  menu: {
    flexDirection: 'row',
    alignItems:'center',
    justifyContent: 'space-between',
    paddingHorizontal: wp(4),
    paddingVertical: hp(1.5),
    borderBottomColor: 'rgba(17, 33, 47, 0.2)'
  },
  left: {
    flexDirection: 'row',
    alignItems:'center'
  },
  icon: {
    width: hp(3)
  },
  title: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.6),
    color: '#ff9700',
    paddingLeft: wp(2),
    paddingTop: hp(.4)
  }
});
