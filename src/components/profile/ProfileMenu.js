import React from 'react';
import {
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import {
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
import Entypo from "react-native-vector-icons/Entypo";

// Components
import styles from './style';

export default function ProfileMenu({
    pressHandler,
    icon,
    menuTitle,
    latest
}) {

    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={[styles.menu, { borderBottomWidth: latest ? 0 : .5 }]}
            onPress={ pressHandler }>
            <View style={styles.left}>
                <View style={styles.icon}>
                    { icon }
                </View>
                <Text style={styles.title}>
                    { menuTitle }
                </Text>
            </View>
            <Entypo
                name="chevron-small-right"
                size={hp(2)}
                color="#11212f" />
        </TouchableOpacity>
    )
}