import React, {useContext} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    Image,
    Modal
} from 'react-native';
import Entypo from "react-native-vector-icons/Entypo";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp
} from 'react-native-responsive-screen';
import { Context } from '../../context/Provider';
import { setItem } from '../../utils/appStorage';
// Components
import styles from './style';

export default function Language({
    closeModalHandler,
    lanHandler
}) {
    const { 
        lang,
        changeLang
    } = useContext(Context);

    // const changeLan = (lan) => {
    //     changeLang(lan);
    //     setItem('@lang', lan);
    //     lanHandler();
    // }

    return (
        <Modal
            transparent={true}
            visible={true}
            onRequestClose={closeModalHandler}
            animationType="fade">
            <TouchableOpacity activeOpacity={1} onPressOut={closeModalHandler}>
                <View style={styles.modalView}>
                    <View style={styles.modalBg}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={()=> lanHandler('en')}
                            style={[styles.item, styles.eng, {
                                backgroundColor: lang === "en" ? "#fff" : '#e3dede',

                            }]}>
                            <View style={styles.row}>
                                <Image
                                    source={require('../../assets/images/engflag.png')}
                                    style={styles.flag} />
                                <Text style={styles.name}>English</Text>
                            </View>
                            {
                                lang === "en" && <Entypo
                                    name="check"
                                    size={hp(2.3)}
                                    color='#ff9700' />
                            }
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={()=> lanHandler('mm')}
                            style={[styles.item, styles.mm, {
                                backgroundColor: lang === "mm" ? "#fff" : '#e3dede'
                            }]}>
                            <View style={styles.row}>
                                <Image
                                    source={require('../../assets/images/mmflag.png')}
                                    style={styles.flag} />
                                <Text style={styles.name}>Myanmar</Text>
                            </View>
                            {
                                lang === "mm" && <Entypo
                                    name="check"
                                    size={hp(2.3)}
                                    color='#ff9700' />
                            }
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        </Modal>
    )
}