import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp, widthPercentageToDP as wp
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    position: 'absolute',
    bottom: 0,
    height: hp(18),
    backgroundColor: '#fff',
    zIndex: 5,
    borderTopLeftRadius: hp(1),
    borderTopRightRadius: hp(1)
  },
  row: {
    flexDirection: 'row',
    width: wp(100),
  },
  numCol: {
    width: wp(25),
    height: hp(6),
    justifyContent:'center',
    alignItems: 'center',
    borderBottomWidth: .2,
    borderColor: 'lightgray',
    borderRightWidth: .2
  },
  numCol2: {
    width: wp(50),
    justifyContent:'center',
    alignItems: 'center',
    borderBottomWidth: .2,
    borderColor: 'lightgray',
    borderRightWidth: .2,
    height: hp(6),
  },
  num: {
    color: "#ff9700",
    fontSize: hp(2),
    fontFamily: 'Poppins-SemiBold'
  }
});
