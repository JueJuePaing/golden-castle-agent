import React from 'react';
import {
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import {
    heightPercentageToDP as hp, widthPercentageToDP as wp
  } from 'react-native-responsive-screen';

// Components
import styles from './style';

export default function NumKeyboard({ 
    pressKey,
    pressBack,
    marginBottom=60
 }) {

    return (
        <View style={[styles.container, {marginBottom}]}>
            <View style={styles.row}>
                <TouchableOpacity
                    onPress={()=> pressKey(1)}
                    style={styles.numCol}>
                    <Text style={styles.num}>1</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={()=> pressKey(2)}
                    style={styles.numCol}>
                    <Text style={styles.num}>2</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(3)}>
                    <Text style={styles.num}>3</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(4)}>
                    <Text style={styles.num}>4</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(5)}>
                    <Text style={styles.num}>5</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(6)}>
                    <Text style={styles.num}>6</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(7)}>
                    <Text style={styles.num}>7</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(8)}>
                    <Text style={styles.num}>8</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(9)}>
                    <Text style={styles.num}>9</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol}
                    onPress={()=> pressKey(0)}>
                    <Text style={styles.num}>0</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.numCol2}
                    onPress={()=> pressBack()}>
                    <Ionicons
                        name="backspace-sharp"
                        size={hp(3.5)}
                        color="#ff9700" />
                </TouchableOpacity>
            </View>
        </View>
    )
}