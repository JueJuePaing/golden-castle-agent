import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  loadingView: {
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    width: wp(100),
    height: hp(100),
    alignItems: 'center',
    position: 'absolute',
    justifyContent: 'center',
    zIndex : 5
  },
  container: {
    flexDirection: 'row'
  },
  loadingView: {
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    width: wp(100),
    height: hp(100),
    alignItems: 'center',
    position: 'absolute',
    justifyContent: 'center',
    zIndex : 5
  },
  circle: {
    width: 30,
    height: 30,
    borderRadius: 35,
    elevation: 2,
  },
  redCircle: {
    backgroundColor: '#ff9700',
  },
  greenCircle: {
    backgroundColor: '#11212f',
  }
});