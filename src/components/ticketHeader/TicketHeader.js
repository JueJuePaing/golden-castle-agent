import React from 'react';
import {
    View, 
    Text
} from 'react-native';

// Components
import styles from './style';

export default function TicketHeader({ title }) {

    return (
        <View style={styles.titleContent}>
            <View style={styles.small} />
            <View style={styles.big} />
            <Text style={styles.title}>
                { title }
            </Text>
            <View style={[styles.big, {marginLeft: 0, marginRight: 2}]} />
            <View style={styles.small} />
        </View>
    )
}