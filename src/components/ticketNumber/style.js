import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  winningNumContent: {
    width: wp(76),
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignSelf: 'center',
    marginBottom: hp(.7)
},
  winningNumBlock: {
    width: wp(10),
    height: wp(9),
    borderRadius: wp(4.7),
    backgroundColor: '#ff9700',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#11212f'
},
winningNum: {
  fontFamily: 'Poppins-Regular',
  fontSize: hp(1.6),
  color: '#fff',
  paddingTop: 3
},
});
