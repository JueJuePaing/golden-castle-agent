import React from 'react';
import {
    View, 
    Text
} from 'react-native';
import {
    widthPercentageToDP as wp
  } from 'react-native-responsive-screen';

// Components
import styles from './style';

export default function TicketNumber({number, width = wp(76), color="#ff9700"}) {

    return (
        <View style={[styles.winningNumContent, {width}]}>
            <View style={[styles.winningNumBlock, {backgroundColor: color}]}>
                <Text style={styles.winningNum}>
                    {number.charAt(0) ? number.charAt(0) : "-"}
                </Text>
            </View>
            <View style={[styles.winningNumBlock, {backgroundColor: color}]}>
                <Text style={styles.winningNum}>
                    {number.charAt(1) ? number.charAt(1) : "-"}
                </Text>
            </View>
            <View style={[styles.winningNumBlock, {backgroundColor: color}]}>
                <Text style={styles.winningNum}>
                    {number.charAt(2) ? number.charAt(2) : "-"}
                </Text>
            </View>
            <View style={[styles.winningNumBlock, {backgroundColor: color}]}>
                <Text style={styles.winningNum}>
                    {number.charAt(3) ? number.charAt(3) : "-"}
                </Text>
            </View>
            <View style={[styles.winningNumBlock, {backgroundColor: color}]}>
                <Text style={styles.winningNum}>
                    {number.charAt(4) ? number.charAt(4) : "-"}
                </Text>
            </View>
            <View style={[styles.winningNumBlock, {backgroundColor: color}]}>
                <Text style={styles.winningNum}>
                    {number.charAt(5) ? number.charAt(5) : "-"}
                </Text>
            </View>
        </View>
    )
}