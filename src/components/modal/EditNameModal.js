import React, {
    useState
} from 'react';
import {
    View, 
    TouchableOpacity,
    Modal,
    Text,
    TextInput,
} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import AntDesign from "react-native-vector-icons/AntDesign";
import ErrorMessage from '../errorMessage/ErrorMessage';

// Components
import styles from './style';
import { useLocal } from '../../hook/useLocal';

export default function EditNameModal({ 
    defaultName,
    closeModalHandler,
    submitHandler
 }) {

    const local = useLocal();

    const [
        editName,
        setEditName
    ] = useState(defaultName);
    const [
        errMsg,
        setErrMsg
    ] = useState();
    const [
        focused,
        setFocused
    ] = useState(false);

    const onSubmit =() => {
        if (editName === "") {
            setErrMsg(local.invalidName);
        } else {
            submitHandler(editName);
        }
    }

    return (
        <Modal
            transparent={true}
            visible={true}
            onRequestClose={closeModalHandler}
            animationType="fade">
            <TouchableOpacity activeOpacity={1} onPressOut={closeModalHandler}>
                <View style={styles.modalView}>
                    <TouchableOpacity
                        onPress={ closeModalHandler }
                        style={ styles.close_btn }>
                        <AntDesign 
                            name="closecircle"
                            size={hp(3)}
                            color={"gray"} />
                        </TouchableOpacity>
                    <View style={styles.editModalBg}>
                        <Text style={styles.title}>
                           {local.updateName}
                        </Text>
                        <TextInput
                            placeholder={local.name}
                            placeholderTextColor="#c9c7c3"
                            value={editName}
                            onChangeText={ val => {
                                setEditName(val);
                                setErrMsg();
                            } }
                            maxLength={30}
                            onFocus={()=> setFocused(true)}
                            onBlur={()=> setFocused(false)}
                            style={[styles.inputText, {
                                borderColor: focused ? "#11212f" : 'lightgray',
                                fontSize: editName === "" ? hp(1.5) : hp(1.7)
                            }]} />
                        <ErrorMessage message={errMsg} width={wp(70)} />
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={[styles.submitBtn, {marginTop : errMsg ? hp(2) : hp(1)}]}
                            onPress={ onSubmit }>
                            <Text style={styles.submitTxt}>{local.submit}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        </Modal>
    )
}
