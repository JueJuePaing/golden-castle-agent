import React from 'react';
import {
    View, 
    TouchableOpacity,
    Modal,
    Text
} from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Entypo from "react-native-vector-icons/Entypo";

// Components
import styles from './style';
import { useLocal } from '../../hook/useLocal';

export default function ForceUpdate({
    updateHandler
 }) {
    const local = useLocal();
    return (
        <Modal
            transparent={true}
            visible={true}
            animationType="fade">
                <View style={styles.modalView}>
                    <View style={styles.sessionModalBg}>
                        <Entypo
                            name='warning'
                            color='red'
                            size={hp(2.5)} />
                        <Text style={ styles.update }>
                            {local.forceUpdateApp}
                        </Text>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.submitBtn }
                            onPress={ updateHandler }>
                            <Text style={styles.submitTxt}>{local.update}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </Modal>
    )
}
