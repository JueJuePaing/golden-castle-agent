import React from 'react';
import {
    View, 
    TouchableOpacity,
    Text
} from 'react-native';
// Components
import styles from './style';
import NoWifi from "../../assets/icons/NoWifi";
import { useLocal } from '../../hook/useLocal';

export default function NetworkProblem({
    reloadHandler
 }) {
    const local = useLocal();
    return (
        <View style={ styles.problemContainer }>
            <NoWifi />
            <Text style={ styles.network }>
                {local.noInternetConnection}
            </Text>
            <TouchableOpacity
                activeOpacity={0.8}
                style={ styles.submitBtn }
                onPress={ reloadHandler }>
                <Text style={styles.submitTxt}>{local.reload}</Text>
            </TouchableOpacity>
        </View>
    )
}
