import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
    modalView: {
        backgroundColor: 'rgba(0,0,0, 0.8)',
        height: hp(100),
        justifyContent: 'center',
    },
    editModalBg: {
        backgroundColor: '#fff',
        borderRadius: hp(1),
        width: wp(90),
        alignSelf: 'center',
        marginBottom: hp(5)
    },
    modalBg: {
        backgroundColor: '#fff',
        borderRadius: hp(1),
        width: wp(60),
        alignSelf: 'center',
        maxHeight: hp(60),
        marginBottom: hp(5)
    },
    option: {
        width: wp(60),
        height: hp(5),
        justifyContent:'center',
        alignItems: 'center',
        borderBottomWidth: .5,
        borderBottomColor: 'lightgray'
    },
    title : {
        fontFamily: 'Poppins-Medium',
        fontSize : hp(1.8),
        textAlign: 'center',
        paddingVertical: hp(2),
        color: '#ff9700'
    },
    inputText: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#000',
        paddingVertical: 0,
        width: wp(80),
        borderWidth: 1,
        height: hp(4.8),
        borderRadius: hp(4),
        borderColor: 'lightgray',
        paddingLeft: wp(4),
        paddingTop: hp(.3),
        alignSelf: 'center'
    },
    submitBtn: {
        alignSelf: 'center',
        height: hp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff9700',
        borderRadius: hp(3),
        marginTop: hp(1),
        paddingHorizontal: wp(6),
        borderWidth: 1,
        borderColor: '#11212f',


        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        marginBottom : hp(2)
    },
    submitTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#fff',
        paddingTop: hp(.5),
        textTransform: 'uppercase'
    },
    close_btn: {
        alignSelf: 'center',
        marginBottom : hp(2)
    },
    sessionExpired: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#ff9700',
        paddingTop: hp(1)
    },
    sessionModalBg: {
        backgroundColor: '#fff',
        borderRadius: hp(1),
        width: wp(90),
        alignSelf: 'center',
        marginBottom: hp(5),
        alignItems: 'center',
        paddingTop : hp(2)
    },
    descrition: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#ff9700'
    },
    alertMessage: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#11212f',
        paddingHorizontal : wp(2),
        paddingBottom: hp(1.5)
    },
    problemContainer: {
        width: wp(100),
        height: hp(100),
        alignItems: 'center',
        justifyContent: 'center'
    },
    network: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.8),
        color: '#ff9700',
        paddingVertical : hp(2)
    },
    update: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#ff9700',
        paddingTop: hp(1),
        paddingHorizontal : wp(2),
        paddingBottom : hp(2),
        textAlign: 'center'
    },
})
