import React, {useState} from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

// Components
import styles from './style';

const SLIDER_1_FIRST_ITEM = 0;

export default function slideImage({data, height}) {
  const [activeSlide, setActiveSlide] = useState(0);


  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.renderImage}
        activeOpacity={0.9}
        // onPress={() => actionBanner(item)}
        >
          <Image source={item.photo} style={styles.renderImage} />
      </TouchableOpacity>
    );
  };

  return (
    <>
      <View style={[styles.container, {height: height}]}>
          <Carousel
            layout={'default'}
            loop={true}
            autoplay={true}
            autoplayDelay={4000}
            autoplayInterval={8000}
            activeAnimationType={'timing'}
            firstItem={SLIDER_1_FIRST_ITEM}
            inactiveSlideScale={0.9}
            inactiveSlideOpacity={0.7}
            inactiveSlideShift={0.6}
            sliderWidth={wp(100)}
            itemWidth={wp(80)}
            pagingEnabled={true}
            loopClonesPerSide={data.length}
            data={data}
            renderItem={_renderItem}
            onSnapToItem={index => setActiveSlide(index)}
          />
      </View>
      {/* <Pagination
        dotsLength={data ? data.length : 1}
        activeDotIndex={activeSlide}
        dotContainerStyle={styles.dotContainer}
        containerStyle={styles.ContainerStyle}
        dotStyle={styles.dotStyle}
        inactiveDotStyle={styles.inDotStyle}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.7}
      /> */}
    </>
  );
}
