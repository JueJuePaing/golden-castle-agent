import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  ticket: {
    height: hp(22),
    width: wp(94),
    marginTop: hp(1.5),
    alignSelf: 'center'
  },
  bgImg: {
    height: hp(22),
    width: wp(94),
    position: 'absolute',
    borderRadius: hp(0.5),
    resizeMode: 'cover'
  },
  row : {
    flexDirection: 'row',
    width: wp(92),
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: wp(3),
    paddingRight: wp(15)
  },
  ticketImg: {
    width: wp(37),
    height: hp(19),
    resizeMode : 'contain'
  },
  middleContent: {
    width: wp(41),
    height: hp(22),
   // justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: hp(2),
    marginRight: wp(-5),
    // backgroundColor: 'pink'
  },
  numberBgImg: {
    width: wp(41),
    height: hp(4.5),
    position: 'absolute',
    resizeMode: 'cover'
  },
  ticketNoContent: {
    width: wp(40),
    height: hp(4.5),
    alignItems: 'center',
    overflow: 'hidden',
    // backgroundColor: 'purple'
    
  },
  naContent: {
    width: wp(40),
    height: hp(4.5),
    alignItems: 'center',
    justifyContent: 'center'
  },
  naTxt : {
    color: '#11212f',
    fontFamily: 'Poppins-Medium',
  },
  test : {
    height: hp(4.5),
    justifyContent: 'center',
    alignItems: 'center'
  },
  ticketNo: {
    color: '#11212f',
    fontFamily: 'Poppins-Medium',
    fontSize: hp(2.3),
    letterSpacing: 8,

    includeFontPadding: false,
    // backgroundColor: 'red',
    lineHeight: hp(4.5),
    // paddingTop: hp(.15)
  },
  barcodeNo : {
    color: '#1a2936',
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.7)
  },
  date: {
    fontFamily: 'Poppins-Medium',
    color: '#000',
    fontSize: hp(1.7),
    color: '#11212f',
    paddingTop: hp(3.5),
    letterSpacing: 1
  },
  barcode: {
    width: wp(41),
    position: 'absolute',
    bottom: hp(1.5),
    alignItems : 'center'
  },
  rightCol: {
    width: wp(8),
    height: hp(20),
    backgroundColor: '#11212f'
  },
  rightCol2: {
    width: wp(8),
    height: hp(20),
    backgroundColor: 'rgba(17, 33, 47, 0.3)'
  },
  live : {
    color: '#ff9700',
    fontStyle: 'italic'  
  },
  liveContent : {
    flexDirection: "row",
    transform: [
      {
        rotate : '90deg'
      }
    ],
    position : "absolute",
    right: 0,
    alignItems: 'center',
    height: hp(20),
    width: wp(8),
  },
  dot: {
    backgroundColor: '#ff9700',
    width: wp(2),
    height: wp(2),
    borderRadius: hp(2),
    marginLeft : wp(1)
  },
  sidelineImg: {
    width: wp(8),
    height: hp(22),
    position: 'absolute',
    right: 0,
    resizeMode: 'cover'
  }
});
