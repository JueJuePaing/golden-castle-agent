import React, {
    useEffect,
    useState
} from 'react';
import {
    View, 
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import Animated, { 
    useSharedValue, 
    useAnimatedStyle, 
    withTiming, 
    Easing
} from 'react-native-reanimated';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
import Barcode from '@kichiyaki/react-native-barcode-generator';

import { setItem, getItem } from '../../utils/appStorage';

// Components
import styles from './style';

export default function Ticket({
    data, 
    drawDate,
    live,
    buy,
    users,
    pressHandler,
    ticket_background,
    ticket_photo
}) {

    const [
        index,
        setIndex
    ] = useState(1);
    const [
        tickets,
        setTickets
    ] = useState();
    const [
        lastDrawIndex,
        setLastDrawIndex
    ] = useState(1);
    const [
        barcodeNo,
        setBarcodeNo
    ] = useState('01234567890123456');

    const generateRandomNumberString = () => {
        let result = '';
        const characters = '0123456789';
      
        for (let i = 0; i < 17; i++) {
          const randomIndex = Math.floor(Math.random() * characters.length);
          result += characters.charAt(randomIndex);
        }
      
        return result;
      }

    let allTickets = buy ? data : users ? data?.user_tickets : data?.last_draw_tickets;
    const lastDraw = data?.last_draw_tickets ? true : false;
    const count = Math.ceil(allTickets?.length / 50);

    const top = useSharedValue(0);

    const animatedStyles = useAnimatedStyle(() => {
        return {
            marginTop: top.value
        };
    });

    const [
        ticketBg,
        setTicketBg
    ] = useState(null);
    const [
        ticketPhoto,
        setTicketPhoto
    ] = useState(null);

    useEffect( () => {
        const setTicketImages = async () => {
            setTicketBg(ticket_background);
            setTicketPhoto(ticket_photo)
            await setItem('@ticket_bg_img', ticket_background);
            await setItem('@ticket_photo', ticket_photo);
        }

        const getTicketImages = async () => {
            const ticket_bg_img = await getItem('@ticket_bg_img');
            const ticket_photo = await getItem('@ticket_photo');
          
            if (ticket_bg_img) setTicketBg(ticket_bg_img);
            if (ticket_photo) setTicketPhoto(ticket_photo);
        }

        if (ticket_background) setTicketImages();
        else getTicketImages();
    }, [ticket_background, ticket_photo]);

    useEffect(() => {
        if (!lastDraw && allTickets) {
            if (allTickets?.length > 50) {
                const firstNumbers = allTickets.slice(0,45);
                let lastNumbers = allTickets.slice(-5);
                setTickets([...firstNumbers, ...lastNumbers]);
                
            } else {
                setTickets(allTickets);
            }
            top.value = 0;
            setIndex(1);
        }
    }, [allTickets, lastDraw]);

    useEffect(() => {
        if (!lastDraw && allTickets && index === allTickets.length) {
            setTimeout(() => {
                top.value = 0;
                setIndex(1);
            }, 3000);
        }
    }, [allTickets, lastDraw, index])

    useEffect(() => {
        if (lastDraw && allTickets) {
            if (lastDrawIndex === 1) {
                setTickets(allTickets.slice(0, 50));
            } else if (lastDrawIndex === 2) {
                setTickets(allTickets.slice(50, 100));
            } else if (lastDrawIndex === 3) {
                setTickets(allTickets.slice(100, 150));
            } else if (lastDrawIndex === 4) {
                setTickets(allTickets.slice(150, 173));
            }
            top.value = 0;
            setIndex(1);
        }
    }, [allTickets, lastDraw, lastDrawIndex, count]);


    useEffect(() => {
        setBarcodeNo(generateRandomNumberString());
    }, []);

    useEffect(() => {
        const interval = setInterval(() => {
            setBarcodeNo(generateRandomNumberString());
            if (tickets && index === tickets.length) {
                if (lastDraw && lastDrawIndex < count) {
                    setLastDrawIndex(prev => prev + 1);
                } else if (lastDraw && lastDrawIndex === count) {
                    setLastDrawIndex(1);
                }
            }
            else {
                top.value = withTiming(hp(-4.5*index), {
                    duration: 1000,
                    easing: Easing.in(Easing.bounce)
                });
                setIndex(prev => prev+1);
            }
        }, 3000);
        return () => {
            if(interval) clearInterval(interval);
        };
    }, [index, tickets, lastDraw, lastDrawIndex, count]);

    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.ticket}
            onPress={ () => pressHandler ? live ? pressHandler(tickets?.[index - 1]) : pressHandler() : null }>
            <Image
                source={
                    ticketBg ? { uri : ticketBg } :
                    require('../../assets/images/ticketBg.png')}
                style={styles.bgImg} />
            <View style={styles.row}>
                <Image
                    source={
                        ticketPhoto ? { uri : ticketPhoto } : null
                    }
                    style={styles.ticketImg} />
                <View style={styles.middleContent}>
                    <View style={styles.ticketNoContent}>
                        <Image
                            source={require('../../assets/images/ticketNoBg.png')}
                            style={styles.numberBgImg} />
                            
                        {
                            tickets && tickets.length > 0 ? (
                                <Animated.View style={animatedStyles}>
                                    {
                                        tickets.map((ticket, ind) => {
                                            return <View style={styles.test} key={ind}>
                                                <Text includeFontPadding={false} style={[styles.ticketNo, {
                                                    paddingTop : index < 20 ? hp(.1) : 0
                                                }]}>
                                                    { ticket }
                                                </Text>
                                            </View>
                                        })
                                    }
                                </Animated.View>
                                ) :
                                !buy ?
                                <View style={ styles.naContent }>
                                    <Text  style={ styles.naTxt }></Text>
                                </View> : null
                        }
                        
                    </View>
                    <Text style={styles.date}>
                        { drawDate ? drawDate : '' }
                    </Text>

                    <View style={styles.barcode}>
                        <Barcode
                            value={drawDate ? drawDate : ''}
                            maxWidth={wp(40)}
                            height={hp(4)}
                        />
                        <Text numberOfLines={1} style={styles.barcodeNo}>{barcodeNo}</Text>
                    </View>
                </View>
                {/* {
                    live ? <View style={styles.rightCol} /> : <View style={styles.rightCol2} />
                }
                {
                    live && <View style={ styles.liveContent }>
                        <Text style={ styles.live }>LIVE</Text>
                        <View style={styles.dot} />
                    </View>
                } */}
            </View>
            <Image
                source={require("../../assets/images/LotterySideLine.png")}
                style={styles.sidelineImg} />
        </TouchableOpacity>
    )
}