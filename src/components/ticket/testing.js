import React, {
    useEffect,
    useState
} from 'react';
import {
    View, 
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import Animated, { 
    useSharedValue, 
    useAnimatedStyle, 
    withTiming, 
    Easing
} from 'react-native-reanimated';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Barcode from '@kichiyaki/react-native-barcode-generator';
import { setItem, getItem } from '../../utils/appStorage';

// Components
import styles from './style';

export default function Ticket({
    data, 
    drawDate,
    live,
    buy,
    users,
    pressHandler,
    ticket_background,
    ticket_photo
}) {

    const [
        index,
        setIndex
    ] = useState(1);
    const [
        ticketBg,
        setTicketBg
    ] = useState(null);
    const [
        ticketPhoto,
        setTicketPhoto
    ] = useState(null);

    useEffect( () => {
        const setTicketImages = async () => {
            setTicketBg(ticket_background);
            setTicketPhoto(ticket_photo)
            await setItem('@ticket_bg_img', ticket_background);
            await setItem('@ticket_photo', ticket_photo);
        }

        const getTicketImages = async () => {
            const ticket_bg_img = await getItem('@ticket_bg_img');
            const ticket_photo = await getItem('@ticket_photo');
          
            if (ticket_bg_img) setTicketBg(ticket_bg_img);
            if (ticket_photo) setTicketPhoto(ticket_photo);
        }

        if (ticket_background) setTicketImages();
        else getTicketImages();
    }, [ticket_background, ticket_photo]);

    const tickets = buy ? data : users ? data?.user_tickets : data?.last_draw_tickets;

    const top = useSharedValue(0);

    const animatedStyles = useAnimatedStyle(() => {
        return {
            marginTop: top.value
        };
    });

    useEffect(() => {
        const interval = setInterval(() => {
            if (tickets && index === tickets.length) {
                top.value = 0;
                setIndex(1);
            }
            else {
                top.value = withTiming(hp(-4.5*index), {
                    duration: 3000,
                    easing: Easing.in(Easing.bounce)
                });
                setIndex(prev => prev+1);
            }
        }, 3000);
        return () => {
            if(interval) clearInterval(interval);
        };
    }, [index, tickets]);
    
    // useEffect(() => {
    //     top.value = withTiming(hp(-4.5*2), {
    //         duration: 3000,
    //        // easing: Easing.in(Easing.bounce)
    //     });

    // }, []);

    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.ticket}
            onPress={ pressHandler ? pressHandler : null }>
            <Image
                source={ 
                    !ticketBg ? { uri : ticketBg } :
                    require('../../assets/images/ticketBg.png')}
                style={styles.bgImg} />
            <View style={styles.row}>
                <Image
                    source={
                        ticketPhoto ? { uri : ticketPhoto } : null
                    }
                    style={styles.ticketImg} />
                <View style={styles.middleContent}>
                    <View style={styles.ticketNoContent}>
                        <Image
                            source={require('../../assets/images/ticketNoBg.png')}
                            style={styles.numberBgImg} />
                        {
                            tickets && tickets.length > 0 ? (
                                <Animated.View style={animatedStyles}>
                                    {
                                        tickets.map((ticket, index) => {
                                           // const ticketString = ticket.toString().padEnd(6, '-');
                                            return <Text key={index} style={styles.ticketNo}>
                                                { ticket }
                                            </Text>
                                        })
                                    }
                                </Animated.View>) :
                                !buy ?
                                <View style={ styles.naContent }>
                                    <Text  style={ styles.naTxt }>N/A</Text>
                                </View> : null
                        }
                    </View>
                    <Text style={styles.date}>
                        { drawDate ? drawDate : 'N/A' }
                    </Text>

                    <View style={styles.barcode}>
                        <Barcode
                            value={drawDate ? drawDate : 'N/A'}
                            maxWidth={wp(40)}
                            height={hp(4)}
                        />
                    </View>
                </View>
       
                {/* {
                    live ? <View style={styles.rightCol} /> : <View style={styles.rightCol2} />
                }
                {
                    live && <View style={ styles.liveContent }>
                        <Text style={ styles.live }>LIVE</Text>
                        <View style={styles.dot} />
                    </View>
                } */}
            </View>
            <Image
                source={require("../../assets/images/LotterySideLine.png")}
                style={styles.sidelineImg} />
        </TouchableOpacity>
    )
}