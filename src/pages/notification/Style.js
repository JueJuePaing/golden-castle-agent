import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    menu_list: {
       marginBottom : 150
    },
    item: {
        width: wp(100),
        paddingHorizontal: wp(3),
        paddingVertical: hp(2),
        backgroundColor: 'gray',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 4
    },
    separator: {
       // backgroundColor: '#a8aaad',
        width:wp(100),
        height: .3,
        position: 'absolute',
        bottom: 0
    },
    logo: {
        width: wp(10),
        height: wp(10),
        borderRadius: wp(10),
        marginRight: wp(2)
    },
    rightContent: {

    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#615f5f',
        width: wp(70)
    },
    time: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.3),
        color: '#a8aaad'
    },
    description: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.4),
        color: 'gray',
        width: wp(82)
    },
    dateContent: {
        paddingHorizontal: wp(3),
        paddingTop: hp(1.4),
        paddingBottom: hp(.6),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    date: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#ff9700'
    },
    mark: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#11212f'
    },
    detailContainer: {
        alignItems: 'center',
        backgroundColor: '#f4f9ff',
        width: wp(100),
        height: hp(100)
    },
    noti: {
        width: wp(80),
        height: hp(35),
        resizeMode: 'contain'
    },
    detailTitle: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2),
        color: '#ff9700',
        textAlign: 'center',
        paddingHorizontal: wp(7),
        paddingTop: hp(2)
    },
    detailDesc: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#ff9700',
        //textAlign: 'center',
        paddingHorizontal: wp(4),
        paddingTop: hp(2)
    },
    detailDateContent: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingHorizontal: wp(6),
        marginTop: hp(1.5),
        alignSelf: 'center'
    },
    detailDate: {
        color: '#11212f',
        fontSize: hp(1.6),
        paddingLeft: wp(1)
    },
    nodata: {
        marginTop : hp(25)
    }
})

export default styles;