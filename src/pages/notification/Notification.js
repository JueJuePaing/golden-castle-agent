import React, { 
    useState,
    useContext,
    useCallback
} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {
    View,
    Text,
    RefreshControl,
    ScrollView,
    FlatList,
    ToastAndroid,
    Image,
    DeviceEventEmitter,
    TouchableOpacity
} from 'react-native';
import moment from 'moment';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { fetchPostByToken, fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage'
import { useLocal } from '../../hook/useLocal';

import Header from '../../components/header/Header';
import styles from './Style';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import LoginModal from '../auth/Login';
import SessionExpired from '../../components/modal/SessionExpired';
import NetworkProblem from '../../components/modal/NetworkProblem';
import { decryptData } from '../../utils/decryptData';
import { encryptData } from '../../utils/encryptData';

const Notification = ({navigation}) => {

    const { 
        changeUserInfo,
        userInfo,
        lang,
        net
    } = useContext(Context);

    const local = useLocal();
    const lanData = {
        language : lang
    }


    const [
        refreshing,
        setRefreshing
      ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        page,
        setPage
    ] = useState(1);
    const [
        notifications,
        setNotifications
    ] = useState();
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        showLogin,
        setShowLogin
    ] = useState(false);
    const [
        hasMore,
        setHasMore
    ] = useState(true);

    useFocusEffect(
        useCallback(() => {
            setNoData(false);
            setLoading(true);
            getNotifications(1)
        }, [userInfo, expired])
      );

    const getNotifications = async (noPage) => {
        if (userInfo?.access_token) {
            const response = await fetchGetByToken(`${apiUrl.notifications}?page=${noPage}`, userInfo.access_token);

            if (response?.status === 200 && response?.data) {
                const decNotis = JSON.parse(decryptData(response.data, userInfo.secret_key));
                if (decNotis?.notifications?.length > 0) {
                    if (noPage === 1) {
                        setNotifications(decNotis.notifications);
                    } else {
                        const lastDate = notifications[notifications.length - 1].date;
                        const firstDate = decNotis.notifications[0].date;
    
                        if (lastDate === firstDate) {
                            let arr = [];
                            notifications.map((notification, index) => {
                                if (index !== notifications.length - 1) {
                                    arr.push(notification)
                                }
                            })
                            let temp = {
                                date : notifications[notifications.length - 1].date,
                                data : [
                                    ...notifications[notifications.length - 1].data,
                                    ...decNotis.notifications[0].data
                                ]
                            }
                            arr.push(temp);
                            decNotis.notifications.map((notification, index) => {
                                if (index !== 0) {
                                    arr.push(notification);
                                }
                            })
                            setNotifications(arr);
                        } else {
                            setNotifications(prev => [
                                ...prev,
                                ...decNotis.notifications
                            ])
                        }
                    }
                    setRefreshing(false);
                }  else {
                    if (noPage === 1) {
                        setNotifications([]);
                        setNoData(true); 
                    } else {
                        setHasMore(false);
                    }
                    setRefreshing(false);
                }
            } else {
                if (response?.status === 429 || response?.message === "Too Many Attempts.") {
                    console.error("TOO MANY ATTEMPTS!!!!");
                    ToastAndroid.show(local.tooManyAttempts, ToastAndroid.SHORT)
                    if (noPage === 1 && (!notifications || notifications.length < 1)) {
                        setNoData(true);
                    }
                } else {
                    if (noPage === 1) {
                        setNotifications([]);
                    }
                    if (response?.status === 401) {
                        // Unauthenticated
                        setExpired(true);
                    } else {
                        if (noPage === 1) {
                            setNoData(true); 
                        } else {
                            setHasMore(false);
                        }
                    }
                }
                setRefreshing(false);
            }

            setLoading(false);
        } else {
            setLoading(false);
            setRefreshing(false);
            if (!expired) {
                setShowLogin(true);
            }
        }
    }


    const loadMoreHandler = () => {
        if (hasMore) {
            getNotifications(page+1);
            setPage(page+1)
        }
    };

    const itemHandler = async (date, item) => {
        if (userInfo?.access_token) {
            setLoading(true);

            let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
            const response = await fetchPostByToken(`agent/notifications/${item.id}/read`, {data: encryptLanData}, userInfo.access_token);
            setLoading(false);
            if (response?.status === 401) {
                setExpired(true)
            } else {
                DeviceEventEmitter.emit('noti_count_change', 'true')
                navigation.navigate('NotificationDetail', {item, date});
            }   
        } else {
            navigation.navigate('NotificationDetail', {item, date});
        }
    }

    const allReadHandler = async () => {
        if (userInfo?.access_token) {
            setLoading(true);

            let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.markAllRead, {data: encryptLanData}, userInfo.access_token);

            if (response?.status === 401) {
                setExpired(true)
            } else {
                setLoading(true);
                getNotifications(1);
                DeviceEventEmitter.emit('noti_count_change', 'true')
            }  
        } else {
            ToastAndroid.show("Unauthenticated!", ToastAndroid.SHORT);
            setExpired(true);
        }
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const reloadHandler = () => {
        if (net) getInitialData();
        else {
            setLoading(true);
            getNotifications(1);
        }
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
    
        const loadData = async () => {
    
            setNoData(false);
            getNotifications(1);
        };
    
        loadData();
      }, [userInfo]);

    if (!net) {
        return <NetworkProblem reloadHandler={reloadHandler} />
    }
    
    return (
        <View>
            <Header title={local.notifications} />
            <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                      refreshing={ refreshing }
                      colors={[ "#11212f" ]}
                      onRefresh={ onRefresh }
                      progressViewOffset={hp(4)}  />
                  }>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    onEndReached={ () => loadMoreHandler() }
                    onEndReachedThreshold={0.1}
                    keyExtractor={(item, index) => index.toString()}
                    style={ styles.menu_list }
                    data={ notifications }
                    renderItem={({item, index}) => (
                        <View key={item.date}>
                            <View style={styles.dateContent}>
                                <Text style={styles.date}>
                                    {
                                        (item.date === 'Yesterday' || item.date === 'Today') ?
                                            item.date : moment(item.date).format('D MMM YYYY')
                                    }
                                </Text>
                                {
                                    index === 0 && <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={ allReadHandler }>
                                        <Text style={styles.mark}>
                                            {local.markAllRead}
                                        </Text>
                                    </TouchableOpacity>
                                }
                            </View> 
                            {
                                item.data.map((notification, index) => {
                                return <TouchableOpacity
                                    key={ index }
                                    activeOpacity={0.8}
                                    style={[ styles.item, {
                                        backgroundColor: notification.read ? "#fff" : "#f0eded"
                                    } ]}
                                    onPress={() => itemHandler(item.date, notification)}>
                                    <Image
                                        source={require('../../assets/images/logo.jpeg')}
                                        style={ styles.logo } />
                                    <View style={styles.rightContent}>
                                        <View style={styles.row}>
                                            <Text numberOfLines={1} style={styles.title}>
                                                { notification.title }
                                            </Text>
                                            <Text style={styles.time}>
                                                { notification.time }
                                            </Text>
                                        </View>
                                        <Text numberOfLines={1} style={styles.description}>
                                            { notification.message }
                                        </Text>
                                    </View>
                                    <View style={styles.separator} />
                                </TouchableOpacity> 
                                })
                            }
                        </View>
                    )}/>
            </ScrollView>
            
            {
                !loading && noData && <View style={ styles.nodata }>
                    <NoData 
                        message={local.noNotification}
                        reloadHandler={ () => {
                            setLoading(true);
                            getNotifications(1)
                        } } />
                </View>
                
            }
            {loading && <Loading />}
            {
                showLogin && (
                    <LoginModal
                        onClose={()=> {
                            setShowLogin(false);
                            navigation.navigate('Profile')
                        }}
                        defaultTab={ 1 } />
                )
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        setLoading(false);
                        logoutHandler();
                    }}/>
            }
        </View>
    )
}

export default Notification;