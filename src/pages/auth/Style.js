import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    modalView: {
        backgroundColor: '#f1f1f1',
        height: hp(100),
        width: wp(100)
    },
    upperContainer: {
        height: hp(50),
        width: wp(100),
        backgroundColor: '#ff9700'
    },
    backBtn: {
        padding: wp(5)
    },
    welcome: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(2),
        color: '#11212f',
        paddingLeft: wp(5),
        paddingTop : hp(2)
    },
    appname: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.8),
        color: '#11212f',
        paddingLeft: wp(5)
    },
    logo: {
        width: wp(40),
        height: wp(35),
        position: 'absolute',
        right: wp(4),
        top: hp(3.5),
        resizeMode: 'contain',
        
    },
    card: {
        width: wp(90),
        borderRadius: hp(1),
        backgroundColor: '#fff',
        alignSelf: 'center',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        marginTop: hp(-16),
        paddingBottom: hp(7)
    },
    headerBtn: {
        width: wp(78),
        alignSelf: 'center',
        paddingVertical: hp(2.8)
    },
    headerTxt: {
        fontFamily: 'Poppins-Medium',
        color: 'lightgray',
        fontSize: hp(1.9),
        textTransform: "uppercase"
    },
    activeHeaderTxt: {
        fontFamily: 'Poppins-SemiBold',
        color: '#ff9700',
        fontSize: hp(1.9),
        // borderBottomWidth: 1,
        borderBottomColor: '#11212f',
        textTransform: "uppercase"
    },
    inputContainer: {
        marginTop: hp(1.5)
    },
    inputContent: {
        height: hp(4.8),
        width: wp(78),
        alignSelf: 'center',
        justifyContent: 'center'
    },
    inputText: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#000',
        paddingVertical: 0,
        width: wp(78),
        borderWidth: 1,
        height: hp(4.8),
        borderRadius: hp(4),
        borderColor: 'lightgray',
        paddingLeft: wp(13),
        paddingTop: hp(.3)
    },
    icon: {
        position: 'absolute',
        paddingLeft: wp(5)
    },
    footer: {
        flexDirection: 'row',
        width: wp(78),
        marginTop: hp(1),
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between'
    },
    rememberContent: {
        flexDirection: 'row',
        alignItems: 'center',
        height: hp(4)
    },
    footerTxt: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        paddingLeft: wp(1),
        color: '#707880',
        paddingTop: hp(.2),
        lineHeight: hp(4)
    },
    agreeTxt: {
        color: '#11212f'
    },
    forget: {
        color: '#11212f',
    },
    loginBtn: {
        height: hp(5),
        borderRadius: wp(10),
        // backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: wp(-6),
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: wp(10),
        elevation: 3,
    },
    gradientBg: {
        height: hp(5),
        borderRadius: wp(10),
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(5)
    },
    submitTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#fff',
        textTransform: 'uppercase'
    },
    close: {
        position: 'absolute',
        right: wp(3),
        top: wp(3),
        zIndex: 2,
        padding: hp(1)
    },
    title: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2),
        color: '#000'
    },
    subtitle: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#38434d',
        paddingTop: hp(2),
        paddingBottom: hp(.7)
    },
    termsDesc: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: '#38434d',
        paddingLeft: wp(2)
    },
    link: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#071017',
        paddingLeft: wp(2),
        paddingTop: hp(2),
        textDecorationLine: 'underline'
    },
    eyeIcon: {
        position: 'absolute',
        right: wp(5)
    },
    termsFooter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: hp(1),
        width: wp(100),
        marginBottom: hp(4.5),
        paddingHorizontal: wp(4)
    },
    footerBtn: {
        paddingHorizontal: wp(4),
        borderRadius: hp(.5),
        backgroundColor: '#11212f',
        paddingVertical: hp(.5)
    },
    declineBtn: {
        backgroundColor: '#f1f1f1',
        borderWidth: 1,
        borderColor: '#11212f'
    },
    acceptTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#fff',
        paddingTop: hp(.3),
        textTransform: 'uppercase'
    },
})

export default styles;