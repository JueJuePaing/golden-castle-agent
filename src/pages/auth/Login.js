import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Modal,
    TextInput,
    TouchableOpacity,
    BackHandler,
    Image,
    StatusBar
} from 'react-native';
import Feather from "react-native-vector-icons/Feather";
import Ionicons from "react-native-vector-icons/Ionicons";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';

import { useLocal } from '../../hook/useLocal';
import { Context } from '../../context/Provider';
import { fetchPost, fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { onlySpaces } from '../../utils/validation';
import { setItem, getItem } from '../../utils/appStorage';
import {encryptData} from '../../utils/encryptData';
import {decryptData} from '../../utils/decryptData';

import Loading from '../../components/loading/Loading';
import styles from './Style';
import Terms from './Terms';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import { useTheme } from 'react-native-paper';

const Login = () => {

    const local = useLocal();

    const {
        changeUserInfo,
        lang,
        changeProfileInfo
    } = useContext(Context);

    const [
        loginShowPw,
        setLoginShowPw
    ] = useState(false)
    const [
        username,
        setUsername
    ] = useState('');
    const [
        password,
        setPassword
    ] = useState('');
    const [
        focused,
        setFocused
    ] = useState();
    const [
        showTerms,
        setShowTerms
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        userinfo,
        setUserinfo
    ] = useState(null);
    const [
        formError,
        setFormError
    ] = useState({
        nameError: null,
        usernameError: null,
        passwordError: null,
        confirmPwError: null,
        agentError: null,
        internalError: null
    })

    const goLogin = async () => {
        let valid = true;
        if(onlySpaces(username)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    usernameError: local.invalidAgentCode
                }
            ));
        }
        if(onlySpaces(password)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    passwordError: local.invalidPassword
                }
            ));
        }

        if (!valid) {
            return;
        }

        setLoading(true);

        let info = await getItem('@deviceInfo');
        let deviceInfo = JSON.parse(info);
        let fcmToken = await getItem('@fcmToken');
        
        const data = {
            language: 'en',
            code: username,
            password,
            device_name: deviceInfo?._device_name,
            device_model: deviceInfo?._device_model,
            os_version: deviceInfo?._os_version,
            os_type: deviceInfo?._os_type,
            app_version_id: deviceInfo?._app_version,
            noti_token: fcmToken ? fcmToken : "FCM_TOKEN",
        }

        let encryptLgData = encryptData(JSON.stringify(data));

        const response = await fetchPost(apiUrl.agentLogin, {data: encryptLgData});

        if (response?.success && response?.data?.data) {
            let decData = JSON.parse(decryptData(response.data.data));
            setShowTerms(true);
            setUserinfo(decData);
        } else {
            if (response?.data?.message) {
                setFormError((prev) => (
                    {
                        ...prev,
                        internalError: response.data.message
                    }
                ));
            }
        }
        setLoading(false);
    }

    const closeTerms = () => {
        setShowTerms(false);
    }

    const acceptHandler = async () => {
        setLoading(true)
        setItem('@userInfo', JSON.stringify(userinfo));

        if (userinfo?.access_token) {
            const response = await fetchGetByToken(apiUrl.profile, userinfo.access_token);
            if (response?.status === 200 && response?.data) {
                const decAgentinfo = JSON.parse(decryptData(response.data, userinfo.secret_key));
                changeProfileInfo(decAgentinfo.agent);
            }
        } 
        setShowTerms(false);
        setLoading(false);
        changeUserInfo(userinfo);
    }

    const declineHandler = () => {
        setShowTerms(false);
    }

    const onChangeUsername = (val) => {
        setUsername(val);
        setFormError((prev) => (
            {
                ...prev,
                usernameError: null,
                internalError: null
            }
        ));
    }

    const onChangePassword = (val) => {
        setPassword(val);
        setFormError((prev) => (
            {
                ...prev,
                passwordError: null,
                internalError: null
            }
        ));
    }

    return (
        <Modal
            transparent={false}
            visible={true}
            onRequestClose={() =>BackHandler.exitApp() }
            animationType="fade">
            {
                showTerms ?
                    <Terms
                        onClose={ closeTerms }
                        acceptHandler={ acceptHandler }
                        declineHandler={ declineHandler } /> :
                    <View 
                        style={styles.modalView}>
                        <StatusBar backgroundColor="#ff9700" barStyle='light-content' />
                        <View style={styles.upperContainer}>
                            <Text style={[lang === 'en' ? styles.welcome : styles.appname, {
                                marginTop: hp(6)
                            }]}>
                                {local.welcomeTo}
                            </Text>
                            <Text style={lang === 'en' ? styles.appname : styles.welcome}>
                                {local.appName}
                            </Text>
                            <Image
                                source={require('../../assets/images/agentWbg.png')}
                                style={styles.logo} />
                        </View>
                        <View style={styles.card}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.headerBtn}
                                >
                                <Text style={styles.activeHeaderTxt}>
                                    { local.login }
                                </Text>
                            </TouchableOpacity>

                            <View style={styles.inputContainer}>
                               
                                <ErrorMessage message={formError.nameError} />

                                <View style={styles.inputContent}>
                                    <Feather
                                        name="user"
                                        size={wp(5)}
                                        color={focused === 1 ? "#11212f" : '#c9c7c3'}
                                        style={styles.icon} />
                                    <TextInput
                                        placeholder={local.code}
                                        placeholderTextColor="#c9c7c3"
                                        value={username}
                                        onChangeText={onChangeUsername}
                                        maxLength={30}
                                        onFocus={()=> setFocused(1)}
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 1 ? "#11212f" : 'lightgray',
                                            fontSize: username === "" ? hp(1.5) : hp(1.7)
                                        }]} />
                                </View>
                                <ErrorMessage message={formError.usernameError} />

                                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                                    <Feather
                                        name="lock"
                                        size={wp(5)}
                                        color={focused === 2 ? "#11212f" : '#c9c7c3'}
                                        style={styles.icon} />
                                    <TextInput
                                        placeholder={local.password}
                                        placeholderTextColor="#c9c7c3"
                                        value={password}
                                        onChangeText={onChangePassword}
                                        secureTextEntry={!loginShowPw}
                                        maxLength={30}
                                        onFocus={()=> setFocused(2)}
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 2 ? "#11212f" : 'lightgray',
                                            fontSize: password === "" ? hp(1.5) : hp(1.7)
                                        }]} />
                                    <TouchableOpacity 
                                        style={styles.eyeIcon}
                                        onPress={()=> {
                                                setLoginShowPw(!loginShowPw)
                                        }}>
                                        <Ionicons
                                            name={loginShowPw ? "eye" : "eye-off" }
                                            size={wp(5)}
                                            color={focused === 2 ? "#ff9700" : '#c9c7c3'} />
                                    </TouchableOpacity>

                                </View>
                                <ErrorMessage message={formError.passwordError} />

                                
                            </View>
                           
                            <ErrorMessage message={formError.internalError} />
                            <TouchableOpacity 
                                activeOpacity={0.8}
                                style={styles.loginBtn}
                                onPress={goLogin}>
                                <LinearGradient
                                    colors={[
                                        'rgba(17, 33, 47, 0.5)',
                                        'rgba(17, 33, 47, 0.6)',
                                        'rgba(17, 33, 47, 0.9)',
                                        'rgba(17, 33, 47, 1)',
                                        'rgba(17, 33, 47, 0.6)',
                                        'rgba(17, 33, 47, 0.5)'
                                    ]}
                                    start={{ x: 0.1, y: 0.06 }}
                                    style={styles.gradientBg}>
                                    <Text style={styles.submitTxt}>
                                        {local.login}
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View> 
                    </View>
            }
            {
                loading && <Loading />
            }
        </Modal>
    )
}

export default Login;