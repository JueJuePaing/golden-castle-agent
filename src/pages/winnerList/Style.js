import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    totalWinContent: {
        marginVertical: hp(1),
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: wp(3)
    },
    totalWin: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: 'gray'
    },
    totalCount: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#ff9700'
    },
    card: {
        width: wp(94),
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderRadius: hp(.5),
        marginTop: hp(3),

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        paddingBottom: hp(1)
    },
    crown: {
        position: 'absolute',
        transform: [
            {
                rotate: '-45deg'
            }
        ],
        top: hp(-1),
        left: hp(-1)
    },
    titleContent: {
        backgroundColor: '#11212f',
        paddingHorizontal: wp(5),
        alignSelf: 'center',
      
        marginTop: hp(-2),
        borderRadius: hp(3),
        height: hp(4.2),
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#fff'
    },
    header: {
        flexDirection : 'row',
       // alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: wp(3),
        marginTop: hp(-4)
       // marginVertical: hp(2)
    },
    title: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(1.6),
        color: '#fff',
        paddingTop: hp(.4)
        //paddingVertical: hp(.5)
    },
    row: {
        flexDirection : 'row',
        alignItems: 'center',
    },
    winnerCount: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.8),
        color: '#ff9700',
        paddingLeft: wp(2)
    },
    prizeAmt: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: '#11212f', //485663
        // paddingLeft: wp(2),
        paddingBottom: hp(.3)
    },
winnerContent: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: hp(1),
        paddingHorizontal: wp(2),
        justifyContent: 'space-between'
    },
    order: {
        width: wp(7),
        textAlign: 'center',
        fontSize: hp(1.7),
        color: '#ff9700'
    },
    profile: {
        width: wp(13),
        height: wp(13),
        borderRadius: wp(10),
    },
    nameContent: {
        width: wp(44.5),
        paddingLeft: wp(2),
        paddingTop: hp(.5)
    },
    name: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#ff9700'
    },
    ticketnoContent: {
       // alignSelf: 'flex-start',
        width: wp(30),
        alignItems: 'center',
        flexDirection: 'row',
        //justifyContent: 'space-around',
        paddingHorizontal: wp(2),
        marginTop: hp(-1.3)
    },
    ticketBg: {
        width: wp(4),
        height: hp(3),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ff9700',
        borderRightWidth: .5,
        borderRadius: hp(.4),
        borderColor: '#11212f'
    },
    ticketno: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.2),
        color: '#ff9700',
        fontStyle: 'italic',

    },
    ticketno2: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#fff'
    },
    nodata: {
        height: hp(100),
        width: wp(100),
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute'
    }
})

export default styles;