import React, {
    useState,
    useContext
} from 'react';
import {
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import { deleteMultiItems } from '../../utils/appStorage';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp
} from 'react-native-responsive-screen';
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from "react-native-vector-icons/Ionicons";

import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { onlySpaces } from '../../utils/validation';
import { Context } from '../../context/Provider';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import SessionExpired from '../../components/modal/SessionExpired';

const ChangePassword = ({navigation}) => {

    const local = useLocal();
    const { 
        userInfo,
        net,
        changeUserInfo
    } = useContext(Context);

    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        oldPw,
        setOldPw
    ] = useState('')
    const [
        newPw,
        setNewPw
    ] = useState('')
    const [
        confirmPw,
        setConfirmPw
    ] = useState('')
    const [
        focused,
        setFocused
    ] = useState()
    const [
        showOldPw,
        setShowOldPw
    ] = useState(false);
    const [
        showNewPw,
        setShowNewPw
    ] = useState(false);
    const [
        showConfirmPw,
        setShowConfirmPw
    ] = useState(false);
    const [
        formError,
        setFormError
    ] = useState({
        oldPwError: null,
        passwordError: null,
        confirmPwError: null,
        internalError: null
    })

    const onClose = () => {
        navigation.goBack();
    }

    const submitHandler = async () => {
        if (!net) {
            ToastAndroid.show(local.noConnection, ToastAndroid.SHORT);
            return;
        }

        if (userInfo?.access_token) {
            let valid = true;
            if(onlySpaces(oldPw)) {
                valid = false;
                setFormError((prev) => (
                    {
                        ...prev,
                        oldPwError: local.invalidOldPassword
                    }
                ));
            }
            if(onlySpaces(newPw)) {
                valid = false;
                setFormError((prev) => (
                    {
                        ...prev,
                        passwordError: local.invalidNewPassword
                    }
                ));
            }
            if(onlySpaces(confirmPw)) {
                valid = false;
                setFormError((prev) => (
                    {
                        ...prev,
                        confirmPwError: local.invalidConfirmPassword
                    }
                ));
            } else if(newPw !== confirmPw) {
                valid = false;
                setFormError((prev) => (
                    {
                        ...prev,
                        confirmPwError: local.mismatchPassword
                    }
                ));
            }
    
            if (!valid) {
                return;
            }
    
            setLoading(true);
    
            const data = {
                current_password : oldPw,
                password : newPw,
                password_confirmation : confirmPw
            }
    
            setLoading(true);

            let encryptPwData = encryptData(JSON.stringify(data), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.updatePassword, {data: encryptPwData}, userInfo.access_token);

            if (response?.success) {
                ToastAndroid.show(local.successful, ToastAndroid.SHORT);
                navigation.goBack();
            } else {
                if (response?.status === 401) {
                    setExpired(true);
                } else {
                    if (response?.message) {
                        setFormError((prev) => (
                            {
                                ...prev,
                                internalError: response.message
                            }
                        ));
                    } else {
                        ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);
                    }
                }
            }

            setLoading(false);
        } else {
            setExpired(true);
        }
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }


    const onChangeOldPassword = (val) => {
        setOldPw(val);
        setFormError((prev) => (
            {
                ...prev,
                oldPwError: null,
                internalError: null
            }
        ));
    }

    const onChangeNewPassword = (val) => {
        setNewPw(val);
        setFormError((prev) => (
            {
                ...prev,
                passwordError: null,
                confirmPwError: null,
                internalError: null
            }
        ));
    }

    const onChangeConfirmPassword = (val) => {
        setConfirmPw(val);
        setFormError((prev) => (
            {
                ...prev,
                confirmPwError: null,
                internalError: null
            }
        ));
    }
    return (
        <View style={styles.container}>
            <Header
                title={local.changePassword}
                goBack={onClose}/>
            
            <View>
                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                    <Feather
                        name="lock"
                        size={wp(5)}
                        color={focused === 1 ? "#11212f" : '#c9c7c3'}
                        style={styles.icon} />
                    <TextInput
                        placeholder={local.oldPassword}
                        placeholderTextColor="#c9c7c3"
                        value={oldPw}
                        onChangeText={ onChangeOldPassword }
                        maxLength={30}
                        onFocus={()=> setFocused(1)}
                        onBlur={()=> setFocused(null)}
                        secureTextEntry={!showOldPw}
                        style={[styles.inputText, {
                            borderColor: focused === 1 ? "#11212f" : 'lightgray',
                            fontSize: oldPw === "" ? hp(1.5) : hp(1.7)
                        }]} />
                     <TouchableOpacity 
                        style={styles.eyeIcon}
                        onPress={()=> {
                                setShowOldPw(!showOldPw)
                        }}>
                        <Ionicons
                            name={showOldPw ? "eye" : "eye-off" }
                            size={wp(5)}
                            color={focused === 1 ? "#ff9700" : '#c9c7c3'} />
                    </TouchableOpacity>
                </View>
                <ErrorMessage message={formError.oldPwError} />

                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                    <Feather
                        name="lock"
                        size={wp(5)}
                        color={focused === 2 ? "#11212f" : '#c9c7c3'}
                        style={styles.icon} />
                    <TextInput
                        placeholder={local.newPassword}
                        placeholderTextColor="#c9c7c3"
                        secureTextEntry={!showNewPw}
                        value={newPw}
                        onChangeText={ onChangeNewPassword }
                        maxLength={30}
                        onFocus={()=> setFocused(2)}
                        onBlur={()=> setFocused(null)}
                        style={[styles.inputText, {
                            borderColor: focused === 2 ? "#11212f" : 'lightgray',
                            fontSize: newPw === "" ? hp(1.5) : hp(1.7)
                        }]} />
                    <TouchableOpacity 
                        style={styles.eyeIcon}
                        onPress={()=> {
                                setShowNewPw(!showNewPw)
                        }}>
                        <Ionicons
                            name={showNewPw ? "eye" : "eye-off" }
                            size={wp(5)}
                            color={focused === 2 ? "#ff9700" : '#c9c7c3'} />
                    </TouchableOpacity>
                </View>
                <ErrorMessage message={formError.passwordError} />

                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                    <Feather
                        name="lock"
                        size={wp(5)}
                        color={focused === 3 ? "#11212f" : '#c9c7c3'}
                        style={styles.icon} />
                    <TextInput
                        placeholder={local.confirmPassword}
                        placeholderTextColor="#c9c7c3"
                        value={confirmPw}
                        secureTextEntry={!showConfirmPw}
                        onChangeText={ onChangeConfirmPassword }
                        maxLength={30}
                        onFocus={()=> setFocused(3)}
                        onBlur={()=> setFocused(null)}
                        style={[styles.inputText, {
                            borderColor: focused === 3 ? "#11212f" : 'lightgray',
                            fontSize: confirmPw === "" ? hp(1.5) : hp(1.7)
                        }]} />
                    <TouchableOpacity 
                        style={styles.eyeIcon}
                        onPress={()=> {
                                setShowConfirmPw(!showConfirmPw)
                        }}>
                        <Ionicons
                            name={showConfirmPw ? "eye" : "eye-off" }
                            size={wp(5)}
                            color={focused === 3 ? "#ff9700" : '#c9c7c3'} />
                    </TouchableOpacity>
                </View>
                <ErrorMessage message={formError.confirmPwError} />
                <ErrorMessage message={formError.internalError} />

                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.submitBtn}
                    onPress={ submitHandler }>
                    <Text style={styles.submitTxt}>{local.submit}</Text>
                </TouchableOpacity>
            </View>
            {
                loading && <Loading />
            }
             {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </View>
    )
}

export default ChangePassword;