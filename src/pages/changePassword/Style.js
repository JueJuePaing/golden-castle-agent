import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputContent: {
        height: hp(4.8),
        width: wp(90),
        alignSelf: 'center',
        justifyContent: 'center'
    },
    eyeIcon: {
        position: 'absolute',
        right: wp(5)
    },
    inputText: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#000',
        paddingVertical: 0,
        width: wp(90),
        borderWidth: 1,
        height: hp(4.8),
        borderRadius: hp(4),
        borderColor: 'lightgray',
        paddingLeft: wp(13),
        paddingTop: hp(.3)
    },
    icon: {
        position: 'absolute',
        paddingLeft: wp(5)
    },
    submitBtn: {
        alignSelf: 'center',
        height: hp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff9700',
        borderRadius: hp(3),
        marginTop: hp(3),
        paddingHorizontal: wp(6),
        borderWidth: 1,
        borderColor: '#11212f',


        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3
    },
    submitTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#fff',
        paddingTop: hp(.5),
        textTransform: 'uppercase'
    }
})

export default styles;