import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ff9700'
    },
    name: {
        color: '#11212f',
        paddingTop: hp(2),
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2)
    }
})

export default styles;