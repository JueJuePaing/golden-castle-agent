import React, {
    useEffect
} from 'react';
import {
    View,
    StatusBar,
    Text
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import styles from './Style';
import Logo from '../../assets/icons/Logo';

const Splash = ({navigation}) => {

    return (
        <View style={styles.container}>
            <StatusBar barStyle={'light-content'} backgroundColor="#ff9700" />
            <Logo size={wp(40)} />
            <Text style={styles.name}>
                GOLDEN CASTLE
            </Text>
        </View>
    )
}

export default Splash;