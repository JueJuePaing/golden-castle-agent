import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ToastAndroid,
    TextInput,
    DeviceEventEmitter
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import { launchImageLibrary } from "react-native-image-picker";

import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';
import { fetchGetByToken, fetchPostImageUpload } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { onlySpaces } from '../../utils/validation';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';
import SessionExpired from '../../components/modal/SessionExpired';
import AccountModal from '../../components/AccountModal/AccountModal';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import PasswordModal from '../../components/passwordModal/PasswordModal';
import CustomAlert from '../../components/modal/CustomAlert';
import { decay } from 'react-native-reanimated';

const WithdrawScreen = ({navigation}) => {

    const { 
        lang,
        net,
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const local = useLocal();
    const lanData = new FormData();
    lanData.append('language', lang);

    const [
        accounts,
        setAccounts
    ] = useState(null);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        selectedAccount,
        setSelectedAccount
    ] = useState();
    const [
        showModal,
        setShowModal
    ] = useState(false);
    const [
        point,
        setPoint
    ] = useState('');
    const [
        accNo,
        setAccNo
    ] = useState('');
    const [
        accName,
        setAccName
    ] = useState('');
    const [
        focused,
        setFocused
    ] = useState();
    const [
        qrImg,
        setQrImg
    ] = useState();
    const [
        showPassword,
        setShowPassword
    ] = useState(false);
    const [
        alert,
        setAlert
    ] = useState();
    const [
        formError,
        setFormError
    ] = useState({
        pointError: null,
        accNoError: null,
        accNameError: null,
        internalError: null
    })

    useEffect(() => {
        if (net && userInfo?.access_token) getAccounts();
        else {
            setExpired(true);
        }
    }, [net, userInfo]);

    const getAccounts = async () => {
        setLoading(true);

        const response = await fetchGetByToken(apiUrl.withdrawAccounts, userInfo.access_token);
        setLoading(false);

        if (response?.status === 200 && response?.data) {
            const decAccounts = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decAccounts?.options?.length > 0) {
                setAccounts(decAccounts.options);
                setSelectedAccount({...decAccounts.options[0], id : 0});
            } else {
                    setNoData(true);
                    if (response?.message) {
                        setAlert(response.message)
                    } else {
                        setAlert(local.somethingWrong)
                    }
            }
        } else {
            if (response?.status === 401) {
                // Unauthenticated
                setExpired(true);
            } else {
                setNoData(true);
                if (response?.message) {
                    setAlert(response.message)
                } else {
                    setAlert(local.somethingWrong)
                }
            }
        }
    }


    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const chooseHandler = (account, index) => {
        setShowModal(false);
        setSelectedAccount({...account, id : index});
    }

    const accountHandler = (account) => {
        setShowModal(true);
    }

    const submitHandler = () => {
        let valid = true;
        if(onlySpaces(accNo)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    accNoError: local.invalidAccNo
                }
            ));
        }
        if(onlySpaces(accName)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    accNameError: local.invalidAccName
                }
            ));
        }
        if(onlySpaces(point)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    pointError: local.invalidPoint
                }
            ));
        } else if (parseInt(point) === 0) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    pointError: local.invalidZeroPoint
                }
            ));
        }

        if (!valid) {
            return;
        }

        setShowPassword(true);

    }

    const submitPasswordHandler = async (password) => {
        setShowPassword(false);

        if (userInfo?.access_token) {
            setLoading(true);



            const uploadData = new FormData();
            let data = {
                option: selectedAccount.name,
                password,
                payee: accName,
                point: parseInt(point),
                account_number: accNo
            }
            let encData = encryptData(JSON.stringify(data), userInfo.secret_key);
            uploadData.append('data', encData);
            if (qrImg) {
                uploadData.append('qr_code', {
                    type: qrImg.type,
                    uri: qrImg.uri,
                    name: qrImg.fileName,
                    width: qrImg.width,
                    height: qrImg.height
                });
            }

            const response = await fetchPostImageUpload(apiUrl.withdraw, uploadData, userInfo.access_token);
            setLoading(false);
            
            if (response?.success) {
                if (response.message) {
                    ToastAndroid.show(response.message, ToastAndroid.SHORT);
                    setAccName('')
                    setAccNo('')
                    setPoint('')
                    setQrImg()
                } else {
                    ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);
                }
                DeviceEventEmitter.emit('withdraw_success', 'true');
            } else {
                if (response?.message) {
                    setAlert(response.message);
                } else {
                    setAlert(local.somethingWrong);
                }
            }
        } else {
            ToastAndroid.show(local.unauthenticated, ToastAndroid.SHORT);
            setExpired(true);
        }
    }

    const rightBtnHandler = () => {
        navigation.navigate("WithdrawHistory")
    }

    const onChangePoint = (val) => {
        setPoint(val);
        setFormError((prev) => (
            {
                ...prev,
                pointError: null,
                internalError: null
            }
        ));
    }

    const onChangeAccNo = (val) => {
        setAccNo(val);
        setFormError((prev) => (
            {
                ...prev,
                accNoError: null,
                internalError: null
            }
        ));
    }

    const onChangeAccName = (val) => {
        setAccName(val);
        setFormError((prev) => (
            {
                ...prev,
                accNameError: null,
                internalError: null
            }
        ));
    }

    const qrUploadHandler = () => {
        launchImageLibrary({
            selectionLimit : 1,
            mediaType : "photo"
        }, response => {
            if (response.didCancel !== true) {
              if (response && response.assets && response.assets.length > 0) {
                setQrImg(response.assets[0]);
              } else {
                ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);
              }
            }
          });
    }

    const removeQrHandler = () => {
        setQrImg();
    }

    return (
        <View style={styles.container}>
 
            <Header
                title={local.withdraw}
                goBack={()=> navigation.goBack()}
                rightBtn={local.history}
                rightBtnHandler={ rightBtnHandler } />
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                        {accounts && (
                            <ScrollView 
                                style={ styles.mainContent }
                                showsVerticalScrollIndicator={false} >

                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.accountInfo}
                                    onPress={ accountHandler }>
                                    <View style={styles.accountInfo2}>
                                        <Image
                                            source={{ uri : selectedAccount?.icon }}
                                            style={ styles.accountIcon } />
                                        <Text style={ styles.accountName }>
                                            { selectedAccount?.name }
                                        </Text>
                                    </View>
                                    <AntDesign
                                        name="downcircleo"
                                        size={hp(2)}
                                        color="#ff9700"
                                        />
                                </TouchableOpacity>

                                <View style={styles.inputContent}>
                                    <Text style={styles.label}>{local.accountNo}</Text>
                                    <TextInput
                                        value={accNo}
                                        onChangeText={ onChangeAccNo }
                                        maxLength={50}
                                        onFocus={()=> setFocused(2)}
                                        keyboardType="numeric"
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 2 ? "#ff9700" : 'lightgray'
                                        }]} />
                                </View>
                                <ErrorMessage width={wp(90)} message={formError.accNoError} />

                                <View style={styles.inputContent}>
                                    <Text style={styles.label}>{local.accountName}</Text>
                                    <TextInput
                                        value={accName}
                                        onChangeText={ onChangeAccName }
                                        maxLength={50}
                                        onFocus={()=> setFocused(3)}
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 3 ? "#ff9700" : 'lightgray'
                                        }]} />
                                </View>
                                <ErrorMessage width={wp(90)} message={formError.accNameError} />

                                <View style={styles.inputContent}>
                                    <Text style={styles.label}>{local.point}</Text>
                                    <TextInput
                                        value={point}
                                        onChangeText={ onChangePoint }
                                        maxLength={30}
                                        onFocus={()=> setFocused(1)}
                                        keyboardType="numeric"
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 1 ? "#ff9700" : 'lightgray'
                                        }]} />
                                </View>
                                <ErrorMessage width={wp(90)} message={formError.pointError} />

                                <View style={styles.qrInputContent}>
                                    <Text style={styles.label}>{local.qrImage}</Text>
                                    {
                                        qrImg ?
                                        <View style={ styles.qrImg }>
                                            <Image 
                                                source={{ uri : qrImg.uri }}
                                                style={ styles.qrImg } />
                                            <TouchableOpacity
                                                activeOpacity={0.8}
                                                style={styles.closeBtn}
                                                onPress={removeQrHandler}>
                                                <AntDesign
                                                    name="closecircle"
                                                    size={hp(2.5)}
                                                    color="#ff9700" />
                                            </TouchableOpacity>
                                        </View> :
                                        <TouchableOpacity
                                        activeOpacity={0.8}
                                            style={styles.addQr}
                                            onPress={qrUploadHandler}>
                                            <Ionicons
                                                name='add'
                                                size={hp(4)}
                                                color='gray' />
                                        </TouchableOpacity>
                                    }
                                </View>

                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={ styles.submitBtn }
                                    onPress={ submitHandler }>
                                    <Text style={styles.submitTxt}>
                                        {local.submit}
                                    </Text>
                                </TouchableOpacity>
                            </ScrollView>
                        ) }
                            
                        {loading && <Loading />}

                        {
                            !loading && noData && <View  style={styles.nodata}>
                                <NoData message={local.noWithdrawAccount} />
                            </View>
                        }
                    </>
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
            {
                showModal && (
                    <AccountModal
                        accounts={accounts}
                        selectedAccount={selectedAccount}
                        closeModalHandler={()=> setShowModal(false)}
                        chooseHandler={ chooseHandler } />
                )
            } 
            {
                showPassword && <PasswordModal
                    point={point}
                    modalVisible={true}
                    closeModalHandler={()=> setShowPassword(false)}
                    submitHandler={submitPasswordHandler} />
            }
            {
                alert && <CustomAlert
                    message={alert}
                    closeHandler={()=>  {
                        setAlert()
                    }}/>
            }


        </View>
    )
}

export default WithdrawScreen;