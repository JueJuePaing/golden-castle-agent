import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: hp(8)
    },
    mainContent: {
        marginVertical: hp(2),
        alignSelf: 'center'
    },
    accountInfo : {
        width: wp(90),
        paddingHorizontal: wp(3),
        paddingVertical: hp(1.5),
        borderRadius: hp(1),
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    accountInfo2: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    accountName : {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        paddingTop: hp(.3),
        color: '#11212f'
    },
    accountIcon: {
        width: wp(13),
        height: wp(10),
        marginRight: wp(2),
        resizeMode: 'contain'
    },
    nodata: {
        height: hp(100),
        width: wp(100),
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute'
    },
    inputContent: {
        height: hp(4.8),
        width: wp(90),
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: hp(3.5),
        marginBottom: hp(1)
    },
    qrInputContent: {
        marginVertical: hp(3)
    },
    inputText: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#000',
        paddingVertical: 0,
        width: wp(90),
        borderWidth: 1,
        height: hp(4.8),
        borderRadius: hp(1),
        borderColor: 'lightgray',
        paddingLeft: wp(2),
        paddingTop: hp(.3)
    },
    label: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.45),
        color: '#11212f'
    },
    addQr: {
        width: wp(55),
        height: wp(55),
        borderRadius: hp(1),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e6e7e8',
        marginTop: hp(0.5)
    },
    qrImg: {
        width: wp(55),
        height: wp(55),
        borderRadius: hp(1),
        marginTop: hp(0.5)
    },
    closeBtn: {
        position : "absolute",
        right : hp(-1),
        top: hp(-1)
    },
    submitBtn: {
        alignSelf: 'center',
        height: hp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#11212F',
        borderRadius: hp(3),
        paddingHorizontal: wp(10),
        borderWidth: 1,
        borderColor: '#FF9700',

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
    },
    submitTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#fff',
        paddingTop: hp(.5)
    },
    record: {
        width: wp(94),
        backgroundColor: '#fff',
        borderRadius: hp(.5),
        paddingHorizontal: wp(3),
        paddingVertical: hp(1.5),
        marginBottom: hp(1)
    },
    topRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    bankRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    bankIcon: {
        width: wp(5),
        height: wp(5),
        resizeMode: 'contain',
        marginRight: wp(1)
    },
    point: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#ff9700',
    },
    account_type: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#000',
        paddingTop: hp(.3)
    },
    recordRow: {
        flexDirection: 'row',
        width: wp(90),
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: hp(.3)
    },
    statusRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    status: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        paddingLeft: wp(1)
    },
    recordDate: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.4),
        color:'darkgray'
    },
    statusDot: {
        width: wp(2),
        height: wp(2),
        borderRadius: wp(2),
    },

    // Detail
    detailcontainer: {
        flex: 1,
        marginBottom: hp(8),
        // alignItems: 'center'
    },
    topContent: {
        backgroundColor: '#11212f',
        width: wp(92),
        paddingVertical: hp(2),
        borderRadius: hp(1),
        alignSelf: 'center',
        alignItems: 'center',
        paddingTop: hp(3),
        marginTop: hp(1.5)
    },
    account_type_detail: {
        color: '#fff',
        fontFamily: 'Poppins-Medium'
    },
    toprow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    paymentIcon: {
        width: wp(12),
        height: wp(12),
        borderRadius: wp(12),
        marginRight: wp(2)
    },
    detailPoint: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2.7),
        color: '#ff9700',
        paddingTop: hp(1.7)
    },
    infoRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: wp(92),
        alignSelf: 'center',
        marginTop: hp(3)
    },
    label: {
        fontFamily: 'Poppins-Regular',
        color: 'darkgray',
        fontSize: hp(1.8)
    },
    value: {
        fontFamily: 'Poppins-Medium',
        color: '#000',
        fontSize: hp(1.7)
    },
    screenshot: {
        alignSelf: 'center',
        width: wp(70),
        height: hp(60),
        resizeMode: 'cover',
        marginTop: hp(2),
        borderRadius: hp(.5),
        marginBottom: hp(5)
    },
    statusDesc: {
        fontFamily: 'Poppins-Regular',
        color: '#fff',
        fontSize: hp(1.7)
    }
});

export default styles;