import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import moment from 'moment';

import { fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';
import SessionExpired from '../../components/modal/SessionExpired';

const WithdrawHistory = ({navigation}) => {

    const { 
        lang,
        net,
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const local = useLocal();
    const [
        records,
        setRecords
    ] = useState(null);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);

    useEffect(() => {
        if (net && userInfo?.access_token) getRecords();
        else setExpired(true);
    }, [net, userInfo]);

    const getRecords = async () => {
        setLoading(true);

        const response = await fetchGetByToken(apiUrl.withdrawRecords, userInfo.access_token);
        setLoading(false);

        if (response?.status === 200 && response?.data) {
            const decRecords = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decRecords?.records) {
                if (decRecords.records.length > 0) {
                    setRecords(decRecords.records);
                } else {
                    setNoData(true);
                }
            } else {
                setNoData(true);
            }
        } else {
            if (response?.status === 401) {
                // Unauthenticated
                setExpired(true);
            } else {
                setNoData(true);
                if (response?.message) {
                    ToastAndroid.show(response.message, ToastAndroid.SHORT);
                }
            }
        }


        if (response?.records) {
            
        } else {
            
        }
    }


    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const accountHandler = (account) => {
        setSelectedAccount(account)
    }

    return (
        <View style={styles.container}>
 
            <Header
                title={local.withdrawHistory}
                goBack={()=> navigation.goBack()}/>
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                        {!loading && records && (
                            <ScrollView 
                                style={ styles.mainContent }
                                showsVerticalScrollIndicator={false} >
                                {
                                    records.map((record) => {
                                        const statusColor = record.status === "Requested" ? "#e5a714" :
                                        record.status === "Completed" ? "#20cd43" : 
                                        record.status === "Rejected" ? "#ef5543" : "gray";

                                        return (
                                            <TouchableOpacity
                                                activeOpacity={1}
                                                style={ styles.record }
                                                onPress={()=> navigation.navigate('RecordDetail', {record})}>
                                                <View style={styles.topRow}>
                                                    <View style={styles.bankRow}>
                                                        {
                                                            record.icon && <Image
                                                                source={{ uri : record.icon }}
                                                                style={styles.bankIcon} />
                                                        }
                                                        <Text style={styles.account_type}>
                                                            {record.option}
                                                        </Text>
                                                    </View>
                                                    
                                                    <Text style={styles.point}>
                                                        {`${record.point} ${local.point}`}
                                                    </Text>
                                                </View>
                                                <View style={styles.recordRow}>
                                                    <View style={styles.statusRow}>
                                                        <View style={[
                                                            styles.statusDot,
                                                            {
                                                                backgroundColor: statusColor
                                                            }
                                                        ]} />
                                                        <Text style={[
                                                            styles.status,
                                                            {
                                                                color: statusColor
                                                            }
                                                        ]}>
                                                            {record.status}
                                                        </Text>
                                                    </View>
                                                    <Text style={styles.recordDate}>
                                                        {moment(record.date).format('hh:mm A - DD MMM, YYYY')}
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })
                                }
                            </ScrollView>
                        ) }
                            
                        {loading && <Loading />}

                        {
                            !loading && noData && <View  style={styles.nodata}>
                                <NoData message={local.noRecord} />
                            </View>
                        }
                    </>
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </View>
    )
}

export default WithdrawHistory;