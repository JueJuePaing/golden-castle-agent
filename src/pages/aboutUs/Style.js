// import { StyleSheet, StatusBar } from "react-native";
// import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#f6f6f6',
//         paddingTop: hp(10),
//         justifyContent: 'center',
//         alignItems: 'center',
//         paddingBottom: hp(15)
//     },
//     logo: {
//         width: wp(70),
//         height: hp(20),
//         resizeMode: 'contain'
//     },
//     title: {
//         fontFamily: 'Poppins-SemiBold',
//         fontSize: hp(2.2),
//         color: '#11212f'
//     },
//     version: {
//         fontFamily: 'Poppins-Regular',
//         fontSize: hp(1.4),
//         color: '#11212f',
//         paddingTop: hp(3)
//     },
//     description: {
//         fontFamily: 'Poppins-Regular',
//         fontSize: hp(1.4),
//         color: '#ff9700',
//         paddingHorizontal: wp(5),
//         paddingTop: hp(2)
//     },
//     close: {
//         position: 'absolute',
//         right: wp(5),
//         top: StatusBar.currentHeight + hp(2)
//     }
// })

// export default styles;

import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f6f6f6',
    },
    content : {
        justifyContent: 'center',
        // alignItems: 'center',
        paddingTop: hp(10),
        paddingBottom: hp(15)
    },
    logo: {
        width: wp(70),
        height: hp(8),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginBottom : hp(4)
    },
    link: {
        textDecorationLine: 'underline'
    },
    title: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.2),
        color: '#11212f',
        alignSelf: 'center'
    },
    version: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.4),
        color: '#ff9700',
        paddingTop: hp(3),
        alignSelf: 'center'
    },
    subtitle: {
        paddingHorizontal: wp(5),
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#071017',
        paddingTop : hp(2.5)
    },
    label: {
        paddingHorizontal: wp(5),
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#071017',
        paddingTop : hp(.9)
    },
    description: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#11212f',
        paddingHorizontal: wp(5),
        paddingTop: hp(2)
    },
    close: {
        position: 'absolute',
        right: wp(5),
        top: StatusBar.currentHeight + hp(2)
    }
})

export default styles;