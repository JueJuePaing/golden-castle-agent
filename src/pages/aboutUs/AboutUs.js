import React, {
    useState
} from 'react';
import {
    View,
    Image,
    Text,
    Linking,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native';
import {
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign'

import styles from './Style';

const AboutUs = ({navigation}) => {

    const onClose = () => {
        navigation.goBack();
    }

    return (
        <ScrollView style={styles.container}>
            <StatusBar backgroundColor={'#f6f6f6'} barStyle='dark-content' />
            <View style={styles.content}>
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.close}
                onPress={onClose}>
                <AntDesign
                    name='closecircleo'
                    size={hp(3)}
                    color='#ff9700' />
            </TouchableOpacity>
            <Image  
                source={require('../../assets/images/agentWbg.png')}
                style={styles.logo} />
            <Text style={styles.title}>
                ShweTaik - Golden Castle
            </Text>
            <Text style={styles.description}>
            &nbsp;&nbsp;&nbsp;  ShweTaik - Golden Castle lottery (Thai e-tickets) မှကြိုဆိုပါတယ်။ 
ShweTaik - Golden Castle Lottery (Thai e-tickets) ဝန်ဆောင်မှုသည် 100% ယုံကြည်စိတ်ချရသော Online စနစ်ဖြစ်သည့်အတွက် ကံစမ်းလိုသူများ အခတ်အခဲ တစ်စုံတစ်ရာ မရှိစေရန် Online Banking စနစ်အား အပြည့်အဝ အသုံးပြုထားပါသည်။ ထို့ကြောင့် သင့်အိမ်မက်များကို လက်တွေ့အကောင်ထည်ဖော်ဖို့ ကူညီပေးမယ့် Golden Castle Lottery မှာလာရောက် ကံစမ်းလိုက်ပါ။ 
</Text>

<Text style={styles.subtitle}>
    ဆုမဲများ
</Text>

<Text style={styles.label}>
ပထမဆု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၆သန်း (၁) ဆု
</Text>

<Text style={styles.label}>
ပထမဆု (တွတ်-၁လုံးတိုး)
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၁သိန်း (၁) ဆု
</Text>

<Text style={styles.label}>
ပထမဆု (တွတ်-၁လုံးလျော့)
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၁သိန်း (၁) ဆု
</Text>

<Text style={styles.label}>
ဒုတိယဆု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၂ သိန်း (၅) ဆု
</Text>

<Text style={styles.label}>
တတိယဆု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၈ သောင်း (၁၀) ဆု
</Text>

<Text style={styles.label}>
စတုတ္ထဆု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၄ သောင်း (၅၀) ဆု
</Text>

<Text style={styles.label}>
ပဉ္စမဆု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၂ သောင်း (၁၀၀) ဆု
</Text>

<Text style={styles.label}>
ရှေ့ (၃)လုံးတူ - ပေါက်ဂဏန်း (၂)ခု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၄ထောင် (၂၀၀၀) ဆု
</Text>

<Text style={styles.label}>
နောက်(၃)လုံးတူ - ပေါက်ဂဏန်း (၂)ခု
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၄ထောင် (၂၀၀၀) ဆု
</Text>

<Text style={styles.label}>
နောက်(၂) လုံးတူ
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ဘတ် ၂ ထောင် (၁၀၀၀၀) ဆု
</Text>

<Text style={styles.label}>
ဆုမဲအရေအတွက် စုစုပေါင်း
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
- ၁၄,၁၆၈ ဆု
</Text>

<Text style={styles.label}>
ဆုကြေးငွေ စုစုပေါင်း
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
(၄၈,၀၀၀,၀၀၀ - ဘတ် ၄၈ သန်း)
</Text>
<Text style={[styles.description, {paddingTop: 0}]}>
အား ဆုကြေးငွေအဖြစ် ဖွင့်လှစ်ပေးပါသည်။ 
</Text>

<Text style={styles.subtitle}>
ShweTaik (Golden Castle Lottery) ထီဖွင့်ရက် - (ထိုင်းထီးဖွင့်သည့်ရက်)
</Text>

<Text style={[styles.description, {paddingTop: hp(0.5)}]}>

  



- လစဉ် (၁) ရက်နေ့နဲ့ (၁၆) ရက်နေ့များတွေ ထိုင်းထီဖွင့်သည့်အတိုင်း ဆုမဲပေါက်စဉ် ဖွင့်လေ့ရှိပါတယ်။ ထူးခြားမှုအနေဖြင့် “ဇန်နဝါရီလမှာ (၁၆) ရက်” နေ့က ထိုင်းနိုင်ငံသားများရဲ့ ဆရာကန်တော့ပွဲနေ့ ဖြစ်တာကြောင့် “ဇန်နဝါရီလ (၁၇) ရက်” နေ့မှာမှ ဆုမဲပေါက်စဉ် ဖွင့်ပြီး၊ “မေလ (၁) ရက်” နေ့က အလုပ်သမားနေ့ကျရောက်သည့်အတွက် “မေလ (၂) ရက်” နေ့မှ ဆုမဲပေါက်စဉ် ဖွင့်ပါသည်။ နောက်ထက်ပြီး “ဇန်နဝါရီ (၁) ရက်” နေ့အစား နှစ်ကုန်စာရင်းချုပ်တာတွေပြုလုပ်မှာမို့ “ဒီဇင်ဘာလ (၃၀) ရက်” နေ့ကို ပြောင်းလဲဖွင့်လှစ်ပါ တယ်။ ShweTaik (Golden Castle Lottery) သည် ထိုင်းအစ်ိုးရ ထီပေါက်စဉ်အတိုင်း ဖွင့်လှစ်ပေးမည်ဖြစ်သော်ကြောင့် အထက်ပါ ထီဖွင့်ဖွင့်ရက်များအတိုင်း ဖွင့်လှစ်ပေးသွားပါမည်။ 
</Text>
<Text style={styles.subtitle}>
ဘယ်လိုထိုးရမလဲ ? ထီပေါက်ကြောင်း ဘယ်လိုသိမလဲ ?
</Text>
<Text style={[styles.description, {paddingTop: hp(0.5)}]}>
- ကံစမ်းချင်သည့် လူကြီးမင်းတို့ အနေဖြင့် ShweTaik - Golden Castle App တွေ ကိုင်ပိုင်အကောင့်တစ်ခု ရှိထားရပါမယ်။ အကောင့်မရှိပါက&nbsp;
<Text onPress={()=> Linking.openURL("https://t.me/ShweTaik_Gc0ffical0ffice")} style={styles.link}>
https://t.me/ShweTaik_Gc0ffical0ffice
  </Text>  (သို့မဟုတ်) နီးစပ်းဟာ Agent များထံဆက်သွယ်၍ အကောင့်ဖွင့်နိုင်ပါသည်။
</Text>
<Text style={[styles.description, {paddingTop: hp(0.5)}]}>
- လက်မှတ် (၁) ဆောင်လျှင် သက်မှတ်စျေးနှုန်း ၈,၀၀၀ ကျပ် ဖြစ်ပြီး ကံစမ်းလိုပါက points များထည့်သွင်းရမည်ဖြစ်ပါသည်။ 
</Text>
<Text style={[styles.description, {paddingTop: hp(0.5)}]}>
- ကံစမ်းမယ်ပေါက်သော ကံထူးရှင်များကို ShweTaik - Golden Castle Agent များမှ (၄၈ နာရီ) အတွင်း ဆက်သွယ်ပေးသွားမှာဖြစ်ပါသည်။  ကံထူးကြောင်းကိုလဲ Application Notifications မှတဆင့်လဲ ထီဖွင့်ပြီး နာရီပိုင်းအတွင်း အကြောင်းကြားပေးမှာပါ။ ဒါ့အပြင် <Text style={styles.link} onPress={()=> Linking.openURL("https://goldencastlelottery.com")}>https://goldencastlelottery.com</Text> ရှိ Check Tickets တွင်လည်း မိမ်ိ ထီလက်မှတ် (သို့မဟုတ်) အခြားသူများ၏ ထီလက်မှတ်များကိုလဲ တိုက်စစ်နိုင်ပါသေးသည်။
</Text>
<Text style={[styles.description, {paddingTop: hp(0.5)}]}>
- ဆုငွေထုတ်ယူရာတွင် ကံထူးသူများထံသို့ ShweTaik - Golden Castle Agent ဘက်မှ အကြားကြားပြီး ဆုငွေအပြည့်အဝကို ကံထူးသူ ဘဏ်အကောင့် ထဲသို့ထည့်သွင်းပေးသွားမည်ဖြစ်ပါသည်။ ကံထူးသူများဘက်မှ မိမိထံ တက်သွယ်မကြောင်းကြားလာသော Agent ကို မိမိ ဆုလက်ခံမည့် ဘဏ်အမည်၊ ဘဏ်အကောင့် နံပတ် တို့ကိုသေချာစွ ပေးပို့ရမည်ဖြစ်သည်။
            </Text>


<TouchableOpacity
    onPress={()=> Linking.openURL("https://t.me/ShweTaik_Gc0ffical0ffice")}>
    <Text style={[styles.label, {paddingTop: hp(3)}]}>
        Telegram: Contact
    </Text>
    <Text style={[styles.label, {paddingTop: 0}]}>
    @ShweTaik_Gc0ffical0ffice
    </Text>
</TouchableOpacity>


            <Text style={styles.version}>
                Version 1.0.2
            </Text>
            </View>
        </ScrollView>
    )
}

export default AboutUs;