import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    Linking,
    StatusBar
} from 'react-native';
import {
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'

import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';
import { deleteMultiItems } from '../../utils/appStorage';
import apiUrl from '../../utils/apiUrl';
import { fetchPostByToken } from '../../utils/fetchData';
import {useLocal} from "../../hook/useLocal";
import styles from './Style';
import { Context } from '../../context/Provider';
import SessionExpired from '../../components/modal/SessionExpired';

const ContactUs = ({navigation}) => {

    const local = useLocal();
    const { 
        lang,
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const lanData = {
        language : lang
    }

    const [
        info,
        setInfo
    ] = useState();
    const [
        expired,
        setExpired
    ] = useState(false);

    useEffect(() => {
        if (userInfo?.access_token) getContactInfo();
    }, [userInfo]);

    const getContactInfo = async () => {

        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
        const response = await fetchPostByToken(apiUrl.contactInfo, {data: encryptLanData}, userInfo.access_token);

        if (response?.success && response?.data) {
            const decContact = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decContact) {
                setInfo(decContact)
            }
        } else if (response?.status === 401) {
            // Unauthenticated
            setExpired(true);
        } 
    }

    const onClose = () => {
        navigation.goBack();
    }

    const viberHandler = () => {
  
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={'#f6f6f6'} barStyle='dark-content' />
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.close}
                onPress={onClose}>
                <AntDesign
                    name='closecircleo'
                    size={hp(3)}
                    color='#11212f' />
            </TouchableOpacity>
            <Image  
                source={require('../../assets/images/contactus3.jpeg')}
                style={styles.img} />
            <View style={styles.topContent}>
                <TouchableOpacity 
                    style={styles.block}
                    onPress={()=> Linking.openURL(`tel:${info?.phone}`)}>
                    <Text style={styles.label}>
                        {local.callUs}
                    </Text>
                    <View style={styles.iconRow}>
                        <FontAwesome
                            name='phone'
                            size={hp(2)}
                            color='#fff' />
                        <Text style={styles.value}>
                            {info?.phone}
                        </Text>
                    </View>
                </TouchableOpacity>
                {/* <View style={styles.block}>
                    <Text style={styles.label}>
                        Call Agent
                    </Text>
                    <View style={styles.iconRow}>
                        <FontAwesome
                            name='phone'
                            size={hp(2)}
                            color='rgba(255, 153, 0, 0.8)' />
                        <Text style={styles.value}>
                            +959 256304626
                        </Text>
                    </View>
                </View> */}
                <TouchableOpacity
                    style={styles.block}
                    onPress={()=> Linking.openURL(`mailto:${info?.email}`)}>
                    <Text style={styles.label}>
                        {local.emailUs}
                    </Text>
                    <View style={styles.iconRow}>
                        <MaterialCommunityIcons
                            name='email'
                            size={hp(2)}
                            color='#fff' />
                        <Text style={styles.value}>
                            {info?.email}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            {/* <View style={styles.orRow}>
                <View style={styles.left} />
                <Text style={styles.orTxt}>OR</Text>
                <View style={styles.left} />
            </View> */}
            {/* <View style={styles.socialRow}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.socialBtn} >
                    <Fontisto
                        name='viber'
                        size={hp(4)}
                        color='#11212f' />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.socialBtn} >
                    <MaterialCommunityIcons
                        name='facebook-messenger'
                        size={hp(4.7)}
                        color='#11212f' />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.socialBtn}
                    onPress={ viberHandler } >
                    <Fontisto
                        name='telegram'
                        size={hp(4)}
                        color='#11212f' />
                </TouchableOpacity>
            </View> */}
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </View>
    )
}

export default ContactUs;