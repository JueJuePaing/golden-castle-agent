import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f6f6'
    },
    close: {
        position: 'absolute',
        marginTop: StatusBar.currentHeight + hp(2),
        right: wp(5)
    },
    img: {
        width: wp(100),
        height: hp(18),
        resizeMode: 'contain',
        marginTop: hp(20), //10
        marginBottom: hp(3)
    },
    topContent: {
        marginHorizontal: wp(3.5),
        marginTop: hp(3),
        justifyContent: 'space-between'
    },
    block: {
        width: wp(80),
        padding: hp(2),
        backgroundColor:'#ff9700',
        borderRadius: hp(1),
        alignSelf: 'center',
        marginBottom: hp(2)
    },
    label: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#edebeb',
        paddingBottom: hp(.7)
    },
    iconRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    value: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#fff',
        paddingLeft: wp(2)
    },
    orRow: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: hp(3),
        marginBottom: hp(3)
    },
    left: {
        width: wp(6),
        height: 1,
        backgroundColor: 'gray'
    },
    orTxt: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#ff9700',
        paddingHorizontal: wp(2)
    },
    socialRow: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    socialBtn: {
        marginHorizontal: wp(3)
    }
})

export default styles;