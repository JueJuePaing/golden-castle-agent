import React, {
    useEffect,
    useContext,
    useState
} from 'react';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import SwitchSelector from "react-native-switch-selector";
import Ionicons from "react-native-vector-icons/Ionicons";

import Loading from '../../components/loading/Loading';
import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { onlySpaces } from '../../utils/validation';
import { numArray } from '../../utils/common';
import { useLocal } from '../../hook/useLocal';
import { deleteMultiItems } from '../../utils/appStorage';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import ResultDateModal from "../../components/modal/ResultDateModal";
import TicketNumber from '../../components/ticketNumber/TicketNumber';
import NumKeyboard from '../../components/keyboard/NumKeyboard';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import SessionExpired from '../../components/modal/SessionExpired';

const CheckResult = ({navigation, route}) => {

    const local = useLocal();
    const {drawDates, date} = route.params;

    const options = [
        { label : local.ticketNumbers, value : 0 },
        { label : local.series, value : 1 }
      ];

    const { 
        lang,
        net,
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        showModal,
        setShowModal
    ] = useState(false);
    const [
        activeTab,
        setActiveTab
      ] = useState(0);
    const [
        fromTicket,
        setFromTicket
    ] = useState("")
    const [
        toTicket,
        setToTicket
    ] = useState("")
    const [
        tickets,
        setTickets
    ] = useState([""]);
    const [
        focused,
        setFocused
    ] = useState(null);
    const [
        selectedDate,
        setSelectedDate
    ] = useState(date);
    const [
        errMsg,
        setErrMsg
    ] = useState();
    const [
        expired,
        setExpired
    ] = useState(false);

    useEffect(() => {
        setSelectedDate(date);
    }, [date]);

    const pressKey = (num) => {
        setErrMsg(null);
        if (activeTab === 0 && tickets[tickets.length-1].length < 6) {
            tickets[tickets.length-1] = tickets[tickets.length-1] + num;
            setTickets(prev => [...prev]);
        }
        else if (activeTab === 1) {
            if (focused === 2 && toTicket.length < 6) {
                setToTicket(prev => prev + num);
            } else if (focused === 1 && fromTicket.length < 6) {
                setFromTicket(prev => prev + num);
            }
        }
    }

    const pressBack = () => {
        if (activeTab === 0 && tickets[tickets.length-1].length > 0) {
            let str = tickets[tickets.length-1].substring(0, tickets[tickets.length-1].length - 1);
            tickets[tickets.length-1] = str;
            setTickets(prev => [...prev]);
        }
        else if (activeTab === 1) {
            if (focused === 1) {
                let str = fromTicket.substring(0, fromTicket.length - 1);
                setFromTicket(str);
            } else {
                let str = toTicket.substring(0, toTicket.length - 1);
                setToTicket(str);
            }
        }
    }

    const addTicket = () => {
        tickets.push("");
        setTickets(prev => [...prev]);
    }

    const removeTicket = (index) => {
        tickets.splice(index, 1);
        setTickets(prev => [...prev]);
    }

    const onSubmit = () => {
        if (!net) {
            ToastAndroid.show(local.noConnection, ToastAndroid.SHORT);
            return;
        }
        if (activeTab === 0 && tickets.length === 1 && tickets[0].length < 6) {
            setErrMsg(local.invalidTicketNo);
            return;
        } else if (activeTab === 1 && (onlySpaces(fromTicket) || onlySpaces(toTicket) || fromTicket.length < 6 || toTicket.length < 6)){
            setErrMsg(local.invalidTicketNos);
            return;
        }
        setLoading(true);
        checkLotteryResult();
    }

    const dateHandler = val => {
        setShowModal(false);
        setSelectedDate(val);
    }

    const checkLotteryResult = async () => {
        if (userInfo?.access_token) {
            let ticket_numbers = [];

            if (activeTab === 0) {
                tickets.map((ticket) => {
                    if (ticket && ticket.length === 6) {
                        ticket_numbers.push(ticket);
                    }
                })
            } else {
                let from = parseInt(fromTicket);
                let to = parseInt(toTicket);
    
                if (parseInt(fromTicket) > parseInt(toTicket)) {
                    from = parseInt(toTicket);
                    to = parseInt(fromTicket);
                }
    
                let range = numArray(from, to);
                ticket_numbers = range;
            }
    
            let data = {
                'draw_id': selectedDate,
                'ticket_numbers': ticket_numbers,
                'language': lang
            }

            let encryptResultData = encryptData(JSON.stringify(data), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.checkLotteryResults, {data: encryptResultData}, userInfo.access_token);

            if (response?.success && response?.data) {
                const decResults = JSON.parse(decryptData(response.data, userInfo.secret_key));
                if (decResults?.results?.length > 0) {
                    clearData();
                    navigation.navigate('Winner', {winners: decResults.results})
                } else {
                    clearData();
                    navigation.navigate('Sorry');
                }
            } else {
                if (response?.status === 401) {
                    // Session Expired
                    setExpired(true);
                } else {
                    clearData();
                    navigation.navigate('Sorry');
                }
            }

            setLoading(false);
        } else {
            logoutHandler();
        }
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const clearData = () => {
        setTickets([""]);
        setFromTicket("");
        setToTicket("");
        setErrMsg(null);
    }

    return (
        <View style={styles.container}>
            <Header
                title={local.checkResult}
                goBack={()=> navigation.goBack()}
                showDate={()=> setShowModal(true)}
                date={ selectedDate } />
            
            <SwitchSelector
                fontSize={hp(1.4)}
                initial={0}
                onPress={value => setActiveTab(value)}
                textColor={ "#11212f" }
                selectedColor="#fff"
                buttonColor={ "#ff9700" }
                borderColor="transparent"
                hasPadding
                options={options}
                height={hp(4.5)}
                style={styles.switch}/>  
            
            {
                activeTab === 0 ? (
                    <View style={styles.card}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                        {
                            tickets.map((ticket, index) => {
                            return <View key={index} style={styles.inputRow}>    
                                <TicketNumber number={ticket} width={wp(68)} />
                                {
                                    index === tickets.length - 1 ?
                                    <TouchableOpacity 
                                        activeOpacity={0.8}
                                        onPress={addTicket}
                                        disabled={tickets[tickets.length-1].length < 6}
                                        style={[styles.addBtn, {
                                            borderColor: tickets[tickets.length-1].length < 6 ? 'rgba(255, 151, 0, 0.3)' : '#11212f'
                                        }]}>
                                        <Ionicons
                                            name="ios-add"
                                            size={hp(2.5)}
                                            color={tickets[tickets.length-1].length < 6 ? 'rgba(255, 151, 0, 0.3)' : '#11212f'} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity 
                                        activeOpacity={0.8}
                                        onPress={()=> removeTicket(index)}
                                        style={styles.addBtn}>
                                        <Ionicons
                                            name="remove"
                                            size={hp(2.5)}
                                            color="#11212f" />
                                    </TouchableOpacity>
                                }
                            </View>
                            })
                        }
                        </ScrollView>
                        <TouchableOpacity 
                            activeOpacity={0.8}
                            onPress={onSubmit}
                            style={styles.submitBtn}>
                            <Text style={styles.submit}>{local.submit}</Text>
                            {/* <View style={styles.moreBtn}>
                                <Entypo
                                    name="chevron-small-right"
                                    size={hp(2.5)}
                                    color="#fff" />
                            </View> */}
                        </TouchableOpacity>
                    </View>
                ) :
                (
                    <View style={styles.card}>
                        <View style={styles.series}>
                            <View style={styles.inputCol}>
                                <Text style={styles.from}>{local.from}</Text>
                                <TextInput
                                    showSoftInputOnFocus={false}
                                    value={fromTicket}
                                    keyboardType="numeric"
                                    style={styles.inputText}
                                    maxLength={6}
                                    onFocus={()=> {
                                        setFocused(1);
                                    }} />
                            </View>
                            <View style={styles.inputCol}>
                                <Text style={styles.from}>{local.to}</Text>
                                <TextInput
                                    showSoftInputOnFocus={false}
                                    value={toTicket}
                                    keyboardType="numeric"
                                    style={styles.inputText}
                                    maxLength={6}
                                    onFocus={()=> {
                                        setFocused(2);
                                    }} />
                            </View>
                        </View>
                        <TouchableOpacity 
                            activeOpacity={0.8}
                            onPress={onSubmit}
                            style={styles.submitBtn}>
                            <Text style={styles.submit}>{local.submit}</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
            <ErrorMessage message={errMsg} width={wp(88)} />
            {/* {
                activeTab === 0 && ( */}
                    <NumKeyboard
                        pressKey={pressKey}
                        pressBack={pressBack}
                        marginBottom={80} />
                {/* )
            } */}

            {showModal && <ResultDateModal
                drawDates={ drawDates }
                closeModalHandler={()=> setShowModal(false)}
                dateHandler={(val) => dateHandler(val)} />}
            {loading && <Loading />}
            {
                expired && <SessionExpired
                    closeModalHandler={()=> {
                        logoutHandler();
                        setExpired(false);
                    }}/>
            }
        </View>
    )
}

export default CheckResult;