import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f1f1'
    },
    header: {
        backgroundColor: '#ff9700',
        paddingTop: StatusBar.currentHeight + hp(1),
        paddingHorizontal: wp(4),
        paddingBottom: hp(1),
        borderBottomLeftRadius: hp(2),
        borderBottomRightRadius: hp(2)
    },
    title: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#fff'
    },
    dateBtn: {
        paddingVertical: hp(.5),
        borderWidth: hp(.12),
        borderColor: '#11212f',
        marginBottom: hp(.4),
        width: wp(33),
        alignItems: 'center',
        borderRadius: hp(1),
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    date: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#fff',
        paddingTop: 3
    },
    checkBtn: {
        paddingVertical: hp(.56),
        backgroundColor: '#11212f',
        bottom: hp(1.4),
        alignItems: 'center',
        borderRadius: hp(1),
        position: 'absolute',
        right: wp(4),
        borderRadius: hp(1),
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: wp(3),

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,

        // borderWidth: hp(.12),
        // borderColor: '#ff9700',
    },
    check: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#fff',
        paddingTop: 3,
        paddingRight: wp(1)
    },
    test: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.5),
        textAlign: 'center',
        color: '#ff9700',
        marginTop: hp(2),
        letterSpacing: -1
    },
    firstRow : {
        marginTop: hp(.5),
        width: wp(94),
        flexDirection: 'row',
        alignItems: 'flex-end',
        alignSelf: 'center',
        justifyContent: 'center'
    },
    beforeContent: {
        width: wp(25),
        height: hp(11)
    },
    beforePrize: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        textAlign: 'center',
        position: 'absolute',
        bottom: hp(1),
        color: '#fff'
        // height: hp(5),
        // paddingTop: hp(1.3)
    },
    beforeBlock: {
        width: wp(25),
        height: hp(8.5),
        backgroundColor: '#11212f',
        alignItems: 'center',
        borderTopLeftRadius: hp(.7),
        paddingTop: hp(1)
    },
    smallSelectedBlock: {
        width: wp(6),
        height: hp(4),
        backgroundColor: '#fff',
        position: 'absolute',
        borderRadius: hp(10),
        bottom: hp(-3)
    },
    smallSelectedDot: {
        backgroundColor: 'rgba(17, 33, 47, 0.5)',
        width: wp(1),
        height: wp(1),
        borderRadius: wp(1),
        position: 'absolute',
        bottom: -1
    },
    firstContent: {
        width: wp(30),
        height: hp(14)
    },
    firstPrize: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        textAlign: 'center',
        color: 'darkgray',
        position: 'absolute',
        bottom: hp(1.5)
    },
    firstBlock: {
        width: wp(24),
        height: hp(14),
        backgroundColor: '#ff9700',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: hp(1.5),
        borderTopRightRadius: hp(1.5),
        // paddingTop: hp(2)
    },
    selectedBlock: {
        width: wp(8),
        height: hp(6),
        backgroundColor: '#fff',
        position: 'absolute',
        borderRadius: hp(10),
        bottom: hp(-5)
    },
    selectedDot: {
        backgroundColor: 'rgba(17, 33, 47, 0.5)',
        width: wp(1),
        height: wp(1),
        borderRadius: wp(1),
        position: 'absolute',
        bottom: -1
    },
    beforeLabel: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(1.5),
        color: '#fff',
        paddingTop: hp(1)
    },
    firstLabelContent: {
        flexDirection: 'row'
    },
    firstLabel: {
        fontFamily: 'Poppins-Bold',
        fontSize: hp(4),
        color: '#fff'
    },
    firstLabel2: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.8),
        color: '#fff',
        paddingTop: 5,
        paddingLeft: wp(1)
    },
    winningNumContent: {
        backgroundColor: '#fff',
        width: wp(84),
        borderRadius: hp(.6),
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignSelf: 'center',
        padding: hp(2)
    },
    winningNumBlock: {
        width: wp(10),
        height: wp(9),
        borderRadius: wp(4.7),
        backgroundColor: '#ff9700',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#11212f'
    },
    winningNumBlockOrange: {
        width: wp(10),
        height: wp(9),
        borderRadius: wp(4.7),
        backgroundColor: '#11212f',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderWidth: 1.2,
        borderColor: '#ff9700'
    },
    winningNum: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#fff',
        paddingTop: 3
    },
    block: {
        backgroundColor: '#fff',
        width: wp(84),
        borderRadius: hp(.6),
        alignSelf: 'center',
        marginTop: hp(2),
        paddingTop: hp(2),
      paddingBottom: hp(1),
    },
    blockHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: wp(3)
    },
    prizeHeader: {
        color: '#ff9700',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7)
    },
    prizeAmount: {
        color: '#ff9700',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6)
    },
   
    winningNumContent2: {
        width: wp(76),
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignSelf: 'center',
        marginBottom: hp(.7)
    },
    flatlist : {
        justifyContent : "space-between"
    },
    menu_list: {
        marginHorizontal: wp(3)
    },
    grid: {
        width: wp(15),
        // height: hp(5),
        marginBottom: hp(1.3),
        justifyContent: 'center',
        alignItems: 'center',
        borderRightWidth : .5,
        borderColor : 'gray'

        // borderTopWidth: 0,
        // borderBottomColor: '#ff9700',
        // borderLeftWidth: 0,
        // borderRightColor: '#ff9700', //a3a6a8
        // borderRadius: hp(1),
        // borderWidth: 3,
        // borderColor: '#ff9700'
    },
    gridNum: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: '#ff9700'
    },
    switch : {
        width : wp(75),
        alignSelf : "center",
        marginTop: hp(1),
        fontSize: 10
    },
    card: {
        backgroundColor: '#fff',
        width: wp(90),
        padding: hp(2),
        alignSelf: 'center',
        marginTop: hp(2),
        borderRadius: hp(1),
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        maxHeight: hp(55),
        overflow: 'hidden'
    },
    moreBtn: {
        width: hp(3),
        height: hp(3),
        backgroundColor: '#ff9700',
        borderRadius: hp(3),
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 3
    },
    submitBtn: {
          width: wp(28),
          height: hp(4),
          backgroundColor: '#11212f',
          borderRadius: hp(3),
          flexDirection:'row',
          alignItems: 'center',
          marginTop: hp(1.5),
          alignSelf: 'center',
          justifyContent: 'center',
          shadowOffset: {width: 5, height: 10},
          shadowColor: '#000',
          shadowOpacity: 0.5,
          shadowRadius: hp(1),
          elevation: 3
    },
    submit: {
          fontSize :hp(1.6),
          color: '#fff',
          fontFamily: 'Poppins-Medium',
          paddingTop: hp(.3),
          textTransform: 'uppercase'
    },
    addBtn: {
        width: wp(8),
        height: wp(8),
        borderRadius: hp(1),
        borderWidth: 2,
        borderColor: '#11212f',
        marginBottom: hp(.7),
        marginLeft: wp(4),
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    series: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: hp(2)
    },
    inputCol: {
        width: wp(38.5)
    },
    from: {
        paddingBottom: hp(1),
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6)
    },
    inputText: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#2d3842',
        width: wp(38.5),
        alignItems: 'center',
        paddingVertical: hp(.8),
        borderRadius: hp(0.5),
        paddingHorizontal: hp(1),
        borderColor: '#fff',
        backgroundColor: '#F2F2F2',
        letterSpacing: 2
    },
    conImg: {
        width: wp(100),
        height: hp(100),
        position: 'absolute'
    },
    modalView: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        height: hp(100),
        justifyContent: 'center',
        width: wp(100)
    },
    modalBg: {
        backgroundColor: '#ff9700',
        borderRadius: hp(1),
        width: wp(80),
        alignSelf: 'center',
        minHeight: hp(25),
        maxHeight: hp(75)
        // marginVertical: hp(10)
    },
    crown: {
        width: wp(50),
        height: hp(20),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: hp(-8)
    },
    sorryImg: {
        width: wp(60),
        height: hp(25),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: hp(-12),
        marginLeft: wp(-4)
    },
    prizeRow: {
        width: wp(65),
        //backgroundColor: 'red',
        alignSelf: 'center',
        marginVertical: hp(2),
        alignItems: 'center'
    },
    prizeTitle: {
        color: '#11212f',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6)
    },
    prizeAmt: {
        color: '#fff',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2.2),
        letterSpacing: .5,
        paddingBottom: hp(.7)
    },
    sorryTitle: {
        color: '#11212f',
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.7),
        textAlign: 'center'
    },
    sorryDesc: {
        color: '#505254',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        textAlign: 'center',
        paddingTop: hp(1),
        paddingBottom: hp(2)
    },
    toggleBtn : {
        // alignSelf : "center",
        backgroundColor : "#11212f",
        height : hp(2),
        width : hp(2),
        borderRadius: hp(3),
        alignItems: 'center',
        justifyContent: "center",
        marginLeft : wp(1),
        marginBottom: hp(.3)
        // marginBottom : -hp(1.2)
    },
    toggleRow: {
        flexDirection:'row',
        alignItems: 'center'
    }
})

export default styles;