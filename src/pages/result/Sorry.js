import React, {useContext} from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Modal
} from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Context } from '../../context/Provider';
import { useLocal } from '../../hook/useLocal';
import styles from './Style';

const Sorry = ({navigation}) => {
    const local = useLocal();
    const { lang } = useContext(Context);

    const onSubmit = () => {
        navigation.replace("Result")
    }

    return (
        <Modal
            transparent={true}
            visible={true}
            animationType="fade">
            <Image
                source={require('../../assets/images/lose3.jpg')}
                style={styles.conImg}/>
            <View style={styles.modalView}>

                <View style={styles.modalBg}>
                    <Image
                        source={require('../../assets/images/sorry.png')}
                        style={styles.sorryImg}/>

                    <Text style={[styles.sorryTitle, {
                        fontSize: lang === 'en' ? hp(2.7) : hp(1.7)
                    }]}>
                        {local.youDidNotWin}
                    </Text>
                    <Text style={styles.sorryDesc}>
                        {local.thankyou}
                    </Text>
                </View>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={onSubmit}
                    style={styles.submitBtn}>
                    <Text style={styles.submit}>{local.ok}</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

export default Sorry;