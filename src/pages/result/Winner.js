import React from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Modal,
    ScrollView
} from 'react-native';
import { formatNumber } from '../../utils/common';

import TicketNumber from '../../components/ticketNumber/TicketNumber';
import styles from './Style';
import { useLocal } from '../../hook/useLocal';

const Winner = ({navigation, route}) => {

    const local = useLocal();
    const {winners} = route.params;

    const onSubmit = () => {
        navigation.replace("Result")
    }

    return (
        <Modal
            transparent={true}
            visible={true}
            animationType="fade">
            <Image
                source={require('../../assets/images/con2.jpg')}
                style={styles.conImg}/>
            <View style={styles.modalView}>

                <View style={styles.modalBg}>
                    <Image
                        source={require('../../assets/images/crown3.png')}
                        style={styles.crown}/>

                    <ScrollView showsVerticalScrollIndicator={false}>
                        {
                            winners.map((winner, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={styles.prizeRow}>
                                        <Text style={styles.prizeTitle}>
                                            {
                                                winner.type === "first" ? local.firstPrize :
                                                winner.type === "closest_first" ? local.closetFirstPrize :
                                                winner.type === "second" ? local.secondPrize :
                                                winner.type === "third" ? local.thirdPrize :
                                                winner.type === "forth" ? local.fourthPrize :
                                                winner.type === "fifth" ? local.fifthPrize : 
                                                winner.type === "first_three" ? local.first3Digits :
                                                winner.type === "last_three" ? local.last3Digits :
                                                winner.type === "last_two" ? local.last2Digits : ""
                                            }
                                        </Text>
                                        <Text style={styles.prizeAmt}>
                                            {`${formatNumber(winner.amount)} ${"\u0E3F"}`}
                                        </Text>
                                        {
                                            winner.tickets.map((ticket, index) => {
                                                return <TicketNumber key={index} number={ticket.toString()} color="#11212f" />
                                            })
                                        }
                                        {/* <TicketNumber number={winner.ticket_number} width={wp(64)} color="#11212f" /> */}
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={onSubmit}
                    style={styles.submitBtn}>
                    <Text style={styles.submit}>{local.finish}</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

export default Winner;