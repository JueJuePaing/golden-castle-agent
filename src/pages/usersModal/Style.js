import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    modalView: {
        backgroundColor: '#f1f1f1',
        height: hp(100),
        width: wp(100)
    },
    list: {

    },
    toggleBtn : {
        // alignSelf : "center",
        backgroundColor : "#11212f",
        height : hp(2),
        width : hp(2),
        borderRadius: hp(3),
        alignItems: 'center',
        justifyContent: "center",
        marginLeft : wp(1),
        marginBottom: hp(.3)
        // marginBottom : -hp(1.2)
    },
    item: {
        backgroundColor: '#fff',
        width: wp(90),
        borderRadius: hp(1),
        paddingVertical: hp(1.4),
        paddingRight: wp(2),
        alignSelf: 'center',
        marginTop: hp(1),
        paddingLeft: wp(3)
    },
    topRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    name: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        paddingTop: hp(.3),
        color: '#11212f'
    },
    openedItem: {
        marginTop: hp(1),
        marginBottom: hp(.6)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: wp(2),
        marginTop: hp(.2)
    },
    user_name: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        paddingTop: hp(.4),
        color:'#11212f',
        paddingLeft: wp(3)
    },
    nodata: {
        marginBottom : hp(40)
    }
})

export default styles;