import React, {
    useContext,
    useCallback,
    useState,
    useEffect
} from 'react';
import {
    View,
    TouchableOpacity,
    Modal,
    FlatList,
    Text,
    Linking,
    ToastAndroid
} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";

import { fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import SessionExpired from '../../components/modal/SessionExpired';

const UsersModal = ({onClose}) => {

    const { 
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const local = useLocal();

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        page,
        setPage
    ] = useState(1);
    const [
        openItem,
        setOpenItem
    ] = useState("");
    const [
        users,
        setUsers
    ] = useState();
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        moreData,
        setMoreData
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);

    useFocusEffect(
        useCallback(() => {
            if (userInfo?.access_token) {
                getUsers(1);
            } else {
                setExpired(true);
            }
        }, [userInfo])
    );

    const getUsers = async (currentPage) => {
        setLoading(true);

        const response = await fetchGetByToken(`${apiUrl.users}?page=${currentPage}`, userInfo.access_token);

        if (response?.status === 200 && response?.data) {
            const decUsers = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decUsers?.users) {
                setMoreData(decUsers.has_more_page);
                if (currentPage === 1) {
                    setUsers(decUsers.users);
                } else {
                    setUsers([...users, ...decUsers.users])
                }
                setNoData(false);

            } else if (currentPage === 1) {
                setNoData(true);
            }
        } else {
            if (response?.status === 429 || response?.message === "Too Many Attempts.") {
                ToastAndroid.show(local.tooManyAttempts, ToastAndroid.SHORT)
                if (!users || users?.length < 1) {
                    setNoData(true);
                } else {
                    setNoData(false);
                }
            }
            else {
                setNoData(true);
            }
            if (response?.status === 401 || response?.message === "Unauthenticated.") {
                setExpired(true);
            }
        }

        setLoading(false);
    }

    const loadMoreHandler = async () => {
        if (moreData) {
            setPage(page + 1);
            getUsers(page + 1);
        }
    };

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const toggleItem = item => {
        if (openItem === item.user_name) {
            setOpenItem()
        } else {
            setOpenItem(item.user_name)
        }
    }

    return (
        <Modal
            transparent={false}
            visible={true}
            onRequestClose={onClose}
            animationType="fade">
            <View style={styles.modalView}>
                <Header
                    title={local.users}
                    goBack={onClose}/>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    onEndReached={ () => loadMoreHandler() }
                    onEndReachedThreshold={0.5}
                    keyExtractor={(item, index) => index.toString()}
                    style={ styles.list }
                    data={ users }
                    renderItem={({item, index}) => (
                        <TouchableOpacity 
                            activeOpacity={1}
                            key={item.id}
                            style={styles.item}>
                            <TouchableOpacity
                                style={styles.topRow}
                                activeOpacity={0.8}
                                onPress={()=> toggleItem(item)}>
                                <Text style={styles.name}>
                                    {item.name}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={openItem === item.user_name ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#ff9700"
                                        />
                                </View>
                            </TouchableOpacity>
                            {
                                openItem === item.user_name && (
                                    <View style={styles.openedItem}>
                                        <View style={styles.row}>
                                            <Entypo
                                                name={'user'}
                                                size={hp(1.6)}
                                                color="#ff9700"
                                                />
                                            <Text style={styles.user_name}>
                                                {item.user_name}
                                            </Text>
                                        </View>
                                        <TouchableOpacity 
                                            style={styles.row}
                                            activeOpacity={0.8}
                                            onPress={()=> Linking.openURL(`tel:${item.phone}`)}>
                                            <Entypo
                                                name={'ticket'}
                                                size={hp(1.6)}
                                                color="#ff9700"
                                                />
                                            <Text style={styles.user_name}>
                                                {item.current_ticket_count}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                )
                            }
                        </TouchableOpacity>
                    )} /> 
            {
                !loading && noData && <View style={styles.nodata}>
                    <NoData 
                    message={local.noUser}
                    reloadHandler={ () => {
                        getUsers(1);
                        setPage(1);
                    } } />
                </View>
            }
            </View>
        
            {loading && <Loading />}
         
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </Modal>
    )
}

export default UsersModal;