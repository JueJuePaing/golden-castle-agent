import React, {
    useContext,
    useState
} from 'react';
import {
    View,
    StatusBar,
    Text,
    TextInput,
    ToastAndroid,
    Image,
    TouchableOpacity
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons'

import { Context } from '../../context/Provider';
import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { deleteMultiItems } from '../../utils/appStorage';
import { onlySpaces } from '../../utils/validation';
import { useLocal } from '../../hook/useLocal';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import SessionExpired from '../../components/modal/SessionExpired';

const Feedback = ({navigation}) => {

    const { 
        lang,
        userInfo,
        net,
        changeUserInfo
    } = useContext(Context);

    const local = useLocal();
    const lanData = new FormData();
    lanData.append('language', lang);

    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        title,
        setTitle
    ] = useState('');
    const [
        description,
        setDescription
    ] = useState('');
    const [
        success,
        setSuccess
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        formError,
        setFormError
    ] = useState({
        titleError: null,
        detailError: null,
        internalError: null
    })

    const onClose = () => {
        navigation.goBack();
    }

    const onSubmit = async () => {

        if (!net) {
            ToastAndroid.show(local.noConnection, ToastAndroid.SHORT);
            return;
        }
        
        let valid = true;
        if(onlySpaces(title)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    titleError: local.invalidFeedbackTitle
                }
            ));
        }
        if(onlySpaces(description)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    detailError: local.invalidFeedbackDetail
                }
            ));
        }

        if (!valid) {
            return;
        }

        setLoading(true);

        if (userInfo && userInfo.access_token) {
            const data = {
                title,
                detail: description
            }
            const response = await fetchPostByToken(apiUrl.feedback, data, userInfo.access_token);;
            if (response?.success) {
                setTitle('');
                setDescription('');
                setSuccess(true);
            } else {
                if (response?.status === 401) {
                    // Unauthenticated
                    setExpired(true);
                } else {
                    if (response?.message) {
                        setFormError((prev) => (
                            {
                                ...prev,
                                internalError: response.message
                            }
                        ));
                    } else {
                        ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);
                    }
                }
            }
        } else {
            ToastAndroid.show(local.unauthenticated, ToastAndroid.SHORT);
            setExpired(true);
        }
        setLoading(false);
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const onChangeTitle = (val) => {
        setTitle(val);
        setFormError((prev) => (
            {
                ...prev,
                titleError: null,
                internalError: null
            }
        ));
    }

    const onChangeDesc = (val) => {
        setDescription(val);
        setFormError((prev) => (
            {
                ...prev,
                detailError: null,
                internalError: null
            }
        ));
    }

    return (
        <View style={styles.container}>
            <Header
                title={local.feedback}
                goBack={onClose}/>
            <Image  
                source={require('../../assets/images/logo_black.png')}
                style={styles.feedback} />
            {
                success ?
                <View style={styles.card}>
                    <Ionicons
                        name='checkmark-done-circle'
                        size={hp(6)}
                        color='#11212f' />
                    <Text style={[styles.title, {marginTop: hp(2)}]}>
                        {local.thanksForFeedback}
                    </Text>
                    <Text style={styles.thanks}>
                        {local.reviewFeedback}
                    </Text>
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={onClose}
                        style={styles.submitBtn}>
                        <Text style={styles.submit}>{local.ok}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={()=> setSuccess(false)}
                        style={styles.feedbackBtn}>
                        <Text style={styles.feedbackTxt}>
                            {local.anotherFeedback}
                        </Text>
                    </TouchableOpacity>
                </View> :
                <View style={styles.card}>
                    <Text style={styles.title}>
                        {local.giveFeedback}
                    </Text>
                    <View style={styles.inputContent}>
                        <TextInput
                            placeholder={local.title}
                            placeholderTextColor="#c9c7c3"
                            value={title}
                            onChangeText={ onChangeTitle }
                            maxLength={30}
                            style={[
                                styles.inputText,
                                {
                                    fontSize: title === "" ? hp(1.5) : hp(1.7),
                                    height: hp(4.8)
                                }]} />
                    </View>
                    <ErrorMessage message={formError.titleError}  />

                    <View style={styles.inputContent}>
                        <TextInput
                            placeholder={local.detail}
                            placeholderTextColor="#c9c7c3"
                            value={description}
                            multiline
                            numberOfLines={8}
                            onChangeText={ onChangeDesc }
                            maxLength={1000}
                            style={[
                                styles.inputText,
                                styles.detailText,
                                {
                                    fontSize: title === "" ? hp(1.5) : hp(1.7)
                                }]} />
                    </View>
                    <ErrorMessage message={formError.detailError}  />

                    <ErrorMessage message={formError.internalError}  />

                    <TouchableOpacity 
                        activeOpacity={0.8}
                        onPress={onSubmit}
                        style={styles.submitBtn}>
                        <Text style={styles.submit}>{local.submit}</Text>
                    </TouchableOpacity>
                </View>
            }
             {
                loading && <Loading />
            }
             {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </View>
    )
}

export default Feedback;