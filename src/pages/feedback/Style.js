import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    close: {
        position: 'absolute',
        right: wp(5),
        top: StatusBar.currentHeight + hp(2)
    },
    feedback: {
        width: wp(90),
        alignSelf: 'center',
        height: hp(15),
        resizeMode: 'contain',
        marginTop: hp(2)
    },
    card: {
        backgroundColor: '#fff',
        width: wp(90),
        padding: hp(2),
        alignSelf: 'center',
        marginTop: hp(1),
        borderRadius: hp(.5),
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        alignItems: 'center'
        // maxHeight: hp(40)
    },
    title: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(1.9),
        color: "#ff9700",
        paddingTop: hp(1)
    },
    inputContent: {
       // height: hp(4.8),
        width: wp(80),
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: hp(2)
    },
    inputText: {
        borderColor: 'lightgray',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#000',
        paddingVertical: 0,
        width: wp(80),
        borderWidth: 1,
       // height: hp(4.8),
        borderRadius: hp(1),
        borderColor: 'lightgray',
        paddingLeft: wp(4),
        paddingTop: hp(.3)
    },
    detailText: {
        textAlignVertical: 'top',
        justifyContent: 'flex-start',
        paddingTop: hp(1.4)
    },
    submitBtn: {
        width: wp(60),
        height: hp(4),
        backgroundColor: '#11212f',
        borderRadius: hp(3),
        alignItems: 'center',
        marginTop: hp(1.5),
        alignSelf: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3
  },
  submit: {
        fontSize :hp(1.6),
        color: '#fff',
        fontFamily: 'Poppins-Medium',
        paddingTop: hp(.3),

  },
  feedbackBtn: {
    marginTop: hp(1.5)
},
feedbackTxt: {
    fontSize :hp(1.5),
    color: '#ff9700',
    fontFamily: 'Poppins-Regular'
},
  thanks: {
    fontSize :hp(1.5),
    color: '#ff9700',
    fontFamily: 'Poppins-Regular',
    paddingVertical: hp(2)
},
})

export default styles;