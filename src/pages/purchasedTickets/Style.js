import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flexDirection: 'row',
        width: wp(100),
        marginTop: hp(2),
        marginBottom: hp(1.3)
    },
    title: {
        fontSize :hp(1.6),
        color:'#ff9700',
        fontFamily: 'Poppins-Medium',
        textAlign: 'center'
    },
    loadMoreBtn: {
        alignSelf: 'center',
        marginBottom : hp(6),
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color:'gray'
    },
    loadMore: {

    }
})

export default styles;