import React, {
    useEffect,
    useContext,
    useState
} from 'react';
import {
    View,
    StatusBar,
    TouchableOpacity,
    Text,
    ScrollView
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import moment from 'moment';

import { fetchPostByToken, fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { getItem } from '../../utils/appStorage';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { decryptData } from '../../utils/decryptData';
import { encryptData } from '../../utils/encryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';

import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import PurchasedTicket from '../../components/purchase/PurchasedTicket';
import SessionExpired from '../../components/modal/SessionExpired';
import NetworkProblem from '../../components/modal/NetworkProblem';

const PurchasedTickets = ({navigation}) => {

    const { 
        net,
        userInfo,
        lang,
        changeUserInfo
    } = useContext(Context);
    const local = useLocal();

    const lanData = {
        language : lang
    }
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        exchange,
        setExchange
    ] = useState();
    const [
        tickets,
        setTickets
    ] = useState();
    const [
        drawDates,
        setDrawDates
    ] = useState([]);
    const [
        nextdraw,
        setNextdraw
    ] = useState();
    const [
        hasMore,
        setHasMore
    ] = useState(false);
    const [
        loadingMore,
        setLoadingMore
    ] = useState(false)
    const [
        page,
        setPage
    ] = useState(1);
    const [
        expired,
        setExpired
    ] = useState(false);

    useEffect(() => {
        if (net && userInfo?.access_token) {
            getDrawDates();
        } else {
            setExpired(true);
        }
     }, [net, userInfo]);

    const getDrawDates = async () => {
       setLoading(true);

        const draw = await getItem('@nextdraw');
        setNextdraw(draw);


        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
        const response = await fetchPostByToken(apiUrl.getDrawDates, {data: encryptLanData}, userInfo.access_token);

        if (response?.success && response?.data) {
            const decDates = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if(decDates?.draw_ids) {
                setDrawDates(decDates.draw_ids);
                getUsersTickets(1);
            } else {
                getUsersTickets(1)
            }
        } else {
            if (response?.status === 401) {
                // Session Expired
                setLoading(false);
                setExpired(true);
            } else {
                getUsersTickets(1)
            }
        }

    }

    const getUsersTickets = async (page) => {
        const response = await fetchGetByToken(`${apiUrl.usersTickets}?page=${page}`, userInfo.access_token);

        if (response?.status === 200 && response?.data) {
            const decTickets = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decTickets?.exchange_rate) {
                setExchange(decTickets.exchange_rate);
            }
            if (decTickets?.purchased_tickets) {
                setTickets(decTickets.purchased_tickets);
                setPage(decTickets.next);
                if (decTickets.purchased_tickets.length >= 10) {
                    setHasMore(true);
                }
            } else {
                if (page === 1) {
                    setNoData(true);
                }
                setHasMore(false);
            }
        } else {
            if (response?.status === 401) {
                // Session Expired
                setLoading(false);
                setExpired(true);
            } else if (page === 1) {
                setNoData(true);
            }
            setHasMore(false);
        }
        setLoading(false);
    }

    const onClose = () => {
        navigation.goBack();
    }

    const moreHandler = () => {
        if (!loadingMore) {
            setLoadingMore(true);
            getUsersTickets(page);
        }
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle={'light-content'} backgroundColor="#ff9700" />
            
            <Header
                title={local.userTickets}
                goBack={onClose}
                exchange_rate={ exchange }
                />
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                    {
                        !loading && tickets && (
                            <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: hp(10)}}>
                                <View style={styles.header}>
                                    <Text style={[styles.title, {width: wp(20)}]}>
                                        {local.draw}
                                    </Text>
                                    <Text style={styles.title}>
                                        {local.tickets}
                                    </Text>
                                </View>

                                {
                                    tickets[nextdraw] ?
                                        <PurchasedTicket
                                            day={moment(nextdraw).format('D')}
                                            month={moment(nextdraw).format('MMM')}
                                            coming={true}
                                            tickets={tickets[nextdraw]} />
                                    : null
                                }
                                {
                                    drawDates && drawDates.map((date, index) => {
                                        return <>
                                            {
                                                tickets[date] ?
                                                    <PurchasedTicket
                                                        day={moment(date).format('D')}
                                                        month={moment(date).format('MMM')}
                                                        coming={false}
                                                        tickets={tickets[date]} /> 
                                                : null
                                            }
                                        </>
                                        
                                    })
                                }
                                {
                                    hasMore && (
                                        <TouchableOpacity
                                            activeOpacity={0.8}
                                            style={ styles.loadMoreBtn }
                                            onPress={ () => moreHandler() }>
                                            {
                                                loadingMore ?
                                                <Text style={ styles.loadMore }>
                                                    {`${local.loading}...`}
                                                </Text>
                                                    :
                                                    <SimpleLineIcons
                                                        name='reload'
                                                        size={hp(2.6)}
                                                        color='gray'
                                                        />
                                            }
                                        </TouchableOpacity>
                                    )
                                }
                            </ScrollView>
                        )
                    }

                    {loading && <Loading />}

                    {
                        !loading && noData && <NoData 
                            message={local.noPurchasedTickets} />
                    }
                    {
                        expired && <SessionExpired
                            closeModalHandler={()=> {
                                setExpired(false)
                                logoutHandler();
                            }}/>
                    }
                    </>
            }
            
        </View>
    )
}

export default PurchasedTickets;