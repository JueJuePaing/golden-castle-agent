import React, {
    useState,
    useContext,
    useCallback,
    useEffect,
    useMemo,
    useRef
} from 'react';
import {
    View,
    Text,
    RefreshControl,
    ScrollView,
    ToastAndroid,
    TouchableOpacity,
    Linking,
    StatusBar,
    Image,
    DeviceEventEmitter
} from 'react-native';
import styles from './Style';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import { Context } from '../../context/Provider';
import moment from 'moment';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Entypo from 'react-native-vector-icons/Entypo';

import { fetchPostByToken, fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { setItem } from '../../utils/appStorage';
import { deleteMultiItems } from '../../utils/appStorage';
import { decryptData } from '../../utils/decryptData';
import { encryptData } from '../../utils/encryptData';

import Ticket from '../../components/ticket/Ticket';
import SlideImageHeader from '../../components/slideImage/slideImageHeader';
import Header from "../../components/header/HomeHeader";
import Countdown from '../../components/countdown/Countdown';
import TicketHeader from '../../components/ticketHeader/TicketHeader';
import Language from '../../components/language/Language';
import LoginModal from '../auth/Login';
import Loading from '../../components/loading/Loading';
import NetworkProblem from '../../components/modal/NetworkProblem';
import ForceUpdate from '../../components/modal/ForceUpdate';
import UsersModal from '../usersModal/UsersModal';
import SessionExpired from '../../components/modal/SessionExpired';

const targetWidth = wp(92);

const Home = ({navigation}) => {

    const local = useLocal();

    const mounted = useRef(false);

    const { 
        changeLang,
        userInfo,
        lang,
        net,
        profileInfo,
        bannersData,
        countdownData,
        lastDrawData,
        usersTicketsData,
        changeUserInfo,
        changeProfileInfo
    } = useContext(Context);

    const lanData = {
        language : lang
    }

    const [
        refreshing,
        setRefreshing
      ] = useState(false);
    const [
        showLan,
        setShowLan
    ] = useState(false);
    const [
        showUsers,
        setShowUsers
      ] = useState(false);
    const [
        showLogin,
        setShowLogin
    ] = useState(false);
    const [
        defaultTab,
        setDefaultTab
    ] = useState(1);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        banners,
        setBanners
    ] = useState([]);
    const [
        newBanners,
        setNewBanners
    ] = useState([]);
    const [
        countdownDate,
        setCountdownDate
    ] = useState(null);
    const [
        drawDate,
        setDrawDate
    ] = useState();
    const [
        countdownDay,
        setCountdownDay
    ] = useState();
    const [
        countdownHour,
        setCountdownHour
    ] = useState();
    const [
        countdownMin,
        setCountdownMin
    ] = useState();
    const [
        countdownSec,
        setCountdownSec
    ] = useState();
    const [
        lastDrawTickets,
        setLastDrawTickets
    ] = useState();
    const [
        usersTickets,
        setUsersTickets
    ] = useState();
    const [
        forceUpdate,
        setForceUpdate
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        noUserTicket,
        setNoUserTicket
    ] = useState(false);
    const [
        contactInfo,
        setContactInfo
    ] = useState();

    function convertMS(ms) {
        var d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        h += d * 24;
        return {h, m, s}
    }

    const getProfileData = async () => {
        if (userInfo?.access_token) {
            const response = await fetchGetByToken(apiUrl.profile, userInfo.access_token);
            if (response?.status === 200 && response?.data) {
                const decAgentinfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
                changeProfileInfo(decAgentinfo?.agent);
                setItem('@offline_profile', JSON.stringify(decAgentinfo?.agent));
            }
        } 
    }

    const onRefresh = useCallback(() => {
        setRefreshing(true);
    
        const reloadData = async () => {
            getContactData();
            getProfileData();
            getInitialData(); 
        };
    
        reloadData();
      }, [userInfo, usersTicketsData, lastDrawData]);

      useEffect(() => {
        if (bannersData) {
            setBanners(bannersData)
        }
        if (countdownData) {
            if (countdownData?.current_draw) {
                setCountdownDate([moment(countdownData.current_draw).format('D'), moment(countdownData.current_draw).format('MMM')]);
                setDrawDate(moment(countdownData.current_draw).format('D MMMM YYYY'))
                setItem('@nextdraw', countdownData.current_draw);
                setItem('@nextdrawDate', moment(countdownData.current_draw).format('D MMMM YYYY'));
            }
            if (countdownData?.day_diff) {
                setCountdownDay(countdownData.day_diff);
            }
        }
        if (lastDrawData) {
            setLastDrawTickets(lastDrawData);
        }
        if (usersTicketsData) {
            setUsersTickets(usersTicketsData)
        }
      }, [bannersData, countdownData, lastDrawData, usersTicketsData])

    useEffect(() => {
        getContactData();
    }, [userInfo]);

    const getContactData = async () => {
        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
        const response = await fetchPostByToken(apiUrl.contactInfo, {data: encryptLanData}, userInfo.access_token);
        if (response?.success && response?.data) {
            const decContact = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decContact) {
                setContactInfo(decContact)
            }
        }
    }

    useEffect(() => {
        const interval = setInterval(() => {
            const start = new Date();
            const end = new Date();
            end.setDate(end.getDate() + 1);
            end.setHours(0,0,0,0)
    
            const diffTime = Math.abs(end - start);
            const result = convertMS(diffTime);
            setCountdownHour(result.h);
            setCountdownMin(result.m);
            setCountdownSec(result.s);
        }, 1000);
        return () => {
            if(interval) clearInterval(interval);
        };
    }, []);

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    useEffect(() => {
        if (mounted.current) {
            setLoading(true);
            getInitialData();
        }
    }, [mounted.current, usersTicketsData, lastDrawData]);

    useEffect(() => {
        mounted.current = true;

        return () => {
            mounted.current = false;
        };
    }, []);

    useEffect(() => {
        if (!userInfo || !userInfo.access_token) setExpired(true);
    }, [userInfo]);

    useEffect(() => {

        let isMounted = true; 

        const getImageHeight = async () => {
          const newData = await Promise.all(banners.map(async (item) => {
            return new Promise((resolve, reject) => {
              Image.getSize(item, (originalWidth, originalHeight) => {

                if (!isMounted) {
                    return; // Component is unmounted, don't update state
                  }

                const aspectRatio = originalWidth / originalHeight;
                const targetHeight = targetWidth / aspectRatio;

                const returnData = {
                  height: Math.floor(targetHeight),
                  url: item,
                };
                resolve(returnData);
              }, (error) => {

                if (!isMounted) {
                    return; // Component is unmounted, don't update state
                  }

                console.error('Error getting image size:', error);
                const returnData = {
                  height, // Set a default height
                  url: item,
                };
                resolve(returnData); // Resolve with the default data
              });
            });
          }));
    
           if (isMounted) {
            setNewBanners(newData);
           }
        }
        if (banners && banners.length > 0) {
          getImageHeight();
        }

        return () => {
            isMounted = false; // Set the flag to indicate unmounting
            // You can also cancel any ongoing async operations here if needed
          };
      }, [banners]);

      const biggestBanner = useMemo(() => {
        if (newBanners?.length > 0) {
            const biggestItem = newBanners?.reduce((prev, current) => {
                return prev.height > current.height ? prev : current;
            }, newBanners);
            return biggestItem;
        } else {
            return null;
        }
      }, [newBanners]);

    const getInitialData = async () => {

        // if (bannersData) {
        //     if (!banners || banners.length === 0)  {
        //         setBanners(bannersData);
        //     }
        // } else {
            const response = await fetchPostByToken(apiUrl.banners, lanData, userInfo.access_token);
            if (response?.success && response?.data) {
                const decBanners = JSON.parse(decryptData(response.data, userInfo.secret_key));
                setBanners(decBanners?.banner_urls);
            } else if (response?.status === 401 || response?.message === "Unauthenticated.") {
                setExpired(true);
            } else if (response?.status === 429 || response?.message === "Too Many Attempts.") {
                ToastAndroid.show(local.tooManyAttempts, ToastAndroid.SHORT);
            }
        // }

        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
        const countdownResponse = await fetchPostByToken(apiUrl.countdown, {data: encryptLanData}, userInfo.access_token);
        if (countdownResponse?.success && countdownResponse?.data) {
            const decCountDown = JSON.parse(decryptData(countdownResponse.data, userInfo.secret_key));

            if (decCountDown?.current_draw) {
                setCountdownDate([moment(decCountDown.current_draw).format('D'), moment(decCountDown.current_draw).format('MMM')]);
                setDrawDate(moment(decCountDown.current_draw).format('D MMMM YYYY'))
                setItem('@nextdraw', decCountDown.current_draw);
                setItem('@nextdrawDate', moment(decCountDown.current_draw).format('D MMMM YYYY'));
            }
            if (decCountDown?.day_diff) {
                setCountdownDay(decCountDown.day_diff);
            }
        } else if (countdownResponse?.status === 401 || countdownResponse?.message === "Unauthenticated.") {
            setExpired(true);
        }

        const lastDrawResponse = await fetchPostByToken(apiUrl.lastDrawTickets, {data: encryptLanData}, userInfo.access_token);

        if (lastDrawResponse?.success && lastDrawResponse?.data) {
            const decLastDraw = JSON.parse(decryptData(lastDrawResponse.data, userInfo.secret_key));
            setLastDrawTickets(decLastDraw);
        } else if (lastDrawResponse?.status === 401 || lastDrawResponse?.message === "Unauthenticated.") {
            setExpired(true);
        } else {
            if (!lastDrawTickets) {
                if (lastDrawData) {
                    setLastDrawTickets(lastDrawData);
                }
            }
        }

        const usersTicketsResponse = await fetchPostByToken(apiUrl.usersTicketsNumbers, {data: encryptLanData}, userInfo.access_token);
        if (usersTicketsResponse?.success && usersTicketsResponse?.data) {
            let decTickets = JSON.parse(decryptData(usersTicketsResponse.data, userInfo.secret_key));

            if (decTickets?.user_tickets?.length > 0) {
                setNoUserTicket(false);
            } else {
                setNoUserTicket(true);
            }
            setUsersTickets(decTickets);
        } else if (usersTicketsResponse?.status === 401 || usersTicketsResponse?.message === "Unauthenticated.") {
            setExpired(true);
        } else if (usersTicketsResponse?.status === 429 || usersTicketsResponse?.message === "Too Many Attempts.") {
            console.error("ERROR : usersTickets");
            if (usersTicketsData) {
                setUsersTickets(usersTicketsData)
                console.error("333 >> ", JSON.stringify(usersTicketsData));
                if (!usersTicketsData || !usersTicketsData.user_tickets || usersTicketsData.user_tickets?.length < 1) {
                    setNoUserTicket(true)
                } else {
                    setNoUserTicket(false);
                }
            } else {
                console.error("444 >> ", JSON.stringify(usersTickets));
                if (usersTickets?.user_tickets?.length > 0) {
                    setNoUserTicket(false);
                } else {
                    setNoUserTicket(true);
                }
            }
           
        } else {
            setNoUserTicket(true);
        }
        setLoading(false);
        setRefreshing(false);
    }

    const lanHandler = async (lan) => {
        setShowLan(false);
        setLoading(true);
        changeLang(lan);
        setItem('@lang', lan);

        if (userInfo?.access_token && lang !== lan) {

            let encryptLanData = encryptData(JSON.stringify({language: lan}), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.updateLanguage, {data: encryptLanData}, userInfo.access_token);
            if (response?.status === 401 || response?.message === "Unauthenticated.") {
                setExpired(true);
            }
        }

        setLoading(false);
    }

    const reloadHandler = () => {
        if (net) {
            setLoading(true);
            getInitialData();
        }
    }

    if (!net) {
        return <NetworkProblem reloadHandler={reloadHandler} />
    }

    const updateHandler = () => {
        Linking.openURL("http://play.google.com/store/apps/details?id=com.facebook.katana")
    }

    const contactHandler = () => {
        setShowUsers(true)
    }

    if (forceUpdate) {
        return <ForceUpdate updateHandler={updateHandler} />
    }

    return (
        <ScrollView 
            refreshControl={
                <RefreshControl
                    refreshing={ refreshing }
                    colors={[ "#11212f" ]}
                    onRefresh={ onRefresh }
                    progressViewOffset={hp(4)}  />
            }
            style={styles.container} showsVerticalScrollIndicator={false}>
            <StatusBar barStyle={"light-content"} backgroundColor='#ff9700' />
            <Header 
                loggedIn={ userInfo !== null }
                name={ profileInfo ? profileInfo.name : '' }
                lanHandler={()=> setShowLan(true)}
                callCenterHandler={() => Linking.openURL(`tel:${contactInfo?.phone}`)}
                point={profileInfo ? profileInfo.point : 0}
                ticketCount={profileInfo ? profileInfo.ticket_count : 0} />

            <View style={styles.infoRow}>
                <View style={[styles.infoBlock, {marginRight: wp(2)}]}>
                    <View style={styles.row}>
                        <FontAwesome5
                            name="coins"
                            size={hp(1.6)}
                            color='#ff9700' />
                        <Text style={styles.point}>
                            {profileInfo ? profileInfo.point : 0}
                        </Text>
                    </View>
                    <Text style={styles.pointLabel}>
                        {local.earningPoints}
                    </Text>
                </View> 
                <View style={ styles.infoBlock }>
                    <View style={styles.row}>
                        <Entypo
                            name={'ticket'}
                            size={hp(1.8)}
                            color="#ff9700" // 596066
                            />
                        <Text style={styles.point}>
                            {profileInfo ? profileInfo.ticket_count : 0}
                        </Text>
                    </View>
                    <Text style={styles.pointLabel}>
                        {local.userTickets}
                    </Text>
                </View> 
            </View> 


            <View style={[styles.bannerBg, {
                height : biggestBanner ? biggestBanner.height + 50 : hp(23)
            }]} />

            <View style={{ marginTop: -(biggestBanner ? biggestBanner.height+40 : hp(21)) }}> 
                {
                    !loading && newBanners?.length > 0 && (
                        <SlideImageHeader 
                            height={biggestBanner ? biggestBanner.height : hp(14)} 
                            data={newBanners} />
                    )
                }
                <Countdown
                    countdownDate={ countdownDate }
                    countdownDay={ countdownDay }
                    countdownHour={ countdownHour }
                    countdownMin={ countdownMin }
                    countdownSec={ countdownSec }/>
            </View>

            <View style={styles.bottomContent}>
                <TicketHeader title={local.userTickets} />
                {
                    !noUserTicket ?
                        <Ticket 
                            users={true}
                            data={usersTickets}
                            drawDate={ drawDate }
                            ticket_background={ usersTickets?.ticket_background }
                            ticket_photo={ usersTickets?.photo }
                            pressHandler={ contactHandler } />
                    :
                        <View style={styles.noticketView}>
                            <Image
                                source={
                                    usersTickets?.ticket_background ? 
                                    { uri : usersTickets?.ticket_background } :
                                    require('../../assets/images/ticketBg.png')}
                                style={styles.bgImg} />
                            <Text style={styles.noTicket}>
                                {local.noUserTickets}
                            </Text>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.contactBtn}
                                onPress={contactHandler}>
                                <Text style={styles.contactTxt}>
                                    {local.contactUsers}
                                </Text>
                            </TouchableOpacity>
                        </View>
                }

                <TicketHeader title={local.lastDrawTickets} />
                <Ticket 
                    data={lastDrawTickets}
                    drawDate={ drawDate }
                    ticket_background={ lastDrawTickets?.ticket_background }
                    ticket_photo={ lastDrawTickets?.photo }
                    pressHandler={() => navigation.navigate("ResultStack")} />
            </View>

            {
                showLan && <Language 
                    closeModalHandler={()=> setShowLan(false)}
                    lanHandler={ lanHandler } />
            }
            {
                showLogin && (
                    <LoginModal
                        onClose={()=> setShowLogin(false)}
                        defaultTab={ defaultTab } />
                )
            }
            {loading && <Loading />}
            {showUsers && <UsersModal onClose={()=> setShowUsers(false)}
            />}
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </ScrollView>
    )
}

export default Home;