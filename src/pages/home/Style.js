import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
       //  marginBottom: hp(10),
        backgroundColor: '#f1f1f1'
    },
    bannerBg: {
        // height: hp(23),
        backgroundColor: '#ff9700'
    },
    bottomContent: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#f1f1f1',
        paddingTop: hp(5),
        marginTop: hp(-6),
        zIndex: 1,
        marginBottom: hp(10)
    },
    text: {
        fontSize :hp(1.9),
        color:'#fff',
        fontFamily: 'Poppins-Regular',
        paddingTop: 2
    },
    ortext: {
        fontSize :hp(1.5),
        color:'#fff',
        fontFamily: 'Poppins-Regular',
        paddingTop: 2,
        zIndex: 3,
    },
    loginRow: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: hp(1.2),
        justifyContent: 'center'
    },
    loginBtn: {
        width: wp(38),
        height: hp(4.5),
        backgroundColor: '#ff9700',
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    regBtn: {
        backgroundColor: '#11212f',
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 5,
        borderBottomRightRadius : 5
    },
    loginTxt: {
        color: '#fff',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7)
    },
    rotateBtn: {
        backgroundColor: '#fff',
        zIndex: 1,
        position: 'absolute',
        width: hp(3.8),
        height: hp(3.8),
        borderRadius: 5,
        transform: [
            {
                rotate: '45deg'
            }
        ],
        marginTop: 3
    },
    orBtn: {
        backgroundColor: 'transparent',
        width: hp(4.5),
        height: hp(4.5),
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 2
    },
    orTxt: {
        color: '#ff9700',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.4),
        paddingTop: 5
    },



    conImg: {
        width: wp(100),
        height: hp(100),
        position: 'absolute'
    },
    modalView: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        height: hp(100),
        justifyContent: 'center',
        width: wp(100)
    },
    modalBg: {
        backgroundColor: '#ff9700',
        borderRadius: hp(1),
        width: wp(80),
        alignSelf: 'center',
        minHeight: hp(25),
        maxHeight: hp(75)
        // marginVertical: hp(10)
    },
    crown: {
        width: wp(50),
        height: hp(20),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: hp(-8)
    },
    sorryImg: {
        width: wp(60),
        height: hp(25),
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: hp(-12),
        marginLeft: wp(-4)
    },
    prizeRow: {
        width: wp(65),
        //backgroundColor: 'red',
        alignSelf: 'center',
        marginVertical: hp(2),
        alignItems: 'center'
    },
    prizeTitle: {
        color: '#11212f',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6)
    },
    prizeAmt: {
        color: '#fff',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2.2),
        letterSpacing: .5,
        paddingBottom: hp(.7)
    },
    sorryTitle: {
        color: '#11212f',
        fontFamily: 'Poppins-SemiBold',
        fontSize: hp(2.7),
        textAlign: 'center'
    },
    sorryDesc: {
        color: '#f2cc94',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        textAlign: 'center',
        paddingTop: hp(1),
    },
    submitBtn: {
        width: wp(28),
        height: hp(4),
        backgroundColor: '#11212f',
        borderRadius: hp(3),
        flexDirection:'row',
        alignItems: 'center',
        marginTop: hp(1.5),
        alignSelf: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3
  },
  submit: {
        fontSize :hp(1.6),
        color: '#fff',
        //paddingLeft: wp(4),
        fontFamily: 'Poppins-Medium',
        paddingTop: hp(.3)
  },
  noticketView: {
    alignSelf: 'center',
    height: hp(20),
    width: wp(92),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: hp(1)
  },
  noTicket: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.65),
    color: '#5c564e'
  },
  contactBtn: {
    height: hp(4.3),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff9700',
    borderRadius: hp(3),
    paddingHorizontal: wp(3),
    borderWidth: 1,
    borderColor: '#11212f',

    shadowOffset: {width: 5, height: 10},
    shadowColor: '#fff',
    shadowOpacity: 0.5,
    shadowRadius: hp(1),
    elevation: 3,
    marginTop: hp(1)
  },
  contactTxt: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.5),
    color: '#fff',
    paddingTop: hp(.5)
  },
    bgImg: {
        height: hp(20),
        width: wp(92),
        position: 'absolute',
        borderRadius: hp(0.5)
    },
    infoRow: {
        flexDirection: 'row',
        backgroundColor: '#ff9700',
        justifyContent: 'center',
        paddingBottom: hp(.2)
    },
    infoBlock: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: hp(1.5),
        backgroundColor: '#1e2c38',
        // borderWidth: 0.3,
        borderColor: '#fff',
        paddingVertical: hp(1),
        shadowOffset: { width: 10, height: 10 },
        shadowColor: '#fff',
        shadowOpacity: 0.8, // Increased opacity for more visible shadow
        shadowRadius: hp(3), // Increased radius for a larger shadow effect
        elevation: 5,
        width: wp(38),
        justifyContent: 'center'
    },
    point: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2.2),
        color: '#fff',
        paddingLeft: wp(2),
        paddingTop: hp(.4)
    },
    pointLabel : {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: '#d0d6db',
        marginTop: hp(-0.4)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})

export default styles;