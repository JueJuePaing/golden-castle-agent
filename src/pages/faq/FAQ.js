import React, {
    useState,
    useEffect,
    useContext
} from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from 'react-native-vector-icons/AntDesign'

import { Context } from '../../context/Provider';
import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { deleteMultiItems } from '../../utils/appStorage';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';
import SessionExpired from '../../components/modal/SessionExpired';

const FAQ = ({navigation}) => {

    const local = useLocal();
    const { 
        net,
        lang,
        userInfo,
        changeUserInfo
    } = useContext(Context);
    const lanData = {
        language : lang
    }


    const [
        active,
        setActive
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        faqs,
        setFaqs
    ] = useState();
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);

    useEffect(() => {
        if (net && userInfo?.access_token) getFAQ();
    }, [net, userInfo]);

    const getFAQ = async () => {
        setLoading(true);

        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
        const response = await fetchPostByToken(apiUrl.faqs, {data: encryptLanData}, userInfo.access_token);

        if (response?.success && response?.data) {
            const decFaq = JSON.parse(decryptData(response.data, userInfo.secret_key));
            if (decFaq?.faqs)  {
                setFaqs(decFaq.faqs)
            } else {
                setNoData(true);
            }
        } else {
            setNoData(true);
            if (response?.status === 401) {
                setExpired(true);
            }
        }
        setLoading(false);
    }

    const onClose = () => {
        navigation.goBack();
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    return (
        <View style={styles.container}>
            <Header
                title={local.frequentlyAskedQues}
                goBack={onClose}/>
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                        {
                            !loading && faqs && (
                                <ScrollView showsVerticalScrollIndicator={false}>
                            <Text style={styles.faq}>
                                {local.faqDescrition}
                            </Text>
                            {
                                faqs.map((faq, index) => {
                                    return (
                                        <View style={styles.card}>
                                            <TouchableOpacity
                                                activeOpacity={0.8}
                                                style={[styles.toggleContent, {
                                                    paddingBottom: active === index ? 0 : hp(2)
                                                }]}
                                                onPress={()=> {
                                                    if (index === active) {
                                                        setActive()
                                                    } else {
                                                        setActive(index)
                                                    }
                                                }}>
                                                <View style={styles.left}>
                                                    <AntDesign 
                                                        name="questioncircle"
                                                        size={hp(2)}
                                                        color='#11212f' />
                                                    <Text style={styles.title}>
                                                        {faq.question}
                                                    </Text>
                                                </View>
                                                <Entypo 
                                                    name={active === index ? 'chevron-small-up' : 'chevron-small-down'}
                                                    size={hp(2)}
                                                    color='gray' />
                                            </TouchableOpacity>
                                            {
                                                active === index && (
                                                    <View style={styles.descContent}>
                                                        <Text style={styles.description}>
                                                            {faq.answer}
                                                        </Text>
                                                    </View>
                                                )
                                            }
                                        </View>
                                    )
                                })
                            }
                        </ScrollView>
                            )
                        }
                        {
                            !loading && noData && <NoData message={local.noData} />
                        }
                        {loading && <Loading />}
                    </>
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </View>
    )
}

export default FAQ;