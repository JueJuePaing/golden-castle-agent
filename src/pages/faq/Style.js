import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    faq: {
        paddingVertical: hp(2),
        paddingHorizontal: wp(3),
        backgroundColor: '#fff',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: 'gray'
    },
    card: {
        width: wp(94),
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderRadius: hp(.5),
        marginTop: hp(1),
       

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        width : wp(94)

    },
    toggleContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: wp(3),
        paddingTop: hp(2)
    },
    left: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#ff9700',
        paddingLeft: wp(2),
        paddingTop: hp(.3),
        maxWidth: wp(80)
    },
    descContent: {
        paddingHorizontal: wp(4),
        paddingVertical: hp(2)
    },
    description: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: '#ff9700'
    },
})

export default styles;