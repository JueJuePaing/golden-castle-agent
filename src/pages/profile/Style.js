import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight
    },
    header: {
        backgroundColor: '#ff9700',
       // height: hp(25),
        width: wp(100)
    },
    pointContent: {
        alignSelf: 'flex-start'
    },
    infoContent: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: wp(3),
        marginTop: hp(2),
        marginBottom: hp(9),
        zIndex: 1
    },
    infoContent2: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    infoContent3: {
        alignSelf: 'center',
        alignItems: 'center'
    },
    logoProfile: {
        width: hp(8),
        height: hp(8)
    },
    profile: {
        // width: hp(6),
        // height: hp(6),
        // borderRadius: hp(6),
        marginRight: wp(2)
    },
    nameRow: {
        flexDirection:'row',
        alignItems: 'center'
    },  
    name: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#fff',
        marginBottom: -hp(.3),
        marginRight : wp(2)
    },
    username: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.4),
        color: '#f0eded'
    },
    agentContent: {
        paddingHorizontal: wp(2),
        paddingTop: hp(.3),
        paddingBottom: hp(.2),
        borderRadius: hp(2),
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 151, 0, 0.7)'
    },
    agent: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.3),
        color: '#fff'
    },
    agentCode: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.3),
        color: '#fff'
    },
    middleContent: {
        width: wp(80),
        height: hp(20),
        backgroundColor: '#fff',
        alignSelf: 'center',
       // position: 'absolute',
      //  marginTop: hp(-11),
        borderRadius: hp(1),
        marginTop: hp(-6),
        marginBottom: hp(2)
    },  
    purchaseContent: {
        width: wp(80),
        alignSelf: 'center',
        height: hp(6),
        borderTopLeftRadius: hp(1),
        borderTopRightRadius: hp(1),
        backgroundColor: '#11212f',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: wp(3),

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3
    },
    ticket: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#fff',
    },
    ticketno: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        color: '#fff',
    },
    logo: {
        width: wp(15),
        height: wp(15)
    },
    detailBtn: {
        backgroundColor: '#ff9700',
        paddingHorizontal: wp(2),
        paddingVertical: hp(.2),
        borderRadius: hp(.5),
        flexDirection: 'row',
        alignItems: 'center'
    },
    detailTxt: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.4),
        color: '#fff',
    },
    lotteryRow: {
        flexDirection: 'row',
      //  backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'space-around',
        
    },
    lotteryBtn: {
        height: hp(10),
        //backgroundColor: 'green',
        width: wp(35),
        marginVertical: hp(2),
        alignItems: 'center'
    },
    lotteryTitle: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#ff9700',
        paddingTop: hp(1)
    },
    icon: {
        width: hp(6),
        height: hp(6),
        backgroundColor: 'rgba(255, 151, 0, 0.4)',
        alignItems:'center',
        justifyContent: 'center',
        borderRadius: hp(6)
    },
    settings: {
        width: wp(100),
        marginTop: hp(17),
        paddingHorizontal: wp(5),
        backgroundColor: '#f1f1f1'
    },
    settingTitle: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: 'gray',
        marginBottom: hp(1)
    },
    settingBlock: {
        backgroundColor: '#fff',
        borderRadius: hp(1),
        width: wp(90)
    },
    logoutBtn: {
        alignSelf: 'center',
        marginVertical: hp(2),
        paddingHorizontal: wp(4),
        paddingVertical: hp(.4),
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: hp(1),
        backgroundColor: '#ff9700',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,

        marginBottom: hp(16)
    },
    logout: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#11212f',
        paddingTop: hp(.5),
        paddingLeft: wp(1)
    },
    loginRow: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: hp(1.2),
        justifyContent: 'center'
    },
    loginBtn: {
        width: wp(35),
        height: hp(4),
        backgroundColor: '#11212f',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    regBtn: {
        backgroundColor: '#11212f',
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 5,
        borderBottomRightRadius : 5
    },
    loginTxt: {
        color: '#fff',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7)
    },
    rotateBtn: {
        backgroundColor: '#fff',
        zIndex: 1,
        position: 'absolute',
        width: hp(3.8),
        height: hp(3.8),
        borderRadius: 5,
        transform: [
            {
                rotate: '45deg'
            }
        ],
        marginTop: 3
    },
    orBtn: {
        backgroundColor: 'transparent',
        width: hp(4.5),
        height: hp(4.5),
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 2
    },
    orTxt: {
        color: '#ff9700',
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.4),
        paddingTop: 5
    },
    pointRow: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: wp(1)
    },
    point: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.6),
        color: '#11212f',
        paddingLeft: wp(1),
        paddingTop: hp(.5)
    },
    modalContainer: {
        marginTop: StatusBar.currentHeight + hp(1)
    }
})

export default styles;