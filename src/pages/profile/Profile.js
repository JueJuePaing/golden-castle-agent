import React, {
    useEffect,
    useState,
    useContext,
    useCallback
} from 'react';
import {
    Linking,
    Alert,
    DeviceEventEmitter,
    View,
    ToastAndroid,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    StatusBar,
    RefreshControl
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import FontAwesome from "react-native-vector-icons/FontAwesome"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { useLocal } from '../../hook/useLocal';
import { fetchPostByToken, fetchGetByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { setItem } from '../../utils/appStorage';
import { decryptData } from '../../utils/decryptData';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { encryptData } from '../../utils/encryptData';
import styles from './Style';
import Crown from '../../assets/icons/Crown';
import Youtube from '../../assets/icons/Youtube';
import ProfileMenu from '../../components/profile/ProfileMenu';
import Language from '../../components/language/Language';
import LoginModal from '../auth/Login';
import Terms from '../auth/Terms';
import SessionExpired from '../../components/modal/SessionExpired';

const Profile = ({navigation}) => {

    const local = useLocal();


const SETTINGS_MENUS = [
    {
        id : 1,
        title: local.general,
        sub_menus: [
            {
                id: 12,
                title: local.changePassword,
                icon: <Feather 
                    name="edit"
                    size={hp(2)}
                    color='#11212f' />
            },
            {
                id: 13,
                title: local.language,
                icon:   <FontAwesome
                    name="language"
                    size={hp(2)}
                    color='#11212f' />
            },
            {
                id: 15,
                title: local.withdraw,
                icon:   <MaterialCommunityIcons
                    name="cash-refund"
                    size={hp(2.2)}
                    color='#11212f' />
            },
        ]
    },
    {
        id : 2,
        title: local.aboutAndTerms,
        sub_menus: [
            {
                id: 21,
                title: local.aboutUs,
                icon: <AntDesign 
                    name="infocirlce"
                    size={hp(2)}
                    color='#11212f' />
            },
            {
                id: 22,
                title: local.contactUs,
                icon: <FontAwesome5 
                    name="phone-alt"
                    size={hp(1.8)}
                    color='#11212f' />
            },
            {
                id: 23,
                title: local.rateUs,
                icon: <MaterialIcons 
                    name="star-rate"
                    size={hp(2.5)}
                    color='#11212f' />
            },
            {
                id: 24,
                title: local.terms,
                icon: <Feather 
                    name="file-text"
                    size={hp(2)}
                    color='#11212f' />
            },
        ]
    },
    {
        id : 3,
        title: local.helpAndSupport,
        sub_menus: [
            {
                id: 31,
                title: local.faq,
                icon: <AntDesign
                    name="questioncircle"
                    size={hp(2)}
                    color='#11212f' />
            }
        ]
    }
]

const SETTINGS_MENUS_UNKNOWN = [
    {
        id : 1,
        title: 'General',
        sub_menus: [
            {
                id: 13,
                title: local.language,
                icon:   <FontAwesome
                    name="language"
                    size={hp(2)}
                    color='#11212f' />
            },
        ]
    },
    {
        id : 2,
        title: 'About & Terms',
        sub_menus: [
            {
                id: 21,
                title: local.aboutUs,
                icon: <AntDesign 
                    name="infocirlce"
                    size={hp(2)}
                    color='#11212f' />
            },
            {
                id: 22,
                title: local.contactUs,
                icon: <FontAwesome5 
                    name="phone-alt"
                    size={hp(1.8)}
                    color='#11212f' />
            },
            {
                id: 23,
                title: local.rateUs,
                icon: <MaterialIcons 
                    name="star-rate"
                    size={hp(2.5)}
                    color='#11212f' />
            },
            {
                id: 24,
                title: local.terms,
                icon: <Feather 
                    name="file-text"
                    size={hp(2)}
                    color='#11212f' />
            },
        ]
    },
    {
        id : 3,
        title: 'Help & Support',
        sub_menus: [
            {
                id: 31,
                title: local.faq,
                icon: <AntDesign 
                    name="questioncircle"
                    size={hp(2)}
                    color='#11212f' />
            }
        ]
    }
]


    const { 
        changeUserInfo,
        changeLang,
        userInfo,
        profileInfo,
        lang,
        changeProfileInfo
    } = useContext(Context);

    const [
        refreshing,
        setRefreshing
      ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        defaultTab,
        setDefaultTab
    ] = useState(1);
    const [
        showLogin,
        setShowLogin
    ] = useState(false);
    const [
        showTerms,
        setShowTerms
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        showLan,
        setShowLan
    ] = useState(false);
    const [
        profileData,
        setProfileData
    ] = useState();

    const onRefresh = useCallback(() => {
        setRefreshing(true);
    
        const reloadData = async () => {
            getProfile();
        };
    
        reloadData();
    }, [userInfo]);

    useEffect(() => {
        if (profileInfo) {
            setProfileData(profileInfo)
        } else {
            setLoading(true);
            getProfile();
        }
    }, [profileInfo]);

    useEffect(()=> {
        const eventEmitter = DeviceEventEmitter.addListener(
            'withdraw_success',
            val => {
              if (val === "true") {
                setLoading(true);
                getProfile();
              }
            },
          );
          return () => eventEmitter;
    }, []);

    const getProfile = async () => {
        if (userInfo?.access_token) {

            const response = await fetchGetByToken(apiUrl.profile, userInfo.access_token);
            if (response?.status === 200 && response?.data) {
                const decAgentinfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
                setProfileData(decAgentinfo.agent);
            } else if (response?.status === 401) {
                setExpired(true);
            } else if (response?.status === 429 || response?.message === "Too Many Attempts.") {
                ToastAndroid.show(local.tooManyAttempts, ToastAndroid.SHORT);
            }
            setLoading(false);
        } else {
            setLoading(false);
            setExpired(true);
        }
        setRefreshing(false)
    }

    const handleCancel = () => {
        console.log("Cancel");
    }

    const showConfirmAlert = () => {
        Alert.alert(
          'Confirmation',
          'Are you sure you want to logout?',
          [
            { text: 'Cancel', onPress: handleCancel, style: 'cancel' },
            { text: 'Confirm', onPress: logoutHandler },
          ],
          { cancelable: false } // Prevent dismissing the alert by tapping outside or pressing the back button
        );
      };

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
        changeProfileInfo();
        setProfileData();
    }

    const pressHandler = (id) => {
        if (id === 12) {
            navigation.navigate('ChangePassword');
        } else  if (id === 13) {
            setShowLan(true);
        } else if (id === 15) {
            navigation.navigate("Withdraw");
        } else if (id === 22) {
            navigation.navigate('ContactUs')
        } else if (id === 31) {
            navigation.navigate('FAQ')
        } else if (id === 32) {
            navigation.navigate('Feedback')
        } else if (id === 21) {
            navigation.navigate('AboutUs')
        } else if (id === 23) {
            Linking.openURL("https://play.google.com/store/apps/details?id=com.agent.shwetaik")
        } else if (id === 24) {
            setShowTerms(true);
        }
    }

    const closeTerms = () => {
        setShowTerms(false);
    }

    const openLoginModal = () => {
        setShowLogin(true);
        setDefaultTab(1);
    }
    
    const lanHandler = async (lan) => {
        setShowLan(false);
        setLoading(true);
        changeLang(lan);
        setItem('@lang', lan);

        if (userInfo?.access_token && lang !== lan) {

            let encryptLanData = encryptData(JSON.stringify({language: lan}), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.updateLanguage, {data: encryptLanData}, userInfo.access_token);
            if (response?.status === 401 || response?.message === "Unauthenticated.") {
                setExpired(true);
            }
        }
        setLoading(false);
    }

    if (showTerms) return <View style={styles.modalContainer}>
        <Terms onClose={ closeTerms } /> 
    </View>

    return (
        <ScrollView 
            refreshControl={
                <RefreshControl
                    refreshing={ refreshing }
                    colors={[ "#11212f" ]}
                    onRefresh={ onRefresh } 
                    progressViewOffset={hp(4)} />
            }
            style={styles.container}
            showsVerticalScrollIndicator={false}>
            <StatusBar barStyle={'light-content'} backgroundColor="#ff9700" />

            <View style={styles.header}>
                <View style={[styles.infoContent, {
                    justifyContent: userInfo ? 'space-between' : 'center'
                }]}>
                    {
                        userInfo ? 
                            <View style={styles.infoContent2}>
                                <FontAwesome
                                    name="user-circle-o"
                                    size={hp(4)}
                                    color='#283e52'
                                    style={styles.profile}  />
                                <View>
                                    <View style={ styles.nameRow }>
                                        <Text style={styles.name}>
                                            { profileData ? profileData.name : ''}
                                        </Text>
                                    </View>
                                    <Text style={styles.username}>
                                        {profileData ? `@${profileData.code}` : '-'}
                                    </Text>
                                </View>
                            </View> :
                            <View style={styles.infoContent3}>
                                <Image 
                                    source={require('../../assets/images/logo_black.png')}
                                    style={styles.logoProfile}/>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.loginBtn}
                                    onPress={ openLoginModal }>
                                    <Text style={styles.loginTxt}>
                                        {local.login}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                    }
                    {
                        userInfo && (
                            <View style={ styles.pointContent }>
                                <View style={ styles.pointRow }>
                                    <FontAwesome5
                                        name="coins"
                                        size={hp(1.6)}
                                        color='#11212f' />
                                    <Text style={styles.point}>
                                        {profileData ? profileData.point : '0'}
                                    </Text>
                                </View> 
                            </View>
                        )
                        
                    }
                </View>
               
            </View>
            
            <View style={styles.middleContent}>
                <TouchableOpacity
                    style={styles.purchaseContent} 
                    disabled={!userInfo}
                    onPress={() =>  navigation.navigate('PurchasedTickets') }>
                    <View>
                        <Text style={styles.ticket}>
                           {local.userTickets}
                        </Text>
                    </View>
                    <View style={styles.detailBtn}>
                        <Text style={styles.detailTxt}>
                            {
                                profileData && profileData.ticket_count ? `${profileData.ticket_count} ${local.notickets}` : `0 ${local.notickets}`
                            }
                        </Text>
                        <Entypo
                            name="chevron-small-right"
                            size={hp(1.7)}
                            color="#fff" />
                    </View>
                </TouchableOpacity>
                <View style={styles.lotteryRow}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.lotteryBtn}
                        onPress={()=> navigation.navigate('WinnerList')}>
                        <View style={styles.icon}>
                            <Crown />
                        </View>
                        <Text style={styles.lotteryTitle}>
                            {local.winnerList}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.lotteryBtn}
                        disabled={!profileData?.live_opening}
                        onPress={()=> Linking.openURL(profileData.live_opening)}>
                            
                        <View style={styles.icon}>
                            <Youtube />
                        </View>
                        <Text style={styles.lotteryTitle}>
                           {local.liveOpening}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View> 
  
               
                {
                    userInfo ? SETTINGS_MENUS.map((menu, index) => {
                        return <View style={[styles.settings, {
                            marginTop: index === 0 ? hp(0) : hp(2)
                        }]}>
                            <Text style={styles.settingTitle}>
                                { menu.title }
                            </Text>
                            <View style={styles.settingBlock}>
                                {
                                    menu.sub_menus.map((submenu, index) => {
                                        return <ProfileMenu
                                            key={ index }
                                            pressHandler={()=> pressHandler(submenu.id)}
                                            icon={ submenu.icon }
                                            menuTitle={ submenu.title }
                                            latest={ index === menu.sub_menus.length - 1 } />
                                    })
                                }
                            </View>
                        </View>
                    }) : 
                    SETTINGS_MENUS_UNKNOWN.map((menu, index) => {
                        return <View style={[styles.settings, {
                            marginTop: index === 0 ? hp(0) : hp(2),
                            marginBottom: index === SETTINGS_MENUS_UNKNOWN.length - 1 ? hp(10) : 0
                        }]}>
                            <Text style={styles.settingTitle}>
                                { menu.title }
                            </Text>
                            <View style={styles.settingBlock}>
                                {
                                    menu.sub_menus.map((submenu, index) => {
                                        return <ProfileMenu
                                            pressHandler={()=> pressHandler(submenu.id)}
                                            icon={ submenu.icon }
                                            menuTitle={ submenu.title }
                                            latest={ index === menu.sub_menus.length - 1 } />
                                    })
                                }
                            </View>
                        </View>
                    })
                }
                {
                    userInfo && <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.logoutBtn}
                        onPress={ showConfirmAlert }>
                        <AntDesign 
                            name="logout"
                            size={hp(2)}
                            color='#11212f' />
                        <Text style={styles.logout}>
                            {local.logout}
                        </Text>
                    </TouchableOpacity>
                }
                {
                    showLan && <Language 
                        closeModalHandler={()=> setShowLan(false)}
                        lanHandler={ lanHandler } />
                }

            {
                showLogin && (
                    <LoginModal
                        onClose={()=> setShowLogin(false)}
                        defaultTab={ defaultTab } />
                )
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        logoutHandler();
                    }}/>
            }
        </ScrollView>
    )
}

export default Profile;