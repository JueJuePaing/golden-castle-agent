import React, { useState } from "react";
import { StatusBar } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";

const Context = React.createContext();

const Provider = (props) => {
    const { children } = props;
    const [auth, setAuth] = useState(true);
    const [lang, setLang] = useState('en');
    const [userInfo, setUserInfo] = useState(null);
    const [drawDates, setDrawDates] = useState([]);
    const [lastLoginUser, setLastLoginUser] = useState();
    const [bannersData, setBanners] = useState();
    const [countdownData, setCountdownData] = useState();
    const [liveSaleData, setLiveSaleData] = useState();
    const [lastDrawData, setLastDrawData] = useState();
    const [profileInfo, setProfileInfo] = useState();
    const [net, setNet] = useState(true);
    const [wonLottery, setWonLottery] = useState(false);
    const [usersTicketsData, setUsersTicketsData] = useState();
    const [homeLoaded, setHomeLoaded] = useState(false);

    const context = {
        auth,
        lang,
        userInfo,
        drawDates,
        lastLoginUser,
        bannersData,
        countdownData,
        liveSaleData,
        lastDrawData,
        profileInfo,
        wonLottery,
        net,
        usersTicketsData,
        homeLoaded,
        changeAuth: val => {
          setAuth(val);
        },
        changeLang: val => {
          setLang(val);
        },
        changeUserInfo: val => {
          setUserInfo(val);
        },
        changeDrawDates: val => {
          setDrawDates(val);
        },
        changeLastLoginUser: val => {
          setLastLoginUser(val);
        },
        changeBanners: val => {
          setBanners(val);
        },
        changeCountdownData: val => {
          setCountdownData(val);
        },
        changeLiveSaleData: val => {
          setLiveSaleData(val);
        },
        changeLastDrawData: val => {
          setLastDrawData(val);
        },
        changeProfileInfo: val => {
          setProfileInfo(val);
        },
        changeNet: val => {
          setNet(val)
        },
        changeWonLottery: val => {
          setWonLottery(val)
        },
        changeUsersTicketsData: val => {
          setUsersTicketsData(val);
        },
        changeHomeLoaded: val => {
          setHomeLoaded(val);
        }
      };

    return (
      <Context.Provider value={context}>
        <SafeAreaProvider>
          <StatusBar style="dark" barStyle={'dark-content'} translucent backgroundColor="transparent" />
            {/* <StatusBar barStyle={'dark-content'} backgroundColor="#f1f1f1" /> */}
            {children}
        </SafeAreaProvider>
      </Context.Provider>
    );
};

export {
  Provider, Context
}
  