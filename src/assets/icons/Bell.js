import React from 'react';
import Svg, {Path} from 'react-native-svg';

function Bell() {
  return (
    <Svg
      width={28}
      height={28}
      viewBox="0 0 24 24"
      fill="#ff9700"
      stroke="#ff9700"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round">
      <Path d="M18 8A6 6 0 006 8c0 7-3 9-3 9h18s-3-2-3-9M13.73 21a2 2 0 01-3.46 0" />
    </Svg>
  );
}

export default Bell;
