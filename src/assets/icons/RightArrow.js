import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
function RightArrow() {
  return (
    <Svg  
      viewBox="0 0 16 16" 
      width={hp(3.5)}
      height={hp(3.5)}>
      <Path
        fillRule="evenodd"
        fill="#fff"
        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
    </Svg>
  );
}

export default RightArrow;
